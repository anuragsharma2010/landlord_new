<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Notification extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

    public function sender_user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function receiver_user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}
