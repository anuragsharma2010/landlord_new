<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class OrderMenuItem extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_menu_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

    /**
     * Get all of the owning orderable models.
     */
    public function orderable()
    {
        return $this->morphTo('orderable');
    }

    public function menu_item_event()
    {
        return $this->belongsTo(Event::class, 'orderable_id');
            //->where('orders.orderable_type', Event::class);
    }
    public function menu_item_order_food()
    {
        return $this->belongsTo(OrderFood::class, 'orderable_id');
            //->where('orders.orderable_type', OrderFood::class);
    }

    public function event_menu_item()
    {
        return $this->belongsTo(FoodMenuItem::class, 'menu_item_id');
    }

    public function order_food_menu_item()
    {
        return $this->belongsTo(FoodMenuItem::class, 'menu_item_id');
    }
}
