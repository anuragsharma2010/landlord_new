<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use Notifiable,Sortable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = 'users';

    //protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'password'];
    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
      return $this->belongsTo('App\Role', 'role_id');
    }

    public function carterDetail()
    {
        return $this->hasOne('\App\CatererDetail','user_id');
    }

    public function userDetail()
    {
        return $this->hasOne('\App\UserDetail','user_id');
    }

    public function uploadDocument()
    {
        return $this->hasOne('\App\UploadDocument','user_id');
    }

    public function dietaries()
    {
        return $this->belongsToMany('App\Dietary', 'user_dietaries', 'user_id', 'dietary_id')->withPivot('preferences', 'other');
    }

    public function cuisines()
    {
        return $this->belongsToMany('App\Cuision', 'user_cuisines', 'user_id', 'cuisine_id');
    }

    public function event_attendees()
    {
        return $this->belongsToMany('App\Event', 'event_attendees', 'attendee_id', 'event_id');
    }

    public function user_promo_codes(){
        return $this->belongsToMany('App\PromoCode', 'promo_code_users', 'user_id', 'promo_code_id');
    }
}
