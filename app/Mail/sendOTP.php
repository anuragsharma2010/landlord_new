<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendOTP extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $password;
    public function __construct($emails)
    {
        $this->password=$emails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject="OTP Request Received";
        return $this->view('mail.user.send_otp')->subject($subject);
    }
}
