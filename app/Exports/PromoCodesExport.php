<?php

namespace App\Exports;

use App\PromoCode;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class PromoCodesExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.NAME'),
            trans('admin.PERCENTAGE'),
            trans('admin.EXPIRY_DATE'),
            trans('admin.USE_PER_PERSON'),
            trans('admin.MINIMUM_AMOUNT'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Promo Codes';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('promo_codes')
            ->select('name','percentage_value',DB::raw('DATE_FORMAT(expiry_date, "%m/%d/%Y") as expiry_date'),'use_per_person',DB::raw('CONCAT("$", "", minimum_amount) as minimum_amount'),DB::raw('(CASE WHEN status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
