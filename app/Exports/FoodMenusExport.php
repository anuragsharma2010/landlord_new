<?php

namespace App\Exports;

use App\FoodMenu;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class FoodMenusExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.FOODMENU'),
            trans('admin.PARENT_FOOD_MENU'),
            trans('admin.FOOD_MENU_PRICE'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Food Menus (Individual)';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('food_menus')
            ->leftJoin('food_menus as parent_food_menus', 'food_menus.parent_id', '=', 'parent_food_menus.id')
            ->select('food_menus.menu_name as menu_name','parent_food_menus.menu_name as parent_menu_name',DB::raw('CONCAT("$", "", food_menus.price) as price'),DB::raw('(CASE WHEN food_menus.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(food_menus.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(food_menus.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('food_menus.created_at', 'desc')
            ->get();
    }
}
