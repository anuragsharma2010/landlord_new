<?php

namespace App\Exports;

use App\OrderFood;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class OrderFoodsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.FOOD_ORDER_TYPE'),
            trans('admin.USER_NAME'),
            trans('admin.LOCATION'),
            trans('admin.DATETIME'),
            trans('admin.BUDGET'),
            trans('admin.EVENT_GUEST_COUNT'),
            trans('admin.CUISION'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Food Orders';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('order_foods')
            ->join('users', 'order_foods.user_id', '=', 'users.id')
            ->join('locations', 'order_foods.location_id', '=', 'locations.id')
            ->leftJoin('budgets', 'order_foods.budget_id', '=', 'budgets.id')
            ->leftJoin('cuisions', 'order_foods.cuisine_id', '=', 'cuisions.id')
            ->select('order_foods.order_type','users.name as order_food_user','locations.name as order_food_location', DB::raw('DATE_FORMAT(CONCAT(order_foods.delivery_date, " ", order_foods.delivery_time), "%m/%d/%Y %H:%i:%s") as order_food_datetime'),'budgets.amount','order_foods.guest_count','cuisions.name as order_food_cuisine',DB::raw('DATE_FORMAT(order_foods.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(order_foods.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('order_foods.created_at', 'desc')
            ->get();
    }
}
