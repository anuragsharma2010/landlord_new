<?php

namespace App\Exports;

use App\EventEnquiry;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class EventEnquiriesExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.USER_NAME'),
            trans('admin.EMAIL'),
            trans('admin.PHONE_NUMBER'),
            trans('admin.LOCATION'),
            trans('admin.EVENT_GUEST_COUNT'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Event Enquiries';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('event_enquiries')
            ->join('users', 'event_enquiries.user_id', '=', 'users.id')
            ->join('locations', 'event_enquiries.location_id', '=', 'locations.id')
            ->select('users.name as user_name','users.email as email','users.contact as contact','locations.name as location_name','event_enquiries.guest_count',DB::raw('DATE_FORMAT(event_enquiries.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(event_enquiries.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('event_enquiries.created_at', 'desc')
            ->get();
    }
}
