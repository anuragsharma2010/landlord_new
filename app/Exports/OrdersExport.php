<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;
use Config;

class OrdersExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{

    public function headings(): array
    {
        return [
            trans('admin.INVOICE_NO'),
            trans('admin.ORDER_TYPE'),
            trans('admin.USER_NAME'),
            trans('admin.TOTAL_AMOUNT'),
            trans('admin.PAYMENT_TYPE'),
            trans('admin.ORDER_STATUS'),
            trans('admin.ORDER_DATE'),
        ];
    }

    public function title(): string
    {
        return 'Orders';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orders = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('payments', 'payments.order_id', '=', 'orders.id')
            ->select('orders.invoice_no','orders.orderable_type','users.name as order_user',DB::raw('CONCAT("$", "", orders.total_amount) as total_amount'),'payments.payment_type',DB::raw('(CASE WHEN orders.order_status = "0" THEN "In Process" WHEN orders.order_status = "1" THEN "Delivered" WHEN orders.order_status = "2" THEN "Cancelled" WHEN orders.order_status = "3" THEN "Rejected" ELSE "" END) as order_status'),DB::raw('DATE_FORMAT(orders.created_at, "%m/%d/%Y") as created_at'))
            ->orderBy('orders.created_at', 'desc')
            ->get();
        
        return $orders->map(function ($row, $key) {

            $order_data = [
                'invoice_no' => $row->invoice_no,
                'orderable_type' => '',
                'order_user' => $row->order_user,
                'total_amount' => $row->total_amount,
                'payment_type' => ucwords(Config::get('global.payment_type.' . $row->payment_type)),
                'order_status' => $row->order_status,
                'created_at' => $row->created_at,
            ];

            if($row->orderable_type == "App\Event") {
                $order_data['orderable_type'] = 'Event Order';
                
            } else if($row->orderable_type == "App\OrderFood") {
                $order_data['orderable_type'] = 'Food Order';
                
            } else if($row->orderable_type == "App\FoodBar") {
                $order_data['orderable_type'] = 'Food Bar Order';
                
            } else {
                $order_data['orderable_type'] = '';
                
            }

            return $order_data;
                
        });
    }
}
