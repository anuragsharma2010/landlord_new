<?php

namespace App\Exports;

use App\Event;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class EventsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.EVENT_TITLE'),
            trans('admin.EVENT_TYPE'),
            trans('admin.EVENT_ORGANISER'),
            trans('admin.LOCATION'),
            trans('admin.EVENT_DATE_TIME'),
            trans('admin.EVENT_BUDGET'),
            trans('admin.EVENT_GUEST_COUNT'),
            trans('admin.CUISION'),
            trans('admin.DESCRIPTION'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Events';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('events')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->join('locations', 'events.location_id', '=', 'locations.id')
            ->join('budgets', 'events.budget_id', '=', 'budgets.id')
            ->join('cuisions', 'events.cuisine_id', '=', 'cuisions.id')
            ->select('events.title','events.event_type','users.name as event_organiser','locations.name as event_location', DB::raw('DATE_FORMAT(CONCAT(events.event_date, " ", events.event_time), "%m/%d/%Y %H:%i:%s") as event_datetime'),'budgets.amount','events.guest_count','cuisions.name as event_cuisine','events.description',DB::raw('DATE_FORMAT(events.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(events.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('events.created_at', 'desc')
            ->get();
    }
}
