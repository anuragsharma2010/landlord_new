<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class UsersExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.NAME'),
            trans('admin.EMAIL'),
            trans('admin.PHONE'),
            trans('admin.USER_DIETARIES'),
            trans('admin.USER_CUISINES'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Users';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $role_id = '5';
        return DB::table('users')
            ->leftJoin('user_dietaries', 'user_dietaries.user_id', '=', 'users.id')
            ->leftJoin('dietaries', 'user_dietaries.dietary_id', '=', 'dietaries.id')
            ->leftJoin('user_cuisines', 'user_cuisines.user_id', '=', 'users.id')
            ->leftJoin('cuisions', 'user_cuisines.cuisine_id', '=', 'cuisions.id')
            ->select('users.name as user_name','users.email','users.contact',DB::raw('GROUP_CONCAT( DISTINCT dietaries.name) as user_dietaries'),DB::raw('GROUP_CONCAT(DISTINCT cuisions.name) as user_cuisines'),DB::raw('(CASE WHEN users.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(users.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(users.updated_at, "%m/%d/%Y") as updated_at'))
            ->where('users.role_id', '=', $role_id)
            ->groupBy('users.id')
            ->orderBy('users.created_at', 'desc')
            ->get();
    }
}
