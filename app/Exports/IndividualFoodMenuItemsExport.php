<?php

namespace App\Exports;

use App\FoodMenuItem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class IndividualFoodMenuItemsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.FOODMENUITEM'),
            trans('admin.PARENT_FOOD_MENU'),
            trans('admin.SUB_FOOD_MENU'),
            trans('admin.DESCRIPTION'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Food Menu Items (Individual)';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('food_menu_items')
            ->join('food_menus', 'food_menu_items.food_menu_id', '=', 'food_menus.id')
            ->join('food_menus as sub_food_menus', 'food_menu_items.sub_food_menu_id', '=', 'sub_food_menus.id')
            ->select('food_menu_items.item_name as item_name','food_menus.menu_name as menu_name','sub_food_menus.menu_name as sub_menu_name','food_menu_items.description as description',DB::raw('(CASE WHEN food_menu_items.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(food_menu_items.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(food_menu_items.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('food_menu_items.created_at', 'desc')
            ->where('food_menu_items.menu_item_for', 'individual')
            ->get();
    }
}
