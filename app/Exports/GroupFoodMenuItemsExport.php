<?php

namespace App\Exports;

use App\FoodMenuItem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class GroupFoodMenuItemsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.FOODMENUITEM'),
            trans('admin.BUDGET'),
            trans('admin.CUISION'),
            trans('admin.DESCRIPTION'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Food Menu Items (Group)';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('food_menu_items')
            ->join('cuisions', 'food_menu_items.cuisine_id', '=', 'cuisions.id')
            ->join('budgets', 'food_menu_items.budget_id', '=', 'budgets.id')
            ->select('food_menu_items.item_name as item_name','budgets.amount','cuisions.name','food_menu_items.description as description',DB::raw('(CASE WHEN food_menu_items.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(food_menu_items.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(food_menu_items.updated_at, "%m/%d/%Y") as updated_at'))
            ->orderBy('food_menu_items.created_at', 'desc')
            ->where('food_menu_items.menu_item_for', 'group')
            ->get();
    }
}
