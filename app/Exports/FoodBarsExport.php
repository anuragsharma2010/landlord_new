<?php

namespace App\Exports;

use App\FoodBar;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class FoodBarsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.TITLE'),
            trans('admin.LOCATION'),
            trans('admin.DATETIME'),
            trans('admin.PRICE'),
            trans('admin.DESCRIPTION'),
            trans('admin.ATTENDIES'),
            trans('admin.EVENT_GUEST_COUNT'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Food Bars';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('food_bars')
            ->join('locations', 'food_bars.location_id', '=', 'locations.id')
            ->leftJoin('food_bar_attendees', 'food_bar_attendees.food_bar_id', '=', 'food_bars.id')
            ->select(array('food_bars.title','locations.name as food_bar_location', DB::raw('DATE_FORMAT(CONCAT(food_bars.food_bar_date, " ", food_bars.food_bar_time), "%m/%d/%Y %H:%i:%s") as food_bar_datetime'),'food_bars.price','food_bars.description', DB::raw("COUNT(food_bar_attendees.attendee_id) as attendees_count"),'food_bars.guest_count',DB::raw('(CASE WHEN food_bars.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(food_bars.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(food_bars.updated_at, "%m/%d/%Y") as updated_at')))
            ->where('food_bar_attendees.attending',1)
            ->groupBy('food_bars.id')
            ->orderBy('food_bars.created_at', 'desc')
            ->get();
    }
}
