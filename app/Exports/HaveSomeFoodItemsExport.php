<?php

namespace App\Exports;

use App\HaveSomeMenuItem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class HaveSomeFoodItemsExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            trans('admin.NAME'),
            trans('admin.PRICE'),
            trans('admin.STATUS'),
            trans('admin.CREATED_AT'),
            trans('admin.UPDATED_AT'),
        ];
    }

    public function title(): string
    {
        return 'Have Some Food Items';
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('have_some_food_items')
            ->select(array('have_some_food_items.item_name','have_some_food_items.price',DB::raw('(CASE WHEN have_some_food_items.status = "1" THEN "Active" ELSE "In-active" END) as status'),DB::raw('DATE_FORMAT(have_some_food_items.created_at, "%m/%d/%Y") as created_at'),DB::raw('DATE_FORMAT(have_some_food_items.updated_at, "%m/%d/%Y") as updated_at')))
            ->orderBy('have_some_food_items.created_at', 'desc')
            ->get();
    }
}
