<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Passport\HasApiTokens;


class Permission extends Authenticatable
{
    use Notifiable,Sortable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/
    protected $table = 'permissions';

    //protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'password'];
    protected $guarded = [];

}
