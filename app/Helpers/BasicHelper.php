<?php
namespace App\Helpers;

use Html;
use File;
use Image;
use Country;
use State;
use City;
use \App\Permission;
use \App\Menu;
use Auth;
use Illuminate\Support\Facades\Route;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class BasicFunction {

    /**
     * @description to upload images on website
     * @param 	   $image (image name to be uploading), 
     * @param 	   $uploadpath  Path for uplaoding file, 
     * @param 	   $image_prefix  prefix for image name, 
     * @param 	   $edit  if true than unlink previous uploaded image of the edit section, 
     * @param 	   $old_image  old image name we need to unlink, 
     * @return 	   image name
     */

    public static function getCurrentUrl($route = '') {
        if ($route != '') {
            $url = $route;
        }
        else {
            $url = Route::currentRouteName();
        }
        $u = explode('.', $url);
        
        if (isset($u[1]) and $u[1] != '') {
            return $u[1];
        }
        else {
            return $u[0];
        }
    }

    public static function uploadImage($image = array(), $uploadpath = '', $image_prefix = '', $edit = false, $old_image = '', $subfolder = false, $thubnail = false) {
        $image_name = '';

        if (!$image->isValid() || empty($uploadpath)) {
            return $image_name;
        }
        
        $edit_updaload_path = $uploadpath;

        if ($subfolder) {
            $year = date('Y');

            $uploadpath .= $year . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }

            $month = date('m');
            $uploadpath .= $month . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }

            $day = date('d');
            $uploadpath .= $day . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }
        }

        if ($image->isValid()) {
            $image_prefix = $image_prefix . date('d_m_Y') . '_' . rand(0, 999999999);

            $ext = $image->getClientOriginalExtension();

            $image_name = $image_prefix . '.' . $ext;

            $image->move($uploadpath, $image_name);

            if ($thubnail) {
                // $thumb_folder_name = T130;

                // if (!File::exists($uploadpath . $thumb_folder_name)) {
                //     File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                // }

                // $thumb_img = Image::make($uploadpath . $image_name);
                // $thumb_img->resize(T130_WIDTH, null, function ($constraint) {
                //     $constraint->aspectRatio();
                // })->save($uploadpath . $thumb_folder_name . $image_name);

                $thumb_folder_name = T200;

                if (!File::exists($uploadpath . $thumb_folder_name)) {
                    File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                }

                $thumb_img = Image::make($uploadpath . $image_name);
                //   $thumb_img->resize(400)
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $thumb_img->resize(T200_WIDTH, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadpath . $thumb_folder_name . $image_name);
            }
            //$image_name =   $year.'/'.$month.'/'.$day.'/'.$image_name;

            if ($edit) {
                @unlink($edit_updaload_path . $old_image);
                @unlink($edit_updaload_path . $thumb_folder_name . $old_image);
                
            }
        }
        else {
            if ($edit) {
                $image_name = $old_image;
            }
        }
        return $image_name;
    }

    public static function uploadDocuments($image = array(), $uploadpath = '', $image_prefix = '', $edit = false, $old_image = '', $subfolder = false, $thubnail = false) {
        $image_name = '';

        if (!$image->isValid() || empty($uploadpath)) {
            return $image_name;
        }
        
        $edit_updaload_path = $uploadpath;

        if ($subfolder) {
            $year = date('Y');

            $uploadpath .= $year . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }

            $month = date('m');
            $uploadpath .= $month . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }

            $day = date('d');
            $uploadpath .= $day . '/';
            if (!File::exists($uploadpath)) {
                File::makeDirectory($uploadpath, 0777, true, true);
            }
        }

        if ($image->isValid()) {
            $image_prefix = $image_prefix . date('d_m_Y') . '_' . rand(0, 999999999);

            $ext = $image->getClientOriginalExtension();

            $image_name = $image_prefix . '.' . $ext;

            $image->move($uploadpath, $image_name);

            if ($thubnail) {
                // $thumb_folder_name = T130;

                // if (!File::exists($uploadpath . $thumb_folder_name)) {
                //     File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                // }

                // $thumb_img = Image::make($uploadpath . $image_name);
                // $thumb_img->resize(T130_WIDTH, null, function ($constraint) {
                //     $constraint->aspectRatio();
                // })->save($uploadpath . $thumb_folder_name . $image_name);

                /*$thumb_folder_name = T200;

                if (!File::exists($uploadpath . $thumb_folder_name)) {
                    File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                }

                $thumb_img = Image::make($uploadpath . $image_name);
                //   $thumb_img->resize(400)
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $thumb_img->resize(T200_WIDTH, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadpath . $thumb_folder_name . $image_name);*/
            }
            //$image_name =   $year.'/'.$month.'/'.$day.'/'.$image_name;

            if ($edit) {
                @unlink($edit_updaload_path . $old_image);
                @unlink($edit_updaload_path . $thumb_folder_name . $old_image);
                
            }
        }
        else {
            if ($edit) {
                $image_name = $old_image;
            }
        }
        return $image_name;
    }

    function currentTime() {
        return time();
    }

    public static function isAllowed($role_id, $menu_id,$field = "can_index"){
        $where = "role_id='".$role_id."' AND module_id='".$menu_id."' AND $field='1'";
        return Permission::whereRaw($where)->get()->count();
    }

    public static function canAccess($controller = "", $action ="can_index"){
        //dd($controller);
        $menu = Menu::where('name','like',$controller)->get(['id'])->first();
        $menu_id = @$menu->id;
        //if($action=='can_add')die;
        $user_role_id = Auth::guard('admin')->user()->role_id;
        if($user_role_id==1)return 1;
        
        $where = "role_id='".$user_role_id."' AND module_id='".$menu_id."' AND $action='1'";
        return Permission::whereRaw($where)->get()->count();
    }

    /**
     * @description to show images on website
     * @param      $root_path , 
     * @param      $http_path, 
     * @param      $image_name  image name, 
     * @param      $attribute all attributes of image like(height,width, class),    
     * @return     image url
     * */
    public static function showImage($root_path = '', $http_path = '', $image_name = '', $attribute = array()) {
        $alt = Configure('CONFIG_SITE_TITLE');
        $title = Configure('CONFIG_SITE_TITLE');
        $height = '';
        $width = '';
        $class = '';
        $link_url = '';
        $type = '';
        $zc = '2';
        $ct = '0';
        
        if (isset($attribute['alt']) && $attribute['alt'] != '') {
            $alt = $attribute['alt'];
        }
        
        if (isset($attribute['title']) && $attribute['title'] != '') {
            $title = $attribute['title'];
        }
        
        if (isset($attribute['height']) && $attribute['height'] != '') {
            $height = $attribute['height'] . 'px';
        }

        if (isset($attribute['width']) && $attribute['width'] != '') {
            $width = $attribute['width'] . 'px';
        }
        
        if (isset($attribute['class']) && $attribute['class'] != '') {
            $class = $attribute['class'];
        }

        if (isset($attribute['url']) && $attribute['url'] != '') {
            $link_url = $attribute['url'];
        }

        // override Default zoom/crop setting of img.php file . 
        if (isset($attribute['zc']) && $attribute['zc'] != '') {
            $zc = $attribute['zc'];
        }

        if (isset($attribute['ct']) && $attribute['ct'] != '') {
            $ct = $attribute['ct'];
        }

        if (isset($attribute['type']) && $attribute['type'] != '') {
            $type = $attribute['type'];
        }

        if (file_exists($root_path . $image_name) && $image_name != '') {
            $url = WEBSITE_IMG_FILE_URL . '?image=' . $http_path . $image_name . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;

            return Html::image($url, $alt, $attributes = array('class' => $class, 'title' => $title));
            //return $this->Html->image($url, array('alt' => $alt, 'class' => $class, 'url' => $link_url, "title" => $title));
        }
        else {
            if ($type == 'banner') {
                $url = WEBSITE_IMG_FILE_URL . '?image=' . 'img/banner_no.png' . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;
            }
            else {
                $url = WEBSITE_IMG_FILE_URL . '?image=' . 'img/fabivo_no_image.png' . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;
            }

            return Html::image($url, $alt, $attributes = array('class' => $class, 'title' => $title));
        }
    }

    /**
     * @description to show images on website
     * @param 	   $root_path , 
     * @param 	   $http_path, 
     * @param 	   $image_name image name, 
     * @param 	   $attribute all attributes of image like(height,width, class), 		
     * @return 	   image url
     * */
    public static function showImageUrl($root_path = '', $http_path = '', $image_name = '', $attribute = array(), $imageUrl = false) {
        $alt = Configure('CONFIG_SITE_TITLE');
        $title = Configure('CONFIG_SITE_TITLE');
        $height = '';
        $width = '';
        $class = '';
        $link_url = '';
        $type = '';
        $zc = '2';
        $ct = '0';

        if (isset($attribute['alt']) && $attribute['alt'] != '') {
            $alt = $attribute['alt'];
        }

        if (isset($attribute['title']) && $attribute['title'] != '') {
            $title = $attribute['title'];
        }

        if (isset($attribute['height']) && $attribute['height'] != '') {
            $height = $attribute['height'] . 'px';
        }

        if (isset($attribute['width']) && $attribute['width'] != '') {
            $width = $attribute['width'] . 'px';
        }

        if (isset($attribute['class']) && $attribute['class'] != '') {
            $class = $attribute['class'];
        }

        if (isset($attribute['url']) && $attribute['url'] != '') {
            $link_url = $attribute['url'];
        }

        // override Default zoom/crop setting of img.php file . 

        if (isset($attribute['zc']) && $attribute['zc'] != '') {
            $zc = $attribute['zc'];
        }
        $zc = 2;

        if (isset($attribute['ct']) && $attribute['ct'] != '') {
            $ct = $attribute['ct'];
        } 
        else {
            $ct = 'fff';
        }

        if (isset($attribute['type']) && $attribute['type'] != '') {
            $type = $attribute['type'];
        }

        if (file_exists($root_path . $image_name) && $image_name != '') {
            if ($imageUrl) {
                $url = WEBSITE_PUBLIC_URL . $http_path . $image_name;
            } 
            else {
                $url = WEBSITE_IMG_FILE_URL . '?image=' . $http_path . $image_name . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&cc=' . $ct . '&ct=1';
            }

            return $url;
            //return $this->Html->image($url, array('alt' => $alt, 'class' => $class, 'url' => $link_url, "title" => $title));
        }
        else {
            if ($type == 'banner') {
                $noimage = 'img/banner_no.png';
            }
            else {
                $noimage = 'img/fabivo_no_image.png';
            }

            if ($imageUrl) {
                $url = WEBSITE_PUBLIC_URL . $noimage;
            } 
            else {
                $url = WEBSITE_IMG_FILE_URL . '?image=' . $noimage . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&cc=' . $ct;
            }
            // $url = WEBSITE_PUBLIC_URL . $http_path . $image_name ;
            return $url;
        }
    }

    /**
     * Generate a unique slug.
     * If it already exists, a number suffix will be appended.
     * It probably works only with MySQL.
     *
     * @link http://chrishayes.ca/blog/code/laravel-4-generating-unique-slugs-elegantly
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $value
     * @return string
     */
    public static function getUniqueSlug($model, $value) {
        //pr($model); die;
        $slug = \Illuminate\Support\Str::slug(trim($value));
        $slugCount = count($model->whereRaw("slug REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        //pr($model); die;
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public static function getParentCategory() {
        $categorylist = \App\Category::where('parent_id', '=', 0)->orderBy('name')->lists('name', 'id')->toArray();
        return $categorylist;
    }

    public static function getUserName($id) {
        $user = \App\User::where('id',$id)->first();
        return $user->name;
    }

    public static function checkEmail($email) {
        $user = \App\User::where('email',$email)->first();
        if(isset($user->id) && $user->id!=''){
            return true;
        }else{
            return false;
        }
    }

    public static function checkContact($contact) {
        $user = \App\User::where('contact',$contact)->first();
        if(isset($user->id) && $user->id!=''){
            return true;
        }else{
            return false;
        }
    }

    public static function getCategoryList() {
        $categorylist = \App\Category::where('parent_id', '=', 0)->orderBy('name')->lists('name', 'id')->toArray();

        $new_list_array = array();
        foreach ($categorylist as $key => $value) {
            $new_list_array[$key] = ucfirst($value);
            $subcategory_list = \App\Category::where('parent_id', '=', $key)->orderBy('name')->lists('name', 'id')->toArray();
            foreach ($subcategory_list as $sub_key => $sub_value) {
                $new_list_array[$sub_key] = '&nbsp;&nbsp;&nbsp;' . ucfirst($sub_value);
            }
        }
        return $new_list_array;
    }

    public static function getAllChildTags() {
        $tagslist = \App\Tag::where('status', '=', 1)->orderBy('name')->lists('name', 'id')->toArray();

        $new_list_array = array();
        foreach ($tagslist as $key => $value) {
            $subtags_list = \App\Tag::where('status', '=', $key)->orderBy('name')->lists('name', 'id')->toArray();

            $new_list_array[ucfirst($value)] = array_map('ucfirst', $subtags_list);
        }
        return $new_list_array;
    }

    public static function getAllChildCategory() {
        $categorylist = \App\Category::where('parent_id', '=', 0)->orderBy('name')->lists('name', 'id')->toArray();

        $new_list_array = array();
        foreach ($categorylist as $key => $value) {
            $subcategory_list = \App\Category::where('parent_id', '=', $key)->orderBy('name')->lists('name', 'id')->toArray();
            $new_list_array[ucfirst($value)] = array_map('ucfirst', $subcategory_list);
        }
        return $new_list_array;
    }

    public static function getAllList() {
        $mainMneuList = \App\AdminMenu::where('parent_id', '=', 0)->where('status', '=', 1)->orderBy('menu_order')->get()->toArray();

        $new_list_array = array();
        foreach ($mainMneuList as $key => $value) {
            $new_list_array[$key] = $value;
            $new_list_array[$key]['child_list'] = \App\AdminMenu::where('parent_id', '=', $value['id'])->where('status', '=', 1)->orderBy('menu_order')->get()->toArray();
        }
        return $new_list_array;
    }

    public static function getAllTagsList() {
        $tags_list = \App\Tag::where('status', '=', 1)->orderBy('name')->get()->toArray();

        $list = array();
        foreach ($tags_list as $key => $value) {
            $list[$value['id']] = ucwords($value['name']);
        }
        return $list;
    }

    public static function getAllUserList() {
        $user_list = \App\User::where('status', '=', 1)->where('role_id', '=', 2)->orderBy('first_name')->get()->toArray();

        $list = array();
        foreach ($user_list as $key => $value) {
            $list[$value['id']] = ucwords($value['first_name'] . ' ' . $value['last_name']);
        }
        return $list;
    }

    public static function getAllLandlordUserList() {
        $user_list = \App\User::where('status', '=', 1)->where('role_id', '=', 6)->orderBy('name')->get()->toArray();

        $list = array();
        foreach ($user_list as $key => $value) {
            $list[$value['id']] = ucwords($value['name'].' ('.$value['email'].') ');
        }
        return $list;
    }

    public static function getAttributeByCategory($category_id) {
        $category_attribute = \App\CategoryAttribute::where('category_id', '=', $category_id)->lists('attribute_id', 'attribute_id')->toArray();
        $attribute = \App\ Attribute::find($category_attribute)->toArray();

        $data_array = array();
        foreach ($attribute as $key => $value) {

            $data_array['data'][$key] = $value;
            $attribute_values = \App\AttributeValue::where('attribute_id', '=', $value['id'])->lists('name', 'id')->toArray();
            $data_array['data'][$key]['AttributeValue'] = $attribute_values;
        }
        return $data_array;
    }

    public static function getLastQuery() {
        if (App::environment('local')) {
            // The environment is local
            DB::enableQueryLog();
            return dd(DB::getQueryLog());
        }
        else {
            return false;
        }
    }

    public static function generateOrderId() {
        $number = 'ORD' . generateStrongPassword(17, '', 'ud'); // better than rand()
        
        // call the same function if the barcode exists already
        if (static::OrderIdExists($number)) {
            return static::generateOrderId();
        }
        // otherwise, it's valid and can be used
        return $number;
    }

    public static function OrderIdExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return \App\Order::whereOrderId($number)->exists();
    }

    public static function generateTransactionId() {
        $number = generateStrongPassword(20, '', 'ud'); // better than rand()

        // call the same function if the barcode exists already
        if (static::TransactionIdExists($number)) {
            return static::generateTransactionId();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public static function TransactionIdExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return \App\Order::whereTransactionId($number)->exists();
    }


    //country state city list starts

    public static function getCountryList() {
        $countrylist = \App\Country::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $countrylist;
    }

    public static function getFoodCategoryList() {
        $foodcategorylist = \App\FoodCategory::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $foodcategorylist;
    }

    public static function getCuisineList() {
        $cuisinelist = \App\Cuision::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $cuisinelist;
    }

    public static function getBudgetList() {
        $budgetlist = \App\Budget::where('status', '=', 1)->orderBy('amount')->pluck('amount', 'id')->toArray();
        return $budgetlist;
    }

    public static function getMainFoodMenuList() {
        $foodmenulist = \App\FoodMenu::where('status', 1)->whereNull('parent_id')->orderBy('menu_name')->pluck('menu_name', 'id');
        return $foodmenulist;
    }

    public static function getSubFoodMenuList($parent_id) {
        $foodmenulist = \App\FoodMenu::where('status', 1)->where('parent_id',$parent_id)->orderBy('menu_name')->pluck('menu_name', 'id');
        return $foodmenulist;
    }

    public static function getAllergiesList() {
        $allergieslist = \App\Allergy::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $allergieslist;
    }
    public static function getVariationList($cat_id) {
        $variationlist = \App\Variation::where('status', '=', 1)->where('food_category_id',$cat_id)->orderBy('name')->select('name', 'id')->get();
        return $variationlist;
    }
    public static function getSubVariationList($variation_id) {
         $subvariationlist = \App\SubVariation::where('variation_id',$variation_id)->where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $subvariationlist;
    }

    public static function getProtiensList() {
        $protinslist = \App\Protein::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $protinslist;
    }



    public static function getStateListByCountryId($id) {
        $statelist = \App\State::where('status', '=', 1)->where('country_id', '=', $id)->orderBy('name')->pluck('name', 'id')->toArray();
        return $statelist;
    }

    public static function getCityListByStateId($id) {
        $citylist = \App\City::where('status', '=', 1)->where('state_id', '=', $id)->orderBy('name')->pluck('name', 'id')->toArray();
        return $citylist;
    }

    public static function getLocatioList() {
        $budgetlist = \App\Location::where('status', '=', 1)->orderBy('name')->pluck('name', 'id')->toArray();
        return $budgetlist;
    }

    public static function getPermissionByRoleId($role_id) {
        $permissionList = \App\Permission::where('role_id', '=', $role_id)->orderBy('module_id')->get()->toArray();
        return $permissionList;
    }

    public static function getSettings() {
        // Get tax percentage
        $setting = \App\Setting::find(1);
        return $setting;
    }

    public static function authorizePayment($data) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(config('services.authorize.login'));
        $merchantAuthentication->setTransactionKey(config('services.authorize.key'));
        $refId = 'ref'.time();

        $op = new AnetAPI\OpaqueDataType();
        $op->setDataDescriptor($data['data_descriptor']);
        $op->setDataValue($data['data_value']);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setOpaqueData($op);

        // Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($data['amount']);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId( $refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        
        return $response;
    }
}
?>