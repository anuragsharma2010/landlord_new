<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;
use \App\Cms;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('web')->check() && !Auth::guard('vendor')->check() && !Auth::guard('user')->check())
        {
            $cms=new Cms;
            $cms_pages=$cms->get();
            $user['first_name']=null;
            $cart_count=0;
            $wishlist=null;
            $counter=1;
//            return response()->view('layouts.front.home',['cms_pages'=>$cms_pages,'user'=>$user,'cart_count'=>$cart_count,'wishlist'=>$wishlist,'counter'=>$counter]);
        }
        return $next($request);
    }
}

