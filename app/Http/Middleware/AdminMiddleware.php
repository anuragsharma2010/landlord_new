<?php
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Session;
use Visitor;
use Config;


class AdminMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
   public function handle($request, Closure $next)
    {
     /* dd(Session::has('url.intended'));
       echo $uri = $request->path();
       
       die;*/
       //$routeArray = app('request')->route()->getAction();
        if(Auth::guard('admin')->user()){
            $user_role_id = Auth::guard('admin')->user()->role_id;
            if($user_role_id==Config::get('global.role_id.landlord')){
                $controllerMethodArray = array('SettingsController.index', 'UnauthorizedController.index', 'PagesController.index', 'UsersController.profile', 'UsersController.ChangePassword', 'AuthController.getLogout', 'CmsController.index', 'CmsController.create', 'CmsController.status_change', 'CmsController.edit', 'UsersController.manage_landlord_user', 'UsersController.create_landlord_user', 'UsersController.edit_landlord_user', 'UsersController.status_change_landlord_user');

                $routeArray = $request->route()->getAction();
                $controllerAction = class_basename($routeArray['controller']);
                list($controller, $action) = explode('@', $controllerAction);

                $MatchPermission = $controller.'.'.$request->route()->getActionMethod();
                //echo $MatchPermission = $controller.'.'.$request->route()->getActionMethod(); die;

                // print_r($controller);
                // echo $request->route()->getActionMethod();
                //dd($request->route()->getAction()); 

                if(!in_array($MatchPermission, $controllerMethodArray)){
                    return redirect()->route('admin.unauthorized.index');
                    //return response()->json(['error' => 'Unauthorized.'], 403);
                }
            }
        }

        $current_path =   $request->path();

         if($current_path=='admin/logout'){
            Session::put('url.intended', 'admin/dashboard');

         }else{
             Session::put('url.intended', $request->path());
         }
    
    
   // dd(Auth::guard('admin')->user());
    if (!Auth::guard('admin')->check()) {
        if($current_path=='admin'){
                return redirect()->route('admin.login');
        }else{

            return redirect()->route('admin.login')->with('alert-error', trans('admin.NOT_LOGIN'));
        }
    }
  $response = $next($request);

    $response->headers->set('Access-Control-Allow-Origin' , '*');
    $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');
    $response->headers->set('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    $response->headers->set('Pragma','no-cache');
    $response->headers->set('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
    return $response;
 
    }

}
