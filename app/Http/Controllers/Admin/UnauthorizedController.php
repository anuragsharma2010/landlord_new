<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Business;
use App\Helpers\BasicFunction;
use Validator;

class UnauthorizedController extends Controller {

    public function index(Request $request) {
        $pageTitle = trans('admin.UNAUTHORIZED');
        $title = trans('admin.UNAUTHORIZED');
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.UNAUTHORIZED'));
        return view('admin.unauthorized.index', compact('pageTitle', 'title', 'breadcrumb'));

    }
}
