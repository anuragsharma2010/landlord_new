<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Helpers\BasicFunction;
use Validator;

class NotificationsController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
       
        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND action LIKE '%".$params['title']."%' ";
        }
        
        $notiflist = Notification::sortable(['created_at' => 'desc'])->whereRaw($where)->with('user')->paginate(Configure('CONFIG_PAGE_LIMIT'));


        $pageTitle = trans('admin.NOTIFICATIONS');
        $title = trans('admin.NOTIFICATIONS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.NOTIFICATIONS'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.notifications');

        return view('admin.notifications.index', compact('notiflist','slug', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    

   
}
