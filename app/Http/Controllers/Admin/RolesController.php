<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Helpers\BasicFunction;
use Validator;

class RolesController extends Controller {

    /*public function __construct(){
        //check is user has permissions 
         if(BasicFunction::canAccess('roles','can_index')==0){
            return redirect()->action('Admin\RolesController@index')->with('alert-error','You have not authority');
        }
        
    }*/

    public function index(Request $request) {

        if(BasicFunction::canAccess('roles','can_index')==0){
            return redirect()->action('Admin\PagesController@index')->with('alert-error','You are not authorized to access this page.');
        }

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
        //pr($params); die;
        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND name LIKE '%".$params['title']."%' ";
        }
        
        $roleslist = Role::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.ROLES');
        $title = trans('admin.ROLES');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ROLES'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.roles');

        return view('admin.roles.index', compact('roleslist', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        if(BasicFunction::canAccess('roles','can_add')==0){
            return redirect()->action('Admin\PagesController@index')->with('alert-error','You are not authorized to access this page.');
        }

        $pageTitle = trans('admin.ADD_ROLE');
        
        $title = trans('admin.ADD_ROLE');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.ROLES')] = 'admin.roles.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ROLE'));

        return view('admin.roles.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        $roleObj = new Role();
        $validator = validator::make($request->all(), [
            'name' => 'required|unique:roles|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($roleObj, $request->name);

        $role = $roleObj->create($input);
        return redirect()->action('Admin\RolesController@index', getCurrentPage('admin.roles'))->with('alert-sucess', trans('admin.ROLE_ADD_SUCCESSFULLY'));
    }

    public function edit($slug) {
        if(BasicFunction::canAccess('roles','can_edit')==0){
            return redirect()->action('Admin\PagesController@index')->with('alert-error','You are not authorized to access this page.');
        }

	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $role = Role::where('slug', $slug)->first();
        
        if (empty($role)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_ROLE');
        $title = trans('admin.EDIT_ROLE');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ROLES')] = 'admin.roles.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ROLE'));

        return view('admin.roles.edit', compact('role', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $id) {

       // pr($request->all()); die;
        if($request->old_name == $request->name){
            $checkVal = 'required|max:255';
        }else{
            $checkVal = 'required|unique:roles|max:255';
        }
        $validator = validator::make($request->all(), [
            'name' => $checkVal,
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $role = Role::findOrFail($id);
        $input = $request->all();
        unset($input['old_name']);
        $role->fill($input)->save();
        return redirect()->action('Admin\RolesController@index', getCurrentPage('admin.roles'))->with('alert-sucess', trans('admin.ROLE_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($slug, $status) {
        if(empty($slug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $role = Role::where('slug', '=', $slug)->first();
        $role->status = $new_status;
        $role->save();
        return redirect()->action('Admin\RolesController@index', getCurrentPage('admin.role'))->with('alert-sucess', trans('admin.ROLE_CHANGE_STATUS_SUCCESSFULLY'));
    }
}
