<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Building;
use App\Helpers\BasicFunction;
use Validator;

class BuildingsController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
        //pr($params); die;
        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND name LIKE '%".$params['title']."%' ";
        }
        
        $buildingslist = Building::with(['user'])->sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        //echo '<pre>';
        //print_r($buildingslist->toArray()); die;

        $pageTitle = trans('admin.BUILDING');
        $title = trans('admin.BUILDING');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.BUILDING'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.buildings');

        return view('admin.buildings.index', compact('buildingslist', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        $pageTitle = trans('admin.ADD_BUILDING');
        
        $title = trans('admin.ADD_BUILDING');

        //call basic helper
        $countries = BasicFunction::getCountryList();
        
        $states = array();
        $cities = array();
        
        // Get states of country, if country id is set
        if(!empty(old('country_id'))) {
            $states = $this->getState(old('country_id'));
        }
        
        // Get cities of state, if state id is set
        if(!empty(old('state_id'))) {
            $cities = $this->getCity(old('state_id'));
        }
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.BUILDING')] = 'admin.buildings.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_BUILDING'));

        return view('admin.buildings.create', compact('pageTitle', 'title', 'breadcrumb', 'countries','states','cities'));
    }

    public function store(Request $request) {
        $buildingsObj = new Building();
        $validator = validator::make($request->all(), [
            'user_id' => 'required',
            'name' => 'required|max:255',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'zip_code' => 'required|integer|min:4|max:10',
            
        ], ['user_id.required'=>'The landlord field is required.', 'country_id.required'=>'The country field is required.', 'state_id.required'=>'The state field is required.', 'city_id.required'=>'The city field is required.']);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($buildingsObj, $request->name);

        $buildings = $buildingsObj->create($input);
        return redirect()->action('Admin\BuildingsController@index', getCurrentPage('admin.buildings'))->with('alert-sucess', trans('admin.BUILDING_ADD_SUCCESSFULLY'));
    }

    public function view($slug) {

        if($slug == '') {
            return $this->InvalidUrl();
        }

        $buildings = Building::with('user', 'country', 'state', 'city')->where('slug', $slug)->first();

        //echo '<pre>';
        //print_r($buildings->toArray()); die;
        
        if (empty($buildings)) {
            return $this->InvalidUrl();
        }

        //call basic helper
        $countries = BasicFunction::getCountryList();
       // pr($countries); die;
        $states = $this->getState($buildings->country_id);
        $cities = $this->getCity($buildings->state_id);

        $pageTitle = trans('admin.VIEW_BUILDING');
        $title = trans('admin.VIEW_BUILDING');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.BUILDING')] = 'admin.buildings.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_BUILDING'));

        return view('admin.buildings.view', compact('buildings', 'countries','states','cities', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function edit($slug) {

	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $buildings = Building::where('slug', $slug)->first();
        
        if (empty($buildings)) {
            return $this->InvalidUrl();
        }

        //call basic helper
        $countries = BasicFunction::getCountryList();
       // pr($countries); die;
        $states = $this->getState($buildings->country_id);
        $cities = $this->getCity($buildings->state_id);

        $pageTitle = trans('admin.EDIT_BUILDING');
        $title = trans('admin.EDIT_BUILDING');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.BUILDING')] = 'admin.buildings.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_BUILDING'));

        return view('admin.buildings.edit', compact('buildings', 'countries','states','cities', 'pageTitle', 'title', 'breadcrumb'));
    }

    //get state list
    public function getState($country_id){
        $states = BasicFunction::getStateListByCountryId($country_id);
        return $states;
    }
    //get city list
    public function getCity($state_id){
        $cities = BasicFunction::getCityListByStateId($state_id);
        return $cities;
    }

    public function update(Request $request, $slug) {

       // pr($request->all()); die;
        /*if($request->old_name == $request->name){
            $checkVal = 'required|max:255';
        }else{
            $checkVal = 'required|unique:buildings|max:255';
        }
        $validator = validator::make($request->all(), [
            'name' => $checkVal,
        ]);*/

        $validator = validator::make($request->all(), [
            'user_id' => 'required',
            'name' => 'required|max:255',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'zip_code' => 'required|integer|digits_between: 4,10',
            
        ], ['user_id.required'=>'The landlord field is required.', 'country_id.required'=>'The country field is required.', 'state_id.required'=>'The state field is required.', 'city_id.required'=>'The city field is required.']);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //$buildings = Building::findOrFail($id);
        $buildings = Building::where('slug',$slug)->first();
        $input = $request->all();
        unset($input['old_name']);
        $buildings->fill($input)->save();
        return redirect()->action('Admin\BuildingsController@index', getCurrentPage('admin.buildings'))->with('alert-sucess', trans('admin.BUILDING_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($slug, $status) {
        if(empty($slug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $buildings = Building::where('slug', '=', $slug)->first();
        $buildings->status = $new_status;
        $buildings->save();
        return redirect()->action('Admin\BuildingsController@index', getCurrentPage('admin.buildings'))->with('alert-sucess', trans('admin.BUILDING_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function delete($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $buildings = Building::where('slug',$slug)->first();
        if (empty($buildings)) {
            return $this->InvalidUrl();
        }

        $buildingsData = Building::find($buildings->id);       
        if($buildingsData)
        {
            $buildingsData->delete();
            return redirect()->action('Admin\BuildingsController@index', getCurrentPage('admin.buildings'))->with('alert-sucess', trans('admin.BUILDING_DELETE_SUCCESSFULLY'));
        }
        else
        {
            return redirect()->action('Admin\BuildingsController@index', getCurrentPage('admin.buildings'));
        }
    }
}
