<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cms;
use App\Helpers\BasicFunction;
use Validator;

class CmsController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';

        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND title LIKE '%".$params['title']."%' ";
        }
        if(isset($params['description']) && $params['description']!=""){
            $form_data['description'] = $params['description'];
            $where.=" AND description LIKE '%".$params['description']."%' ";
        }

        $cmslist = Cms::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.CMS_PAGES');
        $title = trans('admin.CMS_PAGES');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CMS_PAGES'));
        setCurrentPage('admin.cms');

        return view('admin.cms.index', compact('cmslist', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        $pageTitle = trans('admin.CMS_PAGES');
        
        $title = trans('admin.CMS_PAGES');
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CMS_PAGES')] = 'admin.cms.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_CMS_PAGES'));

        return view('admin.cms.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        $cmsObj = new Cms();
        $validator = validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($cmsObj, $request->title);

        $cms = $cmsObj->create($input);
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_ADD_SUCCESSFULLY'));
    }

    public function edit($id) {

	    if($id == '') {
            return $this->InvalidUrl();
        }

        $cms = Cms::find($id);
        
        if (empty($cms)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.CMS_PAGES');
        $title = trans('admin.CMS_PAGES');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CMS_PAGES')] = 'admin.cms.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_CMS_PAGES'));

        return view('admin.cms.edit', compact('cms', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $id) {

        $validator = validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $cms = Cms::findOrFail($id);
        $input = $request->all();
        $cms->fill($input)->save();
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($id, $status) {
        if(empty($id)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $cms = Cms::where('id', '=', $id)->first();
        $cms->status = $new_status;
        $cms->save();
        return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function delete($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $cms = Cms::where('slug',$slug)->first();
        if (empty($cms)) {
            return $this->InvalidUrl();
        }

        $cmsData = Cms::find($cms->id);       
        if($cmsData)
        {
            $cmsData->delete();
            return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'))->with('alert-sucess', trans('admin.CMSPAGES_DELETE_SUCCESSFULLY'));
        }
        else
        {
            return redirect()->action('Admin\CmsController@index', getCurrentPage('admin.cms'));
        }
    }
}
