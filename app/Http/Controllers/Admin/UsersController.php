<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests;
use URL;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\CatererDetail;
use App\UserDetail;
use Illuminate\Support\Facades\Mail;
use App\Mail\Registration;
use App\Mail\AdminUserRegistration;
use App\Mail\AdminLandlordRegistration;
use App\Mail\AdminCaterRegistration;
use App\Exports\UsersExport;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
use DB;
use Validator;
use Config;
use Input;
use App\Helpers\BasicFunction;
use Auth;
use View;
use Excel;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $form_data = array();
        $params = $request->all();
        $form_data['name']='';
        $form_data['email']='';
        $form_data['contact']='';

        $where ="1 = 1";
        if(isset($params['name']) && $params['name']!=""){
            $form_data['name'] = $params['name'];
            $where.=" AND name LIKE '%".$params['name']."%' ";
        }
        if(isset($params['email']) && $params['email']!=""){
            $form_data['email'] = $params['email'];
            $where.=" AND email LIKE '%".$params['email']."%' ";
        }

        if(isset($params['contact']) && $params['contact']!=""){
            $form_data['contact'] = $params['contact'];
            $where.=" AND contact LIKE '%".$params['contact']."%' ";
        }


        //$users = User::where('role_id', '!=', Config::get('global.role_id.admin'))->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        // for caterer
        $role_id = '2';    
        
        $users = User::where('role_id', '=', $role_id)->whereRaw($where)->sortable(['id' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT')); 
        
        $pageTitle = trans('admin.CATERERS');
        $title = trans('admin.CATERERS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.USERS'));
        
        setCurrentPage('admin.CATERERS');
        
        return view('admin.users.index', compact('users','form_data', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.ADD_CATERER');
        $title = trans('admin.ADD_CATERER');

        /** breadcrumb **/
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATERERS')] = 'admin.users.index';

        //call basic helper
        $countries = BasicFunction::getCountryList();
        
        $states = array();
        $cities = array();
        
        // Get states of country, if country id is set
        if(!empty(old('country_id'))) {
            $states = $this->getState(old('country_id'));
        }
        
        // Get cities of state, if state id is set
        if(!empty(old('state_id'))) {
            $cities = $this->getCity(old('state_id'));
        }

        //$roles = Role::all();
        $roles = DB::table('roles')
                 ->where('status','=', '1')
                 ->where('name','!=', 'admin')
                 ->get()
                 ->pluck('name','id')->toArray();

        //pr($roles); die;

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_CATERER'));

        return view('admin.users.create', compact('pageTitle','countries','states','cities','roles', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userObj = new Role();
        $input = $request->all();
        
        $validator = validator::make($input, [

            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users|different:other_email',
            'other_email' => 'email|max:255',
            'contact' => 'required|unique:users|integer|digits_between: 1,10|numeric',
            'other_contact' => 'integer|digits_between: 1,10',
            'password' => 'required|same:confirm_password|min:6',
            'confirm_password' => 'required|min:6|same:password',
            'gender' => 'required',
        
        ]);

        $input['contact']  = validPhoneNumber($input['contact']);
        $input['password'] = Hash::make($request->password);
        $input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['name']);
        $input['email']    = trim($input['email']);
        $input['contact']  = trim($input['contact']);
        
        if($validator->fails()) {

            return redirect()->action('Admin\UsersController@create')
                            ->withErrors($validator)
                            ->withInput();
        }

        //$input = $request->all();
        $slug = $input['name'].rand(111, 999);
        $input['slug']     = BasicFunction::getUniqueSlug($userObj, $slug);

        $caterer['business_name']  = !empty($input['business_name'])? trim($input['business_name']) : '';
        $caterer['other_email']    = !empty($input['other_email'])? trim($input['other_email']) : '';
        $caterer['other_contact']  = !empty($input['other_contact'])? trim($input['other_contact']) : '';
        $caterer['address2']       = !empty($input['address2'])? trim($input['address2']) : '';
        $caterer['bank_account']   = !empty($input['bank_account'])? trim($input['bank_account']) : '';
        $caterer['online_account1']= !empty($input['online_account1'])? trim($input['online_account1']) : '';
        $caterer['online_account2']= !empty($input['online_account2'])? trim($input['online_account2']) : '';


        //pr($input); die;
        unset($input['confirm_password']);
        unset($input['business_name']);
        unset($input['other_email']);
        unset($input['other_contact']);
        unset($input['address2']);
        unset($input['bank_account']);
        unset($input['online_account1']);
        unset($input['online_account2']);

        if($user = User::create($input)){
            $catObj = new CatererDetail();
            $last_insert_id = $user->id;
            $caterer['user_id'] = $last_insert_id;
            //$slug = 'cd'.time();
            $caterer['slug'] = BasicFunction::getUniqueSlug($catObj, $slug);
            if($userDetail = CatererDetail::create($caterer)){
                //mail to caterer
                Mail::to($input['email'])->send(new AdminCaterRegistration($user)); 
                return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.CATERER_ADD_SUCCESSFULLY'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //$user = User::find($id);
    }


    public function view($id) {
        //die('sdfsdf');
        $users = User::find($id);
        $pageTitle = trans('admin.CATERERS');
        $title = trans('admin.CATERERS');
        /*         * breadcrumb* */

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATERERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_CATERERS'));
        return view('admin.users.view', compact('users','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }

        $user = User::with('carterDetail')->where('slug',$slug)->first();
        //pr($user); die;
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        //call basic helper
        $countries = BasicFunction::getCountryList();
       // pr($countries); die;
        $states = $this->getState($user->country_id);
        $cities = $this->getCity($user->state_id);
        
        $user->business_name    = $user->carterDetail->business_name;
        $user->other_email      = $user->carterDetail->other_email;
        $user->other_contact    = $user->carterDetail->other_contact;
        $user->address2         = $user->carterDetail->address2;
        $user->bank_account     = $user->carterDetail->bank_account;
        $user->online_account1  = $user->carterDetail->online_account1;
        $user->online_account2  = $user->carterDetail->online_account2;

       //dd($users);


        $roles = DB::table('roles')
                 ->where('status','=', '1')
                 ->where('name','!=', 'admin')
                 ->get()
                 ->pluck('name','id')->toArray();

        $pageTitle = trans('admin.EDIT_CATERER');
        $title = trans('admin.EDIT_CATERER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATERERS')] = 'admin.users.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_CATERER'));
        return view('admin.users.edit', compact('pageTitle','countries','states','cities','roles', 'title', 'breadcrumb','user'));
    }
    //get state list
    public function getState($country_id){
        $states = BasicFunction::getStateListByCountryId($country_id);
        return $states;
    }
    //get city list
    public function getCity($state_id){
        $cities = BasicFunction::getCityListByStateId($state_id);
        return $cities;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  var $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        //$user = User::findOrFail($slug);
        $user = User::where('slug',$slug)->first();
        //pr($user); die;
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        
        $input = $request->all();
        $input['contact'] = validPhoneNumber($input['contact']);
       
        if($input['old_email'] == $input['email']){
            $checkVal = 'required|email|max:255|different:other_email';
        }else{
            $checkVal = 'required|email|max:255|unique:users|different:other_email';
        }

        if($input['old_other_email'] == $input['other_email']){
            $oldcheckVal = 'email|max:255|different:email';
        }else{
            $oldcheckVal = 'email|max:255|unique:caterer_details|different:email';
        }

        if($input['old_contact'] == $input['contact']){
            $contactCheckVal = 'required|integer|digits_between: 1,10|numeric';
        }else{
            $contactCheckVal = 'required|unique:users|integer|digits_between: 1,10|numeric';
        }

        $validator = validator::make($input, [
                    'name' => 'required|max:255',
                    'email' => $checkVal,
                    'other_email' => $oldcheckVal,
                    'contact' => $contactCheckVal,
                    'other_contact' => 'integer|digits_between: 1,10',
                    'gender' => 'required',
                   
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@edit', $slug)
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        $input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['name']);
        $input['email']    = trim($input['email']);
        
        $caterer['business_name']    = !empty($input['business_name'])? trim($input['business_name']) : '';
        $caterer['other_email']    = !empty($input['other_email'])? trim($input['other_email']) : '';
        $caterer['other_contact']  = !empty($input['other_contact'])? trim($input['other_contact']) : '';
        $caterer['address2']     = !empty($input['address2'])? trim($input['address2']) : '';
        $caterer['bank_account']     = !empty($input['bank_account'])? trim($input['bank_account']) : '';
        $caterer['online_account1']     = !empty($input['online_account1'])? trim($input['online_account1']) : '';
        $caterer['online_account2']     = !empty($input['online_account2'])? trim($input['online_account2']) : '';


        unset($input['old_email']);
        unset($input['old_other_email']);
        unset($input['old_contact']);
        unset($input['business_name']);
        unset($input['other_email']);
        unset($input['other_contact']);
        unset($input['address2']);
        unset($input['bank_account']);
        unset($input['online_account1']);
        unset($input['online_account2']);
        
        if($user->fill($input)->save()){
            $user->detail_id      = $user->carterDetail->id;
            $user->business_name  = $user->carterDetail->business_name;
            $user->other_email    = $user->carterDetail->other_email;
            $user->other_contact  = $user->carterDetail->other_contact;
            $user->address2       = $user->carterDetail->address2;
            $user->bank_account   = $user->carterDetail->bank_account;
            $user->online_account1= $user->carterDetail->online_account1;
            $user->online_account2= $user->carterDetail->online_account2;
            //dd($user); die;
            $caterer['id']        = $user->detail_id;
        
            $caterer_detail = CatererDetail::where('id',$caterer['id'])->first();
            //pr($caterer); die;
            if($userDetail = $caterer_detail->fill($caterer)->save()){
               return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.CATERER_UPDATE_SUCCESSFULLY'));
            }
        }
    }


    public function profile() {
        $admin = adminUser();
        $user = User::find($admin->id);

        $pageTitle = trans('admin.MY_PROFILE');
        $title = trans('admin.MY_PROFILE');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.MY_PROFILE'));
        return view('admin.users.profile', compact('user', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function updateProfile(Request $request) {
        $admin = adminUser();

        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users,email,' . $admin->id,
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@profile')
                            ->withErrors($validator)
                            ->withInput();
        }
        $user = User::findOrFail($admin->id);
        $input = $request->all();
        $user->fill($input)->save();
        return redirect()->action('Admin\UsersController@profile')->with('alert-sucess', trans('admin.MY_PROFILE_UPDATE_SUCCESS'));
    }

    public function ChangePassword() {

        $pageTitle = trans('admin.CHANGE_PASSWORD');
        $title = trans('admin.CHANGE_PASSWORD');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.MY_PROFILE')] = 'admin.profile';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CHANGE_PASSWORD'));
        return view('admin.users.change_password', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function UpdateChangePassword(Request $request) {

        //$2y$10$sJ/gR92jcwn6xEL.dRxGG.DbDcBkiMjdjxAPJk8OBpkw0v79UmKPy

        $input = $request->all();
        
        // Validation rules and messages
        $validationRule = array(
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        );
        $validationMessage = array(
            'old_password.required' => "Old password is required",
            'new_password.required' => "New password is required",
            'confirm_password.same' => "New password and confirm password does not match"
        );

        // validate request data
        $validator = validator::make($input, $validationRule, $validationMessage);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $user = Auth::guard('admin')->user();

        if(!(Hash::check($input['old_password'], $user->password))) {

            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
 
        $user->password = bcrypt($input['new_password']);
        $user->save();

        return redirect()->back()->with("alert-sucess","Password changed successfully !");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = User::find($id)->delete();
        return redirect()->action('Admin\UsersController@index');
    }

    public function forgotPassword() {
        $pageTitle = trans('admin.CONFIG_MANAGEMENT');
        $title = trans('admin.CONFIG_MANAGEMENT');
        return view('admin.users.forgot_password', compact('pageTitle', 'title'));
    }

    public function sendPasswordLink(Request $request) {

        $validator = validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());                
        }

        $input = $request->all();

        $email = $input['email'];
        //$user = User::where('email', '=', $email)->where('role_id', '=', 1)->first();
        $user = User::where('email', '=', $email)->first();
        //pr($user); die;
        if(empty($user->id)) {
            return redirect()->back()->with("error","User not exist.please try with another email."); 
        }

        $email_token = md5(uniqid(rand(), true));

        $is_update = User::where('id', $user->id)->update(['email_token' => $email_token]);

        $view = View::make('admin.template.forgot_password', compact('user', 'email_token')); 
        $body = $view->render();
        
        $subject = 'Reset your Matrix Password';
        $to = $user->email;
        //$to = 'anuragsharma2010@gmail.com';
        EmailHelper::sendMail($to, '', '', $subject, 'default', $body);

        return redirect()->back()->with("success","Password reset link sent successfully on your registered Email address !");

    }

    function resetPassword($email_token = null) {

        if($email_token == null) {
            return redirect()->back()->with("error","Invalid email token."); 
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();

        if(empty($user->id)) {
            return redirect()->back()->with("error","User not exist."); 
        }

        $pageTitle = trans('admin.RESET_PASSWORD');
        $title = trans('admin.RESET_PASSWORD');
        return view('admin.users.reset_password', compact('pageTitle', 'title', 'email_token'));
    }

    public function resetPasswordUpdate(Request $request, $email_token) {

        $inputs = $request->all();
        
        $validator = validator::make($request->all(), [
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()) {
           return redirect()->back()->withErrors($validator->errors());      
        } 

        if($inputs['password']!=$inputs['confirm_password']){
            return redirect()->back()->with("error","New password and Confirm password does not mathched.Please try again.");
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 1)->first();

        $password = bcrypt($request->get('password'));

        $is_update = User::where('id', $user->id)
                ->update(['email_token' => '', 'password' => $password]);

        return redirect()->action('Admin\AuthController@getLogin')->with('sucess', 'Account password changed successfully');

    }

    public function status_change($slug, $status) {

        if (empty($slug)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $user = User::where('id', '=', $slug)->first();
        $user->status = $new_status;
        $user->save();
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.CATERER_CHANGE_STATUS_SUCCESSFULLY'));
    }


    public function email_verify($id, $email_verify) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        
        if ($email_verify == 0) {

            $new_emailverify = 1;
        }else{
            $new_emailverify = 0;
        }
        
        $user = User::where('id', '=', $id)->first();
        $user->email_verify = $new_emailverify;

        $user->save();

        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.USER_EMAIL_VERIFIED_SUCCESSFULLY'));

    }

    function sendCredentials($id) {

        if ($id == '') {
            return $this->InvalidUrl();
        }
        $user = User::findOrFail($id);
        if (empty($user)) {
            return $this->InvalidUrl();
        }
        $password = generateStrongPassword();
        $user->password = Hash::make($password);
        $user->save();

        $email_template = EmailTemplate::where('slug', 
            '=', 'send-login-credentials')->first();
        $email_type = $email_template->email_type;
        $subject = $email_template->subject;
        $body = $email_template->body;

        $to = $user->email;

        $login_link = WEBSITE_URL;
        $body = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL_ADDRESS}',
            '{LOGIN_LINK}',
            '{PASSWORD}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            $user->email,
            $login_link,
            $password,
                ), $body);


        $subject = str_replace(array(
            '{FIRST_NAME}',
            '{LAST_NAME}',
            '{EMAIL_ADDRESS}',
            '{LOGIN_LINK}',
            '{PASSWORD}'
                ), array(
            ucfirst($user->first_name),
            ucfirst($user->last_name),
            $user->email,
            $login_link,
            $password,
                ), $subject);


        EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);
        return redirect()->action('Admin\UsersController@index', getCurrentPage('admin.users'))->with('alert-sucess', trans('admin.CREDENTIALS_SEND_SUCCESSFULLY'));
    }


    /**
     * Display the list of prepares.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_prepare(Request $request, $slug) {

        $form_data = array();
        $params = $request->all();
        $form_data['name']='';
        $form_data['email']='';
        $form_data['contact']='';

        $where ="1 = 1";
        if(isset($params['name']) && $params['name']!=""){
            $form_data['name'] = $params['name'];
            $where.=" AND name LIKE '%".$params['name']."%' ";
        }
        if(isset($params['email']) && $params['email']!=""){
            $form_data['email'] = $params['email'];
            $where.=" AND email LIKE '%".$params['email']."%' ";
        }

        if(isset($params['contact']) && $params['contact']!=""){
            $form_data['contact'] = $params['contact'];
            $where.=" AND contact LIKE '%".$params['contact']."%' ";
        }

        // for preparer
        $role_id = '3';
        $users = User::where('role_id', '=', $role_id)->whereRaw($where)->sortable(['id' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT')); 
        //pr(Config::get('global.role_id.user')); die;

        $pageTitle = trans('admin.PREPARERS');
        $title = trans('admin.PREPARERS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATERERS')] = 'admin.users.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.PREPARERS'));
        
        setCurrentPage('admin.PREPARERS');
        
        return view('admin.users.manage_prepare', compact('users','form_data', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_prepare() {
        $pageTitle = trans('admin.ADD_CATERER');
        $title = trans('admin.ADD_CATERER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATERERS')] = 'admin.users.index';

        //$roles = Role::all();
        $roles = DB::table('roles')
                 ->where('status','=', '1')
                 ->where('name','!=', 'admin')
                 ->get()
                 ->pluck('name','id')->toArray();

        //pr($roles); die;

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_CATERER'));

        return view('admin.users.create', compact('pageTitle','roles', 'title', 'breadcrumb'));
    }


    /**
     * Display the list of normal users.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_user(Request $request) {

        $form_data = array();
        $params = $request->all();
        $form_data['name']='';
        $form_data['email']='';
        $form_data['contact']='';

        $where ="1 = 1";
        if(isset($params['name']) && $params['name']!=""){
            $form_data['name'] = $params['name'];
            $where.=" AND name LIKE '%".$params['name']."%' ";
        }
        if(isset($params['email']) && $params['email']!=""){
            $form_data['email'] = $params['email'];
            $where.=" AND email LIKE '%".$params['email']."%' ";
        }

        if(isset($params['contact']) && $params['contact']!=""){
            $form_data['contact'] = $params['contact'];
            $where.=" AND contact LIKE '%".$params['contact']."%' ";
        }

        // for user
        $role_id = '5';
        $users = User::where('role_id', '=', $role_id)->whereRaw($where)->sortable(['id' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT')); 
        //pr(Config::get('global.role_id.user')); die;

        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.USERS'));
        
        setCurrentPage('admin.USERS');
        
        return view('admin.users.manage_user', compact('users','form_data', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Display the list of Landlord users.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_landlord_user(Request $request) {

        $form_data = array();
        $params = $request->all();
        $form_data['name']='';
        $form_data['email']='';
        $form_data['contact']='';

        $where ="1 = 1";
        if(isset($params['name']) && $params['name']!=""){
            $form_data['name'] = $params['name'];
            $where.=" AND name LIKE '%".$params['name']."%' ";
        }
        if(isset($params['email']) && $params['email']!=""){
            $form_data['email'] = $params['email'];
            $where.=" AND email LIKE '%".$params['email']."%' ";
        }

        if(isset($params['contact']) && $params['contact']!=""){
            $form_data['contact'] = $params['contact'];
            $where.=" AND contact LIKE '%".$params['contact']."%' ";
        }

        // for user
        $role_id = Config::get('global.role_id.landlord');
        $users = User::where('role_id', '=', $role_id)->whereRaw($where)->sortable(['id' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT')); 
        //pr(Config::get('global.role_id.user')); die;

        $pageTitle = 'Landlord User';
        $title = 'Landlord User';
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $breadcrumb = array('pages' => $pages, 'active' => 'Landlord User');
        
        setCurrentPage('admin.USERS');
        
        return view('admin.users.manage_landlord_user', compact('users','form_data', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_user() {
        $pageTitle = trans('admin.ADD_USER');
        $title = trans('admin.ADD_USER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.manage_user';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_USER'));

        return view('admin.users.create_user', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new landlord.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_landlord_user() {
        $pageTitle = trans('admin.ADD_LANDLORD');
        $title = trans('admin.ADD_LANDLORD');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.LANDLORDS')] = 'admin.manage_landlord_user';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_LANDLORD'));

        return view('admin.users.create_landlord_user', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_user(Request $request) {

        $userObj = new User();
        $input = $request->all();

        $input['contact'] = validPhoneNumber($input['contact']);

        
        $validator = validator::make($input, [

            'name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'email' => 'required|email|max:255|unique:users',
            'dob' => 'required',
            'contact' => 'required|integer|digits_between: 1,10|numeric|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
        
        ]);

        if($validator->fails()) {
            return redirect()->action('Admin\UsersController@create_user')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $input['role_id']  = $input['role_id'];
        $input['dob']  = date('Y-m-d', strtotime($input['dob']));
        $input['name']     = trim($input['name']);
        $input['email']    = trim($input['email']);
       /* $input['latitude'] = !empty($input['latitude'])? trim($input['latitude']) : '';
        $input['longitude'] = !empty($input['longitude'])? trim($input['longitude']) : '';*/
        $slug = $input['name'].rand(111, 999);
        $input['slug']     = BasicFunction::getUniqueSlug($userObj, $slug);
        unset($input['confirm_password']);
        //pr($input); die;
        
        if($user = User::create($input)){
            $udetailObj = new UserDetail();
            $last_insert_id = $user->id;
            $user_detail['user_id'] = $last_insert_id;
            ///$slug = 'ud'.time();
            $user_detail['slug'] = BasicFunction::getUniqueSlug($udetailObj, $slug);
            if($userData = UserDetail::create($user_detail)){
                //mail to user
                $user->orignal_pass = $request->password;
                Mail::to($input['email'])->send(new AdminUserRegistration($user)); 
                
                return redirect()->action('Admin\UsersController@manage_user', getCurrentPage('admin.users.manage_user'))->with('alert-sucess', trans('admin.USER_ADD_SUCCESSFULLY'));
            }
        }
    }

    /**
     * Store a newly created resource in storage for landlord.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_landlord_user(Request $request) {

        $userObj = new User();
        $input = $request->all();

        $input['contact'] = validPhoneNumber($input['contact']);

        
        $validator = validator::make($input, [

            'name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'email' => 'required|email|max:255|unique:users',
            'contact' => 'required|integer|digits_between: 1,10|numeric|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
        
        ]);

        if($validator->fails()) {
            return redirect()->action('Admin\UsersController@create_landlord_user')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['name']);
        $input['email']    = trim($input['email']);
       /* $input['latitude'] = !empty($input['latitude'])? trim($input['latitude']) : '';
        $input['longitude'] = !empty($input['longitude'])? trim($input['longitude']) : '';*/
        $slug = $input['name'].rand(111, 999);
        $input['slug']     = BasicFunction::getUniqueSlug($userObj, $slug);
        unset($input['confirm_password']);
        //pr($input); die;
        
        if($user = User::create($input)){
            $udetailObj = new UserDetail();
            $last_insert_id = $user->id;
            $user_detail['user_id'] = $last_insert_id;
            ///$slug = 'ud'.time();
            $user_detail['slug'] = BasicFunction::getUniqueSlug($udetailObj, $slug);
            if($userData = UserDetail::create($user_detail)){
                //mail to user
                $user->orignal_pass = $request->password;
                Mail::to($input['email'])->send(new AdminLandlordRegistration($user)); 
                
                return redirect()->action('Admin\UsersController@manage_landlord_user', getCurrentPage('admin.users.manage_landlord_user'))->with('alert-sucess', trans('admin.LANDLORD_ADD_SUCCESSFULLY'));
            }
        }
    }

    public function view_user($id) {
        //die('sdfsdf');
        $users = User::find($id);
        $pageTitle = trans('admin.USERS');
        $title = trans('admin.USERS');
        /*         * breadcrumb* */

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_USERS'));
        return view('admin.usmanage_users.view_user', compact('users','pageTitle', 'title', 'breadcrumb'));
    }

    public function view_landlord_user($id) {
        //die('sdfsdf');
        $users = User::find($id);
        $pageTitle = trans('admin.LANDLORDS');
        $title = trans('admin.LANDLORDS');
        /*         * breadcrumb* */

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.LANDLORDS')] = 'admin.users.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_LANDLORD'));
        return view('admin.users.view_landlord_user', compact('users','pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_user($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }

        $user = User::where('slug',$slug)->first();

        $user->dob = date('m/d/Y', strtotime($user->dob));
        //pr($user); die;
        if (empty($user)) {
            return $this->InvalidUrl();
        }

       //dd($users);
        $roles = DB::table('roles')
                 ->where('status','=', '1')
                 ->where('name','!=', 'admin')
                 ->get()
                 ->pluck('name','id')->toArray();

        $pageTitle = trans('admin.EDIT_USER');
        $title = trans('admin.EDIT_USER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.USERS')] = 'admin.manage_user';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_USER'));
        return view('admin.users.edit_user', compact('pageTitle','roles', 'title', 'breadcrumb','user'));
    }

    /**
     * Show the form for editing the specified resource landlord.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_landlord_user($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }

        $user = User::where('slug',$slug)->first();
        //pr($user); die;
        if (empty($user)) {
            return $this->InvalidUrl();
        }

       //dd($users);
        $roles = DB::table('roles')
                 ->where('status','=', '1')
                 ->where('name','!=', 'admin')
                 ->get()
                 ->pluck('name','id')->toArray();

        $pageTitle = trans('admin.EDIT_LANDLORD');
        $title = trans('admin.EDIT_LANDLORD');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.LANDLORDS')] = 'admin.manage_landlord_user';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_LANDLORD'));
        return view('admin.users.edit_landlord_user', compact('pageTitle','roles', 'title', 'breadcrumb','user'));
    }

    /**
     * Show the form for delete the specified resource landlord.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function delete_landlord_user($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $user = User::where('slug',$slug)->first();
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        //echo '<pre>';
        //print_r($user->userDetail); die;

        $userData = User::find($user->id);       
        if($userData)
        {
            $userData->delete();
            return redirect()->action('Admin\UsersController@manage_landlord_user', getCurrentPage('admin.users.manage_landlord_user'))->with('alert-sucess', trans('admin.LANDLORD_DELETE_SUCCESSFULLY'));
        }
        else
        {
            return redirect()->action('Admin\UsersController@manage_landlord_user', getCurrentPage('admin.users.manage_landlord_user'));
        }
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  var $slug
     * @return \Illuminate\Http\Response
     */
    public function update_user(Request $request, $slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $user = User::where('slug',$slug)->first();
        
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $input = $request->all();
        //$input['contact'] = validPhoneNumber($input['contact']);
       
        // if($input['old_email'] == $input['email']){

        //     $checkVal = 'required|email|max:255|';
        // }else{
        //     $checkVal = 'required|email|max:255|unique:users';
        // }

        $validator = validator::make($input, [
            'name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'dob' => 'required|date_format:m/d/Y|before:today',
            'social_security_number' => 'required|alpha_dash',
            'marital_status' => 'required',
            'govt_id' => 'required|alpha_dash',
            //'contact' => 'required|integer|digits_between: 1,10|numeric',
        ]);


        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@edit_user', $slug)
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        $input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['name']);
        $input['dob']  = date('Y-m-d', strtotime($input['dob']));
        //$input['email']    = trim($input['email']);
        
        unset($input['old_email']);
          //pr($input); die;
        
        if($user->fill($input)->save()){
            //dd($user); die;
            if(!empty($user->userDetail)) {
                $user->detail_id    = $user->userDetail->id;
                $udata['id']        = $user->detail_id;
                $user_detail        = UserDetail::where('id',$udata['id'])->first();

                $userDetail = $user_detail->fill($udata)->save();
            }
            
            return redirect()->action('Admin\UsersController@manage_user', getCurrentPage('admin.users.manage_user'))->with('alert-sucess', trans('admin.USER_UPDATE_SUCCESSFULLY'));
        }
    }

    /**
     * Update the specified resource in storage landlord.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  var $slug
     * @return \Illuminate\Http\Response
     */
    public function update_landlord_user(Request $request, $slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $user = User::where('slug',$slug)->first();
        
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $input = $request->all();
        //$input['contact'] = validPhoneNumber($input['contact']);
       
        // if($input['old_email'] == $input['email']){

        //     $checkVal = 'required|email|max:255|';
        // }else{
        //     $checkVal = 'required|email|max:255|unique:users';
        // }

        $validator = validator::make($input, [
            'name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            //'contact' => 'required|integer|digits_between: 1,10|numeric',
        ]);


        if ($validator->fails()) {
            return redirect()->action('Admin\UsersController@edit_landlord_user', $slug)
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        $input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['name']);
        //$input['email']    = trim($input['email']);
        
        unset($input['old_email']);
          //pr($input); die;
        
        if($user->fill($input)->save()){
            //dd($user); die;
            if(!empty($user->userDetail)) {
                $user->detail_id    = $user->userDetail->id;
                $udata['id']        = $user->detail_id;
                $user_detail        = UserDetail::where('id',$udata['id'])->first();

                $userDetail = $user_detail->fill($udata)->save();
            }
            
            return redirect()->action('Admin\UsersController@manage_landlord_user', getCurrentPage('admin.users.manage_landlord_user'))->with('alert-sucess', trans('admin.LANDLORD_UPDATE_SUCCESSFULLY'));
        }
    }

    public function status_change_user($slug, $status) {
       
        if (empty($slug)) {
            return $this->InvalidUrl();
        }
        if ($status == 0) {

            $new_status = 1;
        } else {
            $new_status = 0;
        }

        $user = User::where('id', '=', $slug)->first();
        $user->status = $new_status;
        $user->status_updated_by = Config::get('global.role_id.admin');
        //pr($user, true); die;
        
        $user->save();
        //pr($user); die;
        return redirect()->action('Admin\UsersController@manage_user', getCurrentPage('admin.manage_user'))->with('alert-sucess', trans('admin.USER_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function status_change_landlord_user($slug, $status) {
       
        if (empty($slug)) {
            return $this->InvalidUrl();
        }
        if ($status == 0) {

            $new_status = 1;
        } else {
            $new_status = 0;
        }

        $user = User::where('id', '=', $slug)->first();
        $user->status = $new_status;
        $user->status_updated_by = Config::get('global.role_id.landlord');
        //pr($user, true); die;
        
        $user->save();
        //pr($user); die;
        return redirect()->action('Admin\UsersController@manage_landlord_user', getCurrentPage('admin.manage_landlord_user'))->with('alert-sucess', trans('admin.LANDLORD_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function exportData($type) {
        
        $file_name = 'matrix_users_' . date('YmdHis') . '.' . $type;

        if($type == 'xlsx') {
            return Excel::download(new UsersExport, $file_name, \Maatwebsite\Excel\Excel::XLSX);
        } else if($type == 'csv') {
            return Excel::download(new UsersExport, $file_name, \Maatwebsite\Excel\Excel::CSV);
        } else {
            return redirect()->action('Admin\UsersController@manage_user', getCurrentPage('admin.manage_user'))->with('alert-danger', trans('admin.USER_DATA_NOT_EXPORTED'));
        }
        
    }

}
