<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Product;
use App\Coupon;
use App\Cms;
use App\Order;
use App\Helpers\GlobalHelper;
use App\NewsletterSubscriber;
use App\Helpers\BasicFunction;
use Config;
use Input;
class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {    
        return view('admin.pages.index');
    }


         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  newlatters() {

        $newlatters = NewsletterSubscriber::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
  

        $pageTitle = trans('admin.NEWSLETTER_SUBSCRIBER');
        $title = trans('admin.NEWSLETTER_SUBSCRIBER');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.NEWSLETTER_SUBSCRIBER'));
        setCurrentPage('admin.newlatters');

        return view('admin.pages.newlatter', compact('newlatters', 'pageTitle', 'title', 'breadcrumb'));
        //

    } 

    function delete_newlatter($id) {

        $grades = NewsletterSubscriber::find($id)->delete();
        return redirect()->action('Admin\PagesController@newlatters')->with('alert-sucess', trans('admin.SUBSCRIBER_DELETED_SUCCESSFULLY'));
    }


}
