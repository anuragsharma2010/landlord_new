<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Address;
use App\Helpers\BasicFunction;
use Validator;

class AddressController extends Controller {

    public function index(Request $request, $slug) {
//pr($slug); die;
        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
        //pr($params); die;
        $addresslist = array();
        $address = Address::where('user_id',$slug)->select('id','user_id')->first();
        
        $where ="1 = 1";
        if(isset($address->user_id) && $address->user_id!=''){
            $user_address_id = $address->user_id; 
            $where.=" AND user_id =".$user_address_id;
            if(isset($params['title']) && $params['title']!=""){
                $form_data['title'] = $params['title'];
                $where.=" AND address1 LIKE '%".$params['title']."%' ";
            }  
            //dd($where);
        $addresslist = Address::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT')); 
        }
        
        

        $pageTitle = trans('admin.ADDRESS');
        $title = trans('admin.ADDRESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.FOOD_CATEGORIES')] = 'admin.food_categories.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADDRESS'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.address');
        $username = BasicFunction::getUserName($slug);
        return view('admin.address.index', compact('addresslist','username','slug', 'form_data', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function create($slug) {
        $pageTitle = trans('admin.ADD_ADDRESS');
        $title = trans('admin.ADD_ADDRESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.ADDRESS')] = ['admin.address.index',$slug];;
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ADDRESS'));

        return view('admin.address.create', compact('pageTitle','slug', 'title', 'breadcrumb'));
    }

    public function store(Request $request, $slug) {
        $addressObj = new Address();
        $input = $request->all();
        $validator = validator::make($input, [
            'address1' => 'required|max:255',
        ]);
        
        if($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
        }

        $input['slug'] = BasicFunction::getUniqueSlug($addressObj, $request->address1);
        //dd($addressObj);
        //dd($input);
        $address = $addressObj->create($input);
        return redirect()->action('Admin\AddressController@index', $input['user_id'])->with('alert-sucess', trans('admin.ADDRESS_ADD_SUCCESSFULLY'));
    }

    public function edit($id,$slug) {
        
	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $address = Address::where('id',$id)->where('user_id', $slug)->first();
        
        if (empty($address)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_ADDRESS');
        $title = trans('admin.EDIT_ADDRESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ADDRESS')] = ['admin.address.index',$slug];
   
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ADDRESS'));
        $username = BasicFunction::getUserName($slug);
        return view('admin.address.edit', compact('address','username','slug', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $id, $slug) {
        $input = $request->all();
       // pr($request->all()); die;
        $validator = validator::make($input, [
            'address1' => 'required|max:255',
        ]);
        
        if($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $address = Address::where('id',$id)->where('user_id',$slug)->first();
        //dd($address);
        //dd($input);
        $address->fill($input)->save();
        return redirect()->action('Admin\AddressController@index',$slug)->with('alert-sucess', trans('admin.ADDRESS_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($catSlug, $slug, $status) {
        // echo $catSlug; die;
        if(empty($slug) || empty($catSlug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $address = Address::where('slug', '=', $slug)->first();
        $address->status = $new_status;
        $address->save();
        return redirect()->action('Admin\AddressController@index',$catSlug)->with('alert-sucess', trans('admin.ADDRESS_CHANGE_STATUS_SUCCESSFULLY'));
    }
}
