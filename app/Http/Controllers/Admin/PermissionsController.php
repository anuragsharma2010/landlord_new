<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use App\Menu;
use App\Helpers\BasicFunction;
use Validator;

class PermissionsController extends Controller {

   public function index()
    {  
        if(BasicFunction::canAccess('permissions','can_index')==0){
            return redirect()->action('Admin\PagesController@index')->with('alert-error','You are not authorized to access this page.');
        }

        //$roles = Role::pluck('name','id');
        $roles = Role::where('status',1)->pluck('name','id');
        
        $permissions    = Permission::all(); 
        $menus          = Menu::all();   
        
        $pageTitle = trans('admin.PERMISSION_PAGES');
        $title = trans('admin.PERMISSION_PAGES');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.PERMISSION_PAGES'));
        setCurrentPage('admin.permissions');
         
        return view('admin.permissions.index',compact('permissions','pageTitle','title','breadcrumb','roles','menus'));
    }

    public function update(Request $request, $id)
    {
        //check is user has permissions 
        if(BasicFunction::canAccess('permissions','can_edit')==0){
            return redirect()->action('Admin\PagesController@index')->with('alert-error','You have not authority');
        }
         
        $input = $request->all();
       // dd($input);
        $count = $input['count'];
        for($i=1; $i<=$count;$i++){
            $role_id = $input['role_id'.$i];
            $menu_id = $input['menu_id'.$i];
            $can_index = isset($input['can_index'.$i])?1:0;
            $can_add = isset($input['can_add'.$i])?1:0;
            $can_edit = isset($input['can_edit'.$i])?1:0;
            $can_delete = isset($input['can_delete'.$i])?1:0;

            $permission_data = Permission::whereRaw("role_id='".$role_id."' AND module_id='".$menu_id."'")->pluck('id');
          //  dd($permission_data);
            if(isset($permission_data[0])){
                $permission_id = $permission_data[0];
                Permission::where('id', $permission_id)
                        ->update(array(
                            'can_index' => $can_index,
                            'can_add' => $can_add,
                            'can_edit' => $can_edit,
                            'can_delete' => $can_delete
                            ));
            }
            else{
                $data['role_id']=$role_id;
                $data['module_id']=$menu_id;
                $data['can_index']=$can_index;
                $data['can_add']=$can_add;
                $data['can_edit']=$can_edit;
                $data['can_delete']=$can_delete;
                //dd($data);
                Permission::create($data);
            }//pr($permission_data);
        }
        
        return redirect()->action('Admin\PermissionsController@index')->with('alert-sucess', 'Permission assigned successfully.');
    }

    public function show()
    {  
        $pageTitle = trans('admin.PERMISSION_PAGES');
        $title = trans('admin.PERMISSION_PAGES');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.PERMISSION_PAGES'));
        setCurrentPage('admin.permissions');
        return view('admin.permissions.index',compact('pageTitle','title','breadcrumb'));
    }

   
}
