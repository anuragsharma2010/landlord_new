<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Position;
use App\Helpers\BasicFunction;
use Validator;

class PositionController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
        //pr($params); die;
        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND name LIKE '%".$params['title']."%' ";
        }
        
        $positionlist = Position::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.POSITION');
        $title = trans('admin.POSITION');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.POSITION'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.position');

        return view('admin.position.index', compact('positionlist', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        $pageTitle = trans('admin.ADD_POSITION');
        
        $title = trans('admin.ADD_POSITION');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.POSITION')] = 'admin.position.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_POSITION'));

        return view('admin.position.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        $positionObj = new Position();
        $validator = validator::make($request->all(), [
            'name' => 'required|unique:position|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($positionObj, $request->name);

        $position = $positionObj->create($input);
        return redirect()->action('Admin\PositionController@index', getCurrentPage('admin.position'))->with('alert-sucess', trans('admin.POSITION_ADD_SUCCESSFULLY'));
    }

    public function edit($slug) {

	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $position = Position::where('slug', $slug)->first();
        
        if (empty($position)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_POSITION');
        $title = trans('admin.EDIT_POSITION');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.POSITION')] = 'admin.position.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_POSITION'));

        return view('admin.position.edit', compact('position', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $slug) {

       // pr($request->all()); die;
        if($request->old_name == $request->name){
            $checkVal = 'required|max:255';
        }else{
            $checkVal = 'required|unique:position|max:255';
        }
        $validator = validator::make($request->all(), [
            'name' => $checkVal,
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //$position = Position::findOrFail($id);
        $position = Position::where('slug',$slug)->first();
        $input = $request->all();
        unset($input['old_name']);
        $position->fill($input)->save();
        return redirect()->action('Admin\PositionController@index', getCurrentPage('admin.position'))->with('alert-sucess', trans('admin.POSITION_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($slug, $status) {
        if(empty($slug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $position = Position::where('slug', '=', $slug)->first();
        $position->status = $new_status;
        $position->save();
        return redirect()->action('Admin\PositionController@index', getCurrentPage('admin.position'))->with('alert-sucess', trans('admin.POSITION_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function delete($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $position = Position::where('slug',$slug)->first();
        if (empty($position)) {
            return $this->InvalidUrl();
        }

        $positionData = Position::find($position->id);       
        if($positionData)
        {
            $positionData->delete();
            return redirect()->action('Admin\PositionController@index', getCurrentPage('admin.position'))->with('alert-sucess', trans('admin.POSITION_DELETE_SUCCESSFULLY'));
        }
        else
        {
            return redirect()->action('Admin\PositionController@index', getCurrentPage('admin.position'));
        }
    }
}
