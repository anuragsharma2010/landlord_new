<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Business;
use App\Helpers\BasicFunction;
use Validator;

class BusinessController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['title']='';
        $form_data['description']='';
        //pr($params); die;
        $where ="1 = 1";
        if(isset($params['title']) && $params['title']!=""){
            $form_data['title'] = $params['title'];
            $where.=" AND name LIKE '%".$params['title']."%' ";
        }
        
        $businesslist = Business::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.BUSINESS');
        $title = trans('admin.BUSINESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.BUSINESS'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.business');

        return view('admin.business.index', compact('businesslist', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        $pageTitle = trans('admin.ADD_BUSINESS');
        
        $title = trans('admin.ADD_BUSINESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.BUSINESS')] = 'admin.business.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_BUSINESS'));

        return view('admin.business.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        $businessObj = new Business();
        $validator = validator::make($request->all(), [
            'name' => 'required|unique:business|max:255',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($businessObj, $request->name);

        $business = $businessObj->create($input);
        return redirect()->action('Admin\BusinessController@index', getCurrentPage('admin.business'))->with('alert-sucess', trans('admin.BUSINESS_ADD_SUCCESSFULLY'));
    }

    public function edit($slug) {

	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $business = Business::where('slug', $slug)->first();
        
        if (empty($business)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_BUSINESS');
        $title = trans('admin.EDIT_BUSINESS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.BUSINESS')] = 'admin.business.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_BUSINESS'));

        return view('admin.business.edit', compact('business', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $slug) {

       // pr($request->all()); die;
        if($request->old_name == $request->name){
            $checkVal = 'required|max:255';
        }else{
            $checkVal = 'required|unique:business|max:255';
        }
        $validator = validator::make($request->all(), [
            'name' => $checkVal,
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //$business = Business::findOrFail($id);
        $business = Business::where('slug',$slug)->first();
        $input = $request->all();
        unset($input['old_name']);
        $business->fill($input)->save();
        return redirect()->action('Admin\BusinessController@index', getCurrentPage('admin.business'))->with('alert-sucess', trans('admin.BUSINESS_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($slug, $status) {
        if(empty($slug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $business = Business::where('slug', '=', $slug)->first();
        $business->status = $new_status;
        $business->save();
        return redirect()->action('Admin\BusinessController@index', getCurrentPage('admin.business'))->with('alert-sucess', trans('admin.BUSINESS_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function delete($slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $business = Business::where('slug',$slug)->first();
        if (empty($business)) {
            return $this->InvalidUrl();
        }

        $businessData = Business::find($business->id);       
        if($businessData)
        {
            $businessData->delete();
            return redirect()->action('Admin\BusinessController@index', getCurrentPage('admin.business'))->with('alert-sucess', trans('admin.BUSINESS_DELETE_SUCCESSFULLY'));
        }
        else
        {
            return redirect()->action('Admin\BusinessController@index', getCurrentPage('admin.business'));
        }
    }
}
