<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Location;
use App\Helpers\BasicFunction;
use Validator;
use Input;

class LocationsController extends Controller {

    public function index(Request $request) {

        $params = $request->all();
        $form_data['name']='';
        //pr($params); die;
        $where ="1 = 1";
        if(isset($params['name']) && $params['name']!=""){
            $form_data['name'] = $params['name'];
            $where.=" AND name LIKE '%".$params["name"]."%' ";
        }
        
        $locationList = Location::sortable(['created_at' => 'desc'])->whereRaw($where)->paginate(Configure('CONFIG_PAGE_LIMIT'));

        $pageTitle = trans('admin.LOCATIONS');
        $title = trans('admin.LOCATIONS');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.LOCATIONS'));
        //pr($breadcrumb); die;
        setCurrentPage('admin.locations');

        return view('admin.locations.index', compact('locationList', 'form_data', 'pageTitle', 'title', 'breadcrumb'));

    }

    public function create() {
        
        $pageTitle = trans('admin.ADD_LOCATION');
        
        $title = trans('admin.ADD_LOCATION');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        
        $pages[trans('admin.LOCATIONS')] = 'admin.locations.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_LOCATION'));

        return view('admin.locations.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    public function store(Request $request) {
        $locationObj = new Location();
        $validator = validator::make($request->all(), [
            'name' => 'required|unique:locations|max:255'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($locationObj, $request->name);

        $location = $locationObj->create($input);
        return redirect()->action('Admin\LocationsController@index', getCurrentPage('admin.locations'))->with('alert-sucess', trans('admin.LOCATION_ADD_SUCCESSFULLY'));
    }

    public function edit($slug) {

	    if($slug == '') {
            return $this->InvalidUrl();
        }

        $location = Location::where('slug', $slug)->first();
        
        if (empty($location)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_LOCATION');
        $title = trans('admin.EDIT_LOCATION');
        
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.LOCATIONS')] = 'admin.locations.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_LOCATION'));

        return view('admin.locations.edit', compact('location', 'pageTitle', 'title', 'breadcrumb'));
    }

    public function update(Request $request, $slug) {

       // pr($request->all()); die;
        if($request->old_name == $request->name){
            $checkVal = 'required|max:255';
        }else{
            $checkVal = 'required|unique:locations|max:255';
        }
        $validator = validator::make($request->all(), [
            'name' => $checkVal
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //$allergy = Cuision::findOrFail($id);
        $location = Location::where('slug',$slug)->first();
        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($location, $request->name);
        
        unset($input['old_name']);
        $location->fill($input)->save();
        return redirect()->action('Admin\LocationsController@index', getCurrentPage('admin.locations'))->with('alert-sucess', trans('admin.LOCATION_UPDATE_SUCCESSFULLY'));
    }

    public function status_change($slug, $status) {
        if(empty($slug)) {
            return $this->InvalidUrl();
        }
        if($status == 1) {
            $new_status = 0;
        }else {
            $new_status = 1;
        }
        $location = Location::where('slug', '=', $slug)->first();
        $location->status = $new_status;
        $location->save();
        return redirect()->action('Admin\LocationsController@index', getCurrentPage('admin.locations'))->with('alert-sucess', trans('admin.LOCATION_CHANGE_STATUS_SUCCESSFULLY'));
    }
}
