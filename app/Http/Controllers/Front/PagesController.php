<?php

namespace App\Http\Controllers\Front;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cms;
use App\Helpers\BasicFunction;
use Validator;

class PagesController extends Controller {

    public function index($slug) {

        if($slug == '') {
            return $this->InvalidUrl();
        }

        $cmsData = Cms::where('slug', $slug)->first();

        //echo '<pre>';
        //print_r($cmsData->toArray()); die;
        
        if (empty($cmsData)) {
            //return $this->InvalidUrl();
            return redirect()->route('front.unauthorized.notfound');
        }

        //print_r($cmsData); die;

        $pageTitle = $cmsData->title;
        $title = $cmsData->title;
        $pages['Home'] = '/';
        $breadcrumb = array('pages' => $pages, 'active' => $cmsData->title);

        return view('front.pages.index', compact('cmsData', 'pageTitle', 'title', 'breadcrumb'));

    }
}
