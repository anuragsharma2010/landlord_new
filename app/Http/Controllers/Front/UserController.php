<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon;
use DB;
use Artisan;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use App\Helpers\BasicFunction;
use App\User;
use App\UserDetail;
use App\UploadDocument;
use Validator;
use Input;

class UserController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        /*Artisan::call('config:cache');
        Artisan::call('view:clear');*/
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    // public function getSignUp() {
    //     return view('front.user.signup');
    // }
    
    // public function getLogIn() {
    //     return view('front.user.login');
    // }

    public function account() {

        if (!Auth::check())
        {
            return $this->InvalidUrl();
        }

        $userId = Auth::User()->id;
        $user = User::with('uploadDocument')->where('id',$userId)->first();
        
        $explodeUser = explode(' ', $user->name);

        if(!empty($explodeUser[0])){
            $user->first_name = $explodeUser[0];
        }else{
            $user->first_name = '';
        }

        if(isset($explodeUser[1]) && !empty($explodeUser[1])){
            $user->last_name = $explodeUser[1];
        }else{
            $user->last_name = '';
        }

        //call basic helper
        $countries = BasicFunction::getCountryList();
       // pr($countries); die;
        $states = $this->getState($user->country_id);
        $cities = $this->getCity($user->state_id);

        $user->dob = date('d/m/Y', strtotime($user->dob));
        //pr($user); die;
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('front.MY_ACCOUNT');
        $title = trans('front.MY_ACCOUNT');
        /* * breadcrumb* */
        $pages[trans('front.HOME')] = '/';

        $breadcrumb = array('pages' => $pages, 'active' => trans('front.MY_ACCOUNT'));
        return view('front.account.index', compact('pageTitle', 'title', 'breadcrumb','user', 'countries','states','cities'));
    }

    public function profile_update(Request $request, $slug) {

        if ($slug == '') {
            return $this->InvalidUrl();
        }
        
        $user = User::where('slug',$slug)->first();
        
        if (empty($user)) {
            return $this->InvalidUrl();
        }

        $input = $request->all();
        //$input['contact'] = validPhoneNumber($input['contact']);
       
        // if($input['old_email'] == $input['email']){

        //     $checkVal = 'required|email|max:255|';
        // }else{
        //     $checkVal = 'required|email|max:255|unique:users';
        // }

        $validator = validator::make($input, [
            'first_name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'last_name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            //'email' => 'required|email|max:255|unique:users',
            'contact' => 'required|integer|digits_between: 1,10|numeric',
            'dob' => 'required|date_format:d/m/Y|before:today',
            'marital_status' => 'required',
            'social_security_number' => 'required|alpha_dash',
            'zip_code' => 'required|integer|digits_between: 1,7|numeric',
            //'govt_id' => 'required|alpha_dash',
            //'contact' => 'required|integer|digits_between: 1,10|numeric',
        ]);


        if ($validator->fails()) {
            return redirect()->action('Front\UserController@account', $slug)
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
        //$input['role_id']  = $input['role_id'];
        $input['name']     = trim($input['first_name']).' '.trim($input['last_name']);
        $input['dob']  = date('Y-m-d', strtotime($input['dob']));
        //$input['email']    = trim($input['email']);

        
        //unset($input['old_email']);
        unset($input['first_name']);
        unset($input['last_name']);
        unset($input['upload_doc']);
          //pr($input); die;
        
        if($user->fill($input)->save()){

            if(!empty($request->upload_doc)) {
                //$document_detail = new UploadDocument();
                $uploadDocumentData['doc_name'] = 'gov_document';
                $uploadDocumentData['user_id'] = $user->id;
                $uploadDocumentData['file_path'] = BasicFunction::uploadDocuments(Input::file('upload_doc'), GOVID_IMAGE_UPLOAD_DIRECTROY_PATH, '', false, '', false, true);

                //print_r($uploadDocumentData); die;

                $document_detail = UploadDocument::where('user_id',$user->id)->where('doc_name','gov_document')->first();

                if(isset($document_detail->id)){
                    $documentDetail = $document_detail->fill($uploadDocumentData)->save();
                }else{
                    $document_detail = new UploadDocument();
                    $documentDetail = $document_detail->create($uploadDocumentData);
                }
            }
            //dd($user); die;
            if(!empty($user->userDetail)) {
                $user->detail_id    = $user->userDetail->id;
                $udata['id']        = $user->detail_id;
                $user_detail        = UserDetail::where('id',$udata['id'])->first();
                $userDetail = $user_detail->fill($udata)->save();
            }
            
            return redirect()->action('Front\UserController@account')->with('alert-sucess', trans('admin.USER_UPDATE_SUCCESSFULLY'));
        }
    }

    public function profilePasswordUpdate(Request $request) {

        $inputs = $request->all();
        
        if (!Auth::check())
        {
            return $this->InvalidUrl();
        }

        $user_id = Auth::User()->id;

        $validator = validator::make($request->all(), [
            'password' => 'required|min:6|max:8',
            'password_confirmation' => 'required|min:6|max:8|same:password',
        ]);

        if($validator->fails()) {
           //return redirect()->back()->withErrors($validator->errors());
           $responseData = array('status'=>0, 'errors'=>$validator->errors());
            return response()->json($responseData);      
        } 

        if($inputs['password']!=$inputs['password_confirmation']){
            //return redirect()->back()->with("error","New password and Confirm password does not mathched.Please try again.");
            $responseData = array('status'=>0, 'errors'=>array('password'=>'New password and Confirm password does not mathched.Please try again.'));
            return response()->json($responseData); 
        }

        $user = User::where('id', '=', $user_id)->where('role_id', '=', 5)->first();

        $password = bcrypt($request->get('password'));

        $is_update = User::where('id', $user->id)->update(['email_token' => '', 'password' => $password]);

        //return redirect()->action('Admin\AuthController@getLogin')->with('sucess', 'Account password changed successfully');
        $responseData = array('status'=>1, "sucess"=>"Account password has been changed successfully.");
        return response()->json($responseData);

    }

    //get state list
    public function getState($country_id){
        $states = BasicFunction::getStateListByCountryId($country_id);
        return $states;
    }
    //get city list
    public function getCity($state_id){
        $cities = BasicFunction::getCityListByStateId($state_id);
        return $cities;
    }


}
