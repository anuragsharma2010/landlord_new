<?php

namespace App\Http\Controllers\Front;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Helpers\BasicFunction;
use App\EmailTemplate;
use App\Helpers\EmailHelper;
use Validator;
use Config;

class ContactController extends Controller {

    public function index() {

        $pageTitle = 'Contact Us';
        $title = 'Contact Us';
        $pages['Home'] = '/';
        $breadcrumb = array('pages' => $pages, 'active' => 'Contact Us');

        return view('front.contact.index', compact('pageTitle', 'title', 'breadcrumb'));

    }

    public function store(Request $request) {
        $contactObj = new Contact();
        $validator = validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'description' => 'required',
            
        ], ['description.required'=>'The message field is required.']);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        //$input['slug'] = BasicFunction::getUniqueSlug($buildingsObj, $request->name);

        $contact = $contactObj->create($input);
        
        /*********contact mail send*********/
        $email_template = EmailTemplate::where('slug', 
            '=', 'contact-us-admin')->first();
        $email_type = $email_template->email_type;
        $subject = $email_template->subject;
        $body = $email_template->body;

        $admin_emailto = Config::get('settings.CONFIG_ADMIN_EMAIL');
        //$admin_emailto = 'anurag@officebox.vervelogic.com';

        $login_link = WEBSITE_URL;
        $body = str_replace(array(
            '{NAME}',
            '{EMAIL}',
            '{MESSAGE}'
                ), array(
            ucfirst($request->first_name).' '.ucfirst($request->last_name),
            $request->email,
            $request->description,
                ), $body);


        $subject = str_replace(array(
            '{NAME}',
            '{EMAIL}',
            '{MESSAGE}'
                ), array(
            ucfirst($request->first_name).' '.ucfirst($request->last_name),
            $request->email,
            $request->description,
                ), $subject);

        EmailHelper::sendMail($admin_emailto, '', '', $subject, 'default', $body, $email_type);
        /*********contact mail send end*********/

        return redirect()->action('Front\ContactController@index', getCurrentPage('front.contact.index'))->with('alert-sucess', trans('front.CONTACT_ADD_SUCCESSFULLY'));
    }
}
