<?php

namespace App\Http\Controllers\Front;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\BasicFunction;
use Validator;


class UnauthorizedController extends Controller {

    public function index(Request $request) {
        $pageTitle = trans('front.UNAUTHORIZED');
        $title = trans('front.UNAUTHORIZED');
        $subtitle = trans('front.UNAUTHORIZED_ACCESS_DONT_PERMISSION');
        $pages['Home'] = '/';
        $breadcrumb = array('pages' => $pages, 'active' => trans('front.UNAUTHORIZED'));
        return view('front.unauthorized.index', compact('pageTitle', 'title', 'subtitle', 'breadcrumb'));
    }

    public function notfound(Request $request) {
        $pageTitle = trans('front.404NOTFOUND');
        $title = trans('front.404NOTFOUND');
        $subtitle = trans('front.PAGENOTFOUND');
        $pages['Home'] = '/';
        $breadcrumb = array('pages' => $pages, 'active' => trans('front.404NOTFOUND'));
        return view('front.unauthorized.notfound', compact('pageTitle', 'title', 'subtitle', 'breadcrumb'));
    }
}
