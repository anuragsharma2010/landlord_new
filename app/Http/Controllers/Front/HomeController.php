<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon;
use DB;
use Artisan;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Mail;
use App\Event;
use App\InviteUser;
use App\EventAttendee;
use App\Mail\ContactEnquiry;
use App\Helpers\BasicFunction;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        /*Artisan::call('config:cache');
        Artisan::call('view:clear');*/
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index() {
        return view('front.home.index');
    }

    public function updateEventInviteUserData(Request $request, $event_slug, $user_email, $attending) {

        if (!$request->hasValidSignature()) {
            return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.EVENT_INVITE_RESPONSE_URL_INVALID'));
        } else {
            $event = Event::with(['event_invitees'  => function($q) use($user_email){
                $q->where('receiver_email',$user_email);
            }])->where('slug',$event_slug)->first();

            if(!empty($event)) {
                $event = $event->toArray();
                
                if(!empty($event['event_invitees'])) {
                    if($event['event_invitees'][0]['status'] == 0) {

                        if(!empty($event['event_invitees'][0]['registered_user_id'])) {
                            $eventAttendee = EventAttendee::where('attendee_id', '=', $event['event_invitees'][0]['registered_user_id'])->where('event_id', '=', $event['id'])->first();
                    
                            if($eventAttendee){
                                $eventAttendee->attending = $attending;
                                $eventAttendee->save();
                            } else {
                                $eventAttendingData = array();
                                $eventAttendingData['event_id'] = $event['id'];
                                $eventAttendingData['attendee_id'] = $event['event_invitees'][0]['registered_user_id'];
                                $eventAttendingData['attending'] = $attending;
                                $eventAttendee = EventAttendee::create($eventAttendingData);
                            }

                            InviteUser::where('id', $event['event_invitees'][0]['id'])->update(['status' => 1]);

                            if($attending == 1) {
                                $message = trans('front.EVENT_INVITE_ATTEND_SUCCESS');
                            } else {
                                $message = trans('front.EVENT_INVITE_NOT_ATTEND_SUCCESS');
                            }

                            return redirect()->action('Front\HomeController@index')->with('alert-sucess', $message);

                        } else {
                            return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.EVENT_USER_NOT_REGISTERED'));
                        }
                        
                    } else {
                        return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.EVENT_INVITATION_EXPIRED'));
                    }
                     
                } else {
                    return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.EVENT_USER_NOT_INVITED'));
                }
                
            } else {
                return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.EVENT_NOT_EXISTS'));
            }           
            
        }   
    }

    public function sendContactEnquiry(Request $request) {
        $validator = validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'phoneno' => 'required|numeric',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ],[
            'phoneno.required' => 'Phone number field is required',
            'phoneno.numeric' => 'Phone number field must be a number',
            'g-recaptcha-response.required' => 'Captcha field is required',
            'g-recaptcha-response.recaptcha' => 'Invalid captcha field'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $enquiry_data = $request->all();

        $siteSettings = BasicFunction::getSettings();
        if(!empty($siteSettings)) {
            $invoiceStaffMail = $siteSettings['staff_mail'];
            Mail::to($invoiceStaffMail)->send(new ContactEnquiry($enquiry_data));

            return redirect()->action('Front\HomeController@index')->with('alert-sucess', trans('front.CONTACT_ENQUIRY_SUCCESS'));
        } else {
            return redirect()->action('Front\HomeController@index')->with('alert-error', trans('front.CONTACT_ENQUIRY_ERROR'));
        }
    }

    
}
