<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\User;
use App\UserDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use App\Helpers\BasicFunction;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationSuccess;
use App\EmailTemplate;
use App\Helpers\EmailHelper;

use Config;
use Input;
use View;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendemail(Request $request)
    {
        $validator = validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if($validator->fails()) {
            //return redirect()->back()->withErrors($validator->errors()); 
            $responseData = array('status'=>0, 'errors'=>$validator->errors());
            return response()->json($responseData);               
        }

        $input = $request->all();

        $email = $input['email'];
        //$user = User::where('email', '=', $email)->where('role_id', '=', 1)->first();
        $user = User::where('email', '=', $email)->where('role_id', '=', $input['role_id'])->first();
        //pr($user); die;
        if(empty($user->id)) {
            //return redirect()->back()->with("error","User not exist.please try with another email.");
            $responseData = array('status'=>0, 'errors'=>array('email'=>'User not exist.please try with another email.'));
            return response()->json($responseData); 
        }

        $email_token = md5(uniqid(rand(), true));
        //$change_pass_var = '#change-again';


        $is_update = User::where('id', $user->id)->update(['email_token' => $email_token]);

        //$email_token = array('email_token'=>$email_token,'change_pass_var'=>$change_pass_var);
        $view = View::make('front.template.forgot_password', compact('user', 'email_token')); 
        $body = $view->render();
        
        $subject = 'Reset your Matrix Password';
        $to = $user->email;
        //$to = 'anuragsharma2010@gmail.com';
        EmailHelper::sendMail($to, '', '', $subject, 'default', $body);

        //return redirect()->back()->with("success","Password reset link sent successfully on your registered Email address !");
        $responseData = array('status'=>1, 'success'=>'Password reset link sent successfully on your registered Email address !');
            return response()->json($responseData);
    }


}
