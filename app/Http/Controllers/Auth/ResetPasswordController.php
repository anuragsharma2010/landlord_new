<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use App\User;
use App\UserDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use App\Helpers\BasicFunction;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationSuccess;
use App\EmailTemplate;
use App\Helpers\EmailHelper;

use Config;
use Input;
use View;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    function resetPassword($email_token = null, $change_pass_var = null) {

        if($email_token == null) {
            return redirect()->back()->with("error","Invalid email token."); 
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 5)->first();

        if(empty($user->id)) {
            return redirect()->back()->with("error","User not exist."); 
        }
        return view('front.home.index', compact('email_token', 'change_pass_var'));
    }

    public function resetPasswordUpdate(Request $request) {

        $inputs = $request->all();
        
        $email_token = $inputs['email_token'];
        $validator = validator::make($request->all(), [
            'password' => 'required|min:6|max:8',
            'password_confirmation' => 'required|min:6|max:8|same:password',
        ]);

        if($validator->fails()) {
           //return redirect()->back()->withErrors($validator->errors());
           $responseData = array('status'=>0, 'errors'=>$validator->errors());
            return response()->json($responseData);      
        } 

        if($inputs['password']!=$inputs['password_confirmation']){
            //return redirect()->back()->with("error","New password and Confirm password does not mathched.Please try again.");
            $responseData = array('status'=>0, 'errors'=>array('password'=>'New password and Confirm password does not mathched.Please try again.'));
            return response()->json($responseData); 
        }

        $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 5)->first();

        $password = bcrypt($request->get('password'));

        $is_update = User::where('id', $user->id)
                ->update(['email_token' => '', 'password' => $password]);

        //return redirect()->action('Admin\AuthController@getLogin')->with('sucess', 'Account password changed successfully');
        $responseData = array('status'=>1, "sucess"=>"Account password changed successfully.");
        return response()->json($responseData);

    }
}
