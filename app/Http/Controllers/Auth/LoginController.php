<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
//use Validator;
//use Config;
//use Input;
//use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $input = $request->all();

        $validator = validator::make($request->all(),[
            'email' => 'required|email', 'password' => 'required|min:6',
        ]);

        $responseData = '';
            
        if($validator->fails()) {
            //return redirect()->back()->withErrors($validator->errors());
            $responseData = array('status'=>0, 'errors'=>$validator->errors());
            return response()->json($responseData);          
        }

        $users = User::with('role')->where('email',$input['email'])->where('role_id',$input['role_id'])->where('status',1)->first();
        if(!empty($users)){
            $credentials = ['email' => $request->input('email'), 'password' => $request->input('password'), 'role_id' => $request->input('role_id')];
            if (Auth::guard('web')->attempt($credentials, $request->has('remember'))) {
            $responseData = array('status'=>1, "sucess"=>"User login successfully.");
            return response()->json($responseData);
            //return redirect()->intended($this->redirectPath())->with('alert-sucess', 'User login successfully.');
        } else {
                    $responseData = array('status'=>0, "errors"=>array("email"=>"These credentials do not match our records."));
                    return response()->json($responseData);
                    //$request->session()->flash('error', 'Fill correct credentials');
                    //return redirect()->route('index');
                }
        }else{
            $responseData = array('status'=>0, "errors"=>array("email"=>"You are not authorized user."));
            return response()->json($responseData);
            //return redirect()->back()->with("error","You are not authorized user.");
        }

        //return redirect()->back()->with("error","Invalid Login Credentials.");
        //return response()->json($responseData);
    }
}
