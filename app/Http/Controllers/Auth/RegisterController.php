<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use App\Helpers\BasicFunction;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationSuccess;
use App\EmailTemplate;
use App\Helpers\EmailHelper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function register(Request $request)
    {
        $userObj = new User();
        $input = $request->all();
        $responseData = '';

        //$input['contact'] = validPhoneNumber($input['contact']);

        $validator = validator::make($input, [
            'first_name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'last_name' => 'required|max:255|regex:/^[a-zA-Z0-9\s]+$/',
            'email' => 'required|email|max:255|unique:users',
            'dob' => 'required',
            'contact' => 'required|integer|digits_between: 1,10|numeric',
            'password' => 'required|min:6|max:8',
            'password_confirmation' => 'required|min:6|max:8|same:password',
        ]);

        if($validator->fails()) {
            //return redirect()->action('Admin\UsersController@create_user')->withErrors($validator)->withInput();
            $responseData = array('status'=>0, 'errors'=>$validator->errors());
            return response()->json($responseData);
        }

        //$input = $request->all();
        $input['name']     = trim($input['first_name']).' '.trim($input['last_name']);
        $input['password'] = Hash::make($request->password);
        $input['email']    = trim($input['email']);
        $input['role_id']  = $input['role_id'];
        $input['contact']  = $input['contact'];
        $input['dob']  = date('Y-m-d', strtotime($input['dob']));
       /* $input['latitude'] = !empty($input['latitude'])? trim($input['latitude']) : '';
        $input['longitude'] = !empty($input['longitude'])? trim($input['longitude']) : '';*/
        $slug = trim($input['name']).rand(111, 999);
        $input['slug']     = BasicFunction::getUniqueSlug($userObj, $slug);
        unset($input['password_confirmation']);
        unset($input['first_name']);
        unset($input['last_name']);
        //pr($input); die;
        
        if($user = User::create($input)){
            $udetailObj = new UserDetail();
            $last_insert_id = $user->id;
            $user_detail['user_id'] = $last_insert_id;
            ///$slug = 'ud'.time();
            $user_detail['slug'] = BasicFunction::getUniqueSlug($udetailObj, $slug);
            if($userData = UserDetail::create($user_detail)){
                //mail to user
                $user->orignal_pass = $request->password;
                Mail::to($input['email'])->send(new RegistrationSuccess($user)); 
                
                //return redirect()->action('Admin\UsersController@manage_user', getCurrentPage('admin.users.manage_user'))->with('alert-sucess', trans('admin.USER_ADD_SUCCESSFULLY'));
                $responseData = array('status'=>1, "sucess"=>"User register successfully.");
                return response()->json($responseData);
            }
        }
        
    }


}
