<?php 
return [
	'status_list' => array("1"=>"Active","0"=>"Deactive"),
	
	'visibility' => array("Inactive"=>"Inactive","Active"=>"Active","Not Visible"=>"Not Visible"),
	
	'role_id' => array("admin"=>1, "user"=>2, "craterer"=>3, "preparer"=>4, "deliverer"=>5, "landlord"=>6),
	
	'file_max_size' => 2048,// in kb
	
	'image_mime_type' => 'jpeg,gif,jpg,png',// image  extention
	
	'html_entity_decode_option'=>array('remove' => false,'charset' => 'UTF-8','quotes' => ENT_QUOTES,'double' => true),
	
	'category_type' => array("main_category"=>0,"sub_category"=>1),
	
	'menu_type' => array("dynamic"=>0,"cms_page"=>1),
	
	'menu_position' => array("1"=>'Header',"2"=>'Footer'),
	
	'approve_list' => array("1"=>"Yes","0"=>"No"),

	'show_items' => array("0"=>"All","10"=>"10","50"=>"50","100"=>"100"),

	'variation_type' => array("0"=>"Single","1"=>"Multiple"),

	'event_type' => array("public"=>"Public","private"=>"Private"),

	"order_status"=>array(0=>'in process',1=>'delivered',2=>'cancelled',3=>'rejected'),

	'food_menu_for' => array("group"=>'Group',"individual"=>'Individual'),

	'event_time' => array("11:30"=>'11:30 A.M.',"12:00"=>'12 Noon'),

	"payment_type"=>array("credit_card"=>'Credit Card', "pay_later"=>'Pay Later'),
	
	"marital_status"=>array(''=>'Marital Status', '1'=>'Single', '2'=>'Married', '3'=>'Divorced', '4'=>'Widow'),
];