<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <?php echo $__env->make('includes.front.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body>
        <!-- Header -->
        <?php echo $__env->make('includes.front.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Main Content -->
        <?php echo $__env->yieldContent('content'); ?>
    
        <!-- Footer -->
        <?php echo $__env->make('includes.front.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('includes.front.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>
