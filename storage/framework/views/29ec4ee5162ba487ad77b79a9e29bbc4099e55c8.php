<?php $__env->startSection('content'); ?>  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1><?php echo e($pageTitle); ?></h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::model($user,['method'=>'patch','route'=>['admin.update_user',$user->slug],'files'=>true]); ?>

            <div class="block full">
               
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.NAME'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]); ?>

                            <div class="error"><?php echo e($errors->first('name')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                        <?php echo Form::label(trans('admin.PROFILE_IMAGE'),null); ?>

                        <?php if(!empty($user->profile_image)): ?>
                            <img height="100" width="100" src="<?php echo e(USER_IMAGE_URL . $user->profile_image); ?>" alt="User Profile Image" />
                        <?php else: ?>
                            <br>No Image
                        <?php endif; ?>      
                        
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.DOB'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('dob',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control dateofbirth','placeholder'=>trans('admin.DOB')]); ?>

                            <div class="error"><?php echo e($errors->first('dob')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.SOCIAL_SECURITY_NUMBER'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('social_security_number',null,['minlength'=>1,'class'=>'form-control','placeholder'=>trans('admin.SOCIAL_SECURITY_NUMBER')]); ?>

                            <div class="error"><?php echo e($errors->first('social_security_number')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.GOVTID'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('govt_id',null,['minlength'=>1, 'class'=>'form-control','placeholder'=>trans('admin.GOVTID')]); ?>

                            <div class="error"><?php echo e($errors->first('govt_id')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.MARITAL_STATUS'),null,['class'=>'required_label']); ?> 
                            <?php $marital_status_list = Config::get('global.marital_status'); ?>
                            <?php echo Form::select('marital_status', $marital_status_list, null, ['class' => 'form-control select2 autocomplete']); ?>

                            <div class="error"><?php echo e($errors->first('marital_status')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    
                </div><!-- /.row -->

                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.EMAIL'),null); ?>

                            <?php echo Form::text('email',null,['id'=>'email','class'=>'form-control','placeholder'=>trans('admin.EMAIL'), 'readonly'=>true]); ?>

                            <div class="error"><?php echo e($errors->first('email')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
             
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.PHONE'),null); ?>

                            <?php echo Form::text('contact',null,['id'=>'contact','minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE'), 'readonly'=>false]); ?>

                            <div class="error"><?php echo e($errors->first('contact')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.STATUS'),null,['class'=>'required_label']); ?>

                            <?php $status_list = Config::get('global.status_list'); ?>
                            <?php echo Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']); ?>

                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
                
                <input type="hidden" name="role_id" value="5">
                <input type="hidden" name="old_email" value="<?php echo e($user->email); ?>">
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            <?php echo Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info ']); ?>

                            <?php echo Form::reset(trans('admin.RESET'),['class' => 'btn btn-default ']); ?>

                        </div>
                    </div>
                </div>

                
            </div>
        <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<!-- END Page Content -->
<script type="text/javascript">
    $(document).ready(function(){
        $("#email").prop('disabled', true);
        $("#contact").prop('disabled', true);

        $('.dateofbirth').datepicker({
            format: 'mm/dd/yyyy'
        });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>