<?php $__env->startSection('content'); ?>  

    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1><?php echo e($pageTitle); ?></h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo Form::open(['method'=>'delete','method'=>'get','route'=>['admin.locations.index']]); ?>


                <div class="search_table clearfix" style="margin-bottom: 30px; ">
                    <span style="float: left; width: 50%;">
                        <?php echo Form::text('name', $form_data['name'], ['class'=>'form-control', 'placeholder'=>'Location Name', 'style'=>'float:left; width: 60%' ]); ?>

                        <button class="btn btn-primary" style="float: left" type="submit">Search</button>
                    </span>
                    
                    <h3 class="pull-right" style="margin: 0; float: right;">  
                        <?php echo Html::decode(Html::link(route('admin.locations.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?>

                    </h3>
                </div>                         
                
            <?php echo Form::close(); ?>


            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('name', trans('admin.NAME')));?></th>
                            <th width="12%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('created_at', trans('admin.CREATED_AT')));?></th>
                            <th width="12%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('updated_at', trans('admin.UPDATED_AT')));?></th>
                            <th  width="15%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!$locationList->isEmpty()): ?>
                            <?php $__currentLoopData = $locationList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(ucfirst($location->name)); ?></td>
                                    <td><?php echo e(date_val($location->created_at,MDY_FORMATE )); ?></td>
                                    <td><?php echo e(date_val($location->updated_at,MDY_FORMATE )); ?></td>
                                    <td align="center">
                                        <?php if($location->status == 1): ?>
                                            <?php echo Html::decode(Html::link(route('admin.locations.status_change',['slug' => $location->slug,'status'=>$location->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])); ?>

                                        <?php else: ?>
                                            <?php echo Html::decode(Html::link(route('admin.locations.status_change',['slug' => $location->slug,'status'=>$location->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])); ?>

                                        <?php endif; ?>

                                        <?php echo Html::decode(Html::link(route('admin.locations.edit', $location->slug),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])); ?>

                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php echo $locationList->appends(Input::all('page'))->render(); ?>

            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>