    


<?php $__env->startSection('content'); ?>  

        <!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
                <i class="fa fa-cube"></i> <strong>Welcome to <?php echo e(config('settings.CONFIG_SITE_TITLE')); ?></strong>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuickInv">
                <!-- Login Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <!--<?php echo Html::link(route('admin.forgot_password'), '<i class="fa fa-exclamation-circle"></i>', array('class' => 'btn btn-effect-ripple btn-primary', 'data-toggle'=>'tooltip', 'data-placement'=>'left', 'title'=>'Forgot your password?')); ?>-->

                        <a href="<?php echo e(route('admin.forgot_password')); ?>" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Forgot your password?"><i class="fa fa-exclamation-circle"></i></a>
                        <!--<a href="page_ready_register.html" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Create new account"><i class="fa fa-plus"></i></a>-->
                    </div>
                    <h2>Please Login</h2>
                </div>
                <!-- END Login Title -->

                <div class="row">
                    <div class="col-md-12">
                        <?php if(session('error')): ?>
                          <div class="alert alert-danger">
                              <?php echo e(session('error')); ?>

                          </div>
                        <?php endif; ?>
                          <?php if(session('success')): ?>
                              <div class="alert alert-success">
                                  <?php echo e(session('success')); ?>

                              </div>
                          <?php endif; ?>
                    </div>
                </div>

                <!-- Login Form -->
                <?php echo Form::open(array('url' =>  URL::to('admin/login'), 'id' => 'form-login', 'class'=>'form-horizontal')); ?>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <?php echo Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','id'=>'email']); ?>

                            <div class="error" class="server-side help-block animation-slideUp"><?php echo e($errors->first('email')); ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                        <?php echo Form::password('password',['class'=>'form-control','placeholder'=>'Password','id'=>'password']); ?>

                        <div class="error" class="server-side help-block animation-slideUp"><?php echo e($errors->first('password')); ?></div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-8">
                            <?php echo Html::link(route('admin.forgot_password'), 'Forgot Password', array('class' => 'btn btn-default btn-flat')); ?>

                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo Form::submit('Let\'s Go',['class'=>'btn btn-effect-ripple btn-sm btn-primary']); ?>

                        </div>
                    </div>
                <?php echo Form::close(); ?>

                <!-- END Login Form -->
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="javascript:void(0);" target="_blank"><?php echo e(config('settings.CONFIG_SITE_TITLE')); ?></a></small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->
<style type="text/css">
.error{
    font-size: 12px;
    color: red;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.blank', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>