<!-----------Header---------------->

<header class="header-wrapper" id="header">
        <div class="container">
            <nav class="nav-wraper navs-in">
                <a href="<?php echo e(url('/')); ?>" class="brand"> <img src="<?php echo e(asset('public/front/images/home/logo.jpg')); ?>" alt="logo"></a>
                
                <div class="nav-content">
                    <button type="button" class="close m-0" id="closest">
                        <span aria-hidden="true" class="font-clrff">×</span>
                    </button>
                    <div class="nav-cinner">
                        <ul class="links-controls">
                            <li>
                                <a href="#" class="active font1">For Renter</a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('about-us')); ?>" class="font1">About Us</a>
                            </li>
                             <li>
                                <a href="<?php echo e(url('for-management')); ?>" class="font1">For Management</a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('pages/contact-us')); ?>" class="font1">Contact Us</a>
                            </li> 
                        </ul>
                    </div>
                </div>
                <div class="user-name align-self-center ">
                    <ul class="homeheader d-flex">
                    <?php if(auth()->guard()->guest()): ?>
                        <li>
                            <a class="font18 font1" onclick="OpenModal('#login', '#home-tab')">Sign In</a>
                        </li>
                        <li class="font18 font1">
                            <a onclick="OpenModal('#login', '#profile-tab')">Sign Up</a>
                        </li>
                    <?php else: ?>
                    <li>
                            <a class="font18 font1" href="<?php echo e(url('account')); ?>"><?php echo e(ucwords(Auth::user()->name)); ?></a>
                        </li>
                        <li class="font18 font1">
                            <a class="" href="<?php echo e(route('logout')); ?>"    
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                <?php echo csrf_field(); ?>
                            </form>
                        </li>
                    <?php endif; ?>
                    </ul>
                </div>
                <button class="bars"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </nav>
        </div>
</header>

<!-----------Header-end---------------->