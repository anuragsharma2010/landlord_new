<?php $__env->startSection('content'); ?>  
<style type="text/css">
    .nav-tabs-custom {
        margin-bottom: 20px;
        background: #fff;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        border-radius: 3px;
     }

    .nav-tabs-custom>.nav-tabs {
        margin: 0;
        border-bottom-color: #f4f4f4;
        border-top-right-radius: 3px;
        border-top-left-radius: 3px;
     }

    .nav-tabs-custom>.nav-tabs>li:first-of-type {
        margin-left: 0;
    }

    .nav-tabs-custom>.nav-tabs>li {
        border-top: 3px solid transparent;
        margin-bottom: -2px;
        margin-right: 5px;
    }

    ul.permissions_ul li {
        padding: 10px 15px;
        width: 180px;
       
    }
</style>
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1><?php echo e($pageTitle); ?></h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <?php echo Form::model($permissions, ['route' => ['admin.permissions.update', 1], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-settings']); ?>

                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs permissions_ul">
                        <li>&nbsp;</li>
                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role_key=>$role_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($role_key!=1): ?>
                            <li><?php echo e($role_value); ?></li>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    <div class="tab-content">
                        <!-- Start Global Settings -->
                        <div class="tab-pane active" id="global_settings">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $count = 1;?>
                                    <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <ul class="nav nav-tabs permissions_ul">
                                    <li><?php echo e($menu->name); ?></li>
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role_key=>$role_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($role_key!=1): ?>
                                    <li>
                                    <?php   $can_index = BasicFunction::isAllowed($role_key,$menu->id,'can_index');
                                            $can_add = BasicFunction::isAllowed($role_key,$menu->id,'can_add');
                                            $can_edit = BasicFunction::isAllowed($role_key,$menu->id,'can_edit');
                                            $can_delete = BasicFunction::isAllowed($role_key,$menu->id,'can_delete');
                                            if($can_index==1 && $can_add==1 && $can_edit==1 && $can_delete==1){
                                                $all_check= 1;
                                            }else{
                                                $all_check= 0;
                                            }
                                    ?>
                                        <?php echo Form::checkbox('select_all'.$count,1,$all_check,array('class'=>'all_checkbox')); ?> All<br>
                                        <?php echo Form::checkbox('can_index'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_index')); ?> List<br>
                                        <?php echo Form::checkbox('can_add'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_add')); ?> Add<br>
                                        <?php echo Form::checkbox('can_edit'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_edit')); ?> Edit<br>
                                        <?php echo Form::checkbox('can_delete'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_delete')); ?> Delete<br>
                                        <?php echo Form::hidden('role_id'.$count,$role_key); ?>

                                        <?php echo Form::hidden('menu_id'.$count,$menu->id); ?>

                                    </li>
                                    <?php $count++;?>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>       
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                </div>
                            </div><!-- /.col -->


                     </div>
                    <!-- END Global Settings -->

                    <?php echo Form::hidden('count',$count-1); ?>

                    <?php echo Html::link(route('admin.permissions.index'), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-left']); ?>

                    

                    <?php echo Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info pull-right']); ?>

                    </div>
                <!-- /.tab-content -->
                
            </div>
            <!-- nav-tabs-custom -->
         

            <?php echo Form::close(); ?>


        </div><!-- /.col -->

           
        </div>
        <!-- END Datatables Block -->
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.all_checkbox').on('click',function(){
            if($(this).is(":checked")){
                $(this).parent().find('input[type=checkbox]').prop('checked',true);
            }else{
                $(this).parent().find('input[type=checkbox]').prop('checked',false);
            }
        });
    });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>