<?php $__env->startSection('content'); ?>  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1><?php echo e($pageTitle); ?></h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block full">
               
                <div class="row">

                    <div class="col-md-12">

                        <div class="row form-group">

                        <center><h2><?php echo e(trans('admin.UNAUTHORIZED_ACCESS')); ?></h2>
                        <h4><?php echo e(trans('admin.UNAUTHORIZED_ACCESS_DONT_PERMISSION')); ?></h4></center>
                            
                        </div>
                        <!-- /.form-group -->

                    </div><!-- /.col -->

                </div><!-- /.row -->
                
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php echo Html::script(asset('admin/js/plugins/ckeditor/ckeditor.js')); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>