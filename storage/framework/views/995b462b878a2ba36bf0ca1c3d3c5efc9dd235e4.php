Dear <?php echo e($user->name); ?>,<br>

You recently requested to reset your password for your matrix account.Click the link below to reset it.
<br>
<a href="<?php echo e(url('password/reset_password', ['email_token'=>$email_token, 'change_pass_var'=>'change-again'])); ?>"><?php echo e(url('password/reset_password', ['email_token'=>$email_token, 'change_pass_var'=>'change-again'])); ?></a> 

<p style=" font-size: 16px; font-weight: 200 ; color: #F5070B">You are advised to change the password at next login to ensure safety of your account and data.</p>
<strong>To login, please click</strong> <a href="<?php echo e(url('/')); ?>" style="text-decoration: none ;"><?php echo e(url('/')); ?></a>

<p><strong>Note:</strong> In case you have not requested a password change / forgotten your password, please ensure that your account remains safe by logging in to check.</p>