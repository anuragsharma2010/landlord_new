<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="initial-scale=1.0" />
      <meta name="format-detection" content="telephone=no" />
      <title></title>
      <style type="text/css">
p { font-size: 13px;}
ul { margin: 0px; padding: 0px;}
li { display: inline-block ;}
li a { list-style: none ; text-decoration: none ; color: #000 ;}
.detail { padding:10px 0}
.detail ul {}
.detail ul li {font-size: 13px; display: block ; padding:0 0 ; padding:4px 5px ;border-bottom: 1px solid #ccc;}
.detail ul li a { }
.detail ul li strong { font-size: 13px;}
.detail ul li p { margin:0 ; padding:0 ; font-size: 13px; }
</style>
   </head>
  <body style="background-color:#E4E6E9">
   <div style="width:100%;" align="center">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td align="center" valign="top">
           <table width="600" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td align="center" valign="top" bgcolor="#fff" style="background-color:#fff; padding:0 20px ; color:#000; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><br><br><br>
                 <div style="color:#000;text-align:left;line-height:25px;">
                    <?php echo $body; ?> 
                  </div>
               <p>This is system generated notification. Please do not reply to this e-mail address</p>

               <p><strong>Disclaimer:</strong> This message and the information contained herein is proprietary and confidential and Subject to Parking Privacy Policy.</p>
               <br><br><br><br><br>
             </td>
           </tr>
         </table>
       </td>
     </tr>
     </table>
   </div>
   </body>
</html>

