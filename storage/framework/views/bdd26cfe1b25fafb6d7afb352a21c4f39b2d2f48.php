<!DOCTYPE html>
<html lang="en">
    <?php echo $__env->make('includes.admin.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('includes.admin.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="login-page">
        <?php echo $__env->yieldContent('content'); ?>
        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.3.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <?php echo Html::script( asset('admin/js/vendor/bootstrap.min.js')); ?>

        <?php echo Html::script( asset('admin/js/plugins.js')); ?>

        <?php echo Html::script( asset('admin/js/app.js')); ?>


        <!-- Load and execute javascript code used only in this page -->
        <?php echo Html::script( asset('admin/js/pages/readyLogin.js')); ?>

        <script>
            $(function () {
                ReadyLogin.init();
            });
        </script>
        
        <?php echo Html::script( asset('admin/js/pages/readyReminder.js')); ?>

        <script>
            $(function () {
                ReadyReminder.init();
            });
        </script>
    </body>
</html>