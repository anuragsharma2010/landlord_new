<?php $__env->startSection('content'); ?>  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1><?php echo e($pageTitle); ?></h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::model($buildings,['method'=>'patch','route'=>['admin.buildings.update',$buildings->slug],'files'=>true]); ?>

            <div class="block full">
               
                <div class="row">

                    <div class="col-md-12">

                        <div class="form-group col-md-6">
                             <?php echo Form::label(trans('admin.LANDLORD_NAME'),null,['class'=>'required_label']); ?>

                             <?php $LandlordUserList = BasicFunction::getAllLandlordUserList(); ?>
                            <?php echo Form::select('user_id', $LandlordUserList, null, ['class' => 'form-control select2', 'placeholder'=>'--- Select Landlord ---', 'tab-index'=>'4']); ?>

                            <div class="error"><?php echo e($errors->first('user_id')); ?></div>
                        </div>

                        <div class="col-md-6 form-group ">
                            <?php echo Form::label(trans('admin.BUILDING'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.BUILDING')]); ?>

                            <div class="error"><?php echo e($errors->first('name')); ?></div>
                        </div><!-- /.form-group -->

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label(trans('admin.COUNTRY'),null,['class'=>'required_label']); ?>

                                <?php echo Form::select('country_id', [''=>'--- Select Country ---'] + $countries, null, ['class' => 'form-control select2 autocomplete']); ?>

                                <div class="error"><?php echo e($errors->first('country_id')); ?></div>
                            </div><!-- /.form-group -->
                        </div><!-- /.col --> 

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label(trans('admin.STATE'),null,['class'=>'required_label']); ?>

                                <?php echo Form::select('state_id', $states, null, ['class' => 'form-control select2 autocomplete', 'placeholder'=>'--- Select State ---']); ?>

                                <div class="error"><?php echo e($errors->first('state_id')); ?></div>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label(trans('admin.CITY'),null,['class'=>'required_label']); ?>

                                <?php echo Form::select('city_id', [''=>'--- Select City ---'] + $cities, null, ['class' => 'form-control select2 autocomplete']); ?>

                                <div class="error"><?php echo e($errors->first('city_id')); ?></div>
                            </div>
                        </div>

                        <div class="col-md-6 form-group ">
                            <?php echo Form::label(trans('admin.ZIP_CODE'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('zip_code',null,['class'=>'form-control','placeholder'=>trans('admin.ZIP_CODE')]); ?>

                            <div class="error"><?php echo e($errors->first('zip_code')); ?></div>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6">
                            <?php echo Form::label(trans('admin.STATUS'),null,['class'=>'required_label']); ?>

                            <?php $status_list = Config::get('global.status_list'); ?>
                            <?php echo Form::select('status', $status_list, null, ['class' => 'form-control']); ?>

                            <div class="error"><?php echo e($errors->first('status')); ?></div>
                        </div><!-- /.form-group -->

                    </div><!-- /.col -->

                </div><!-- /.row -->
                <?php echo Form::hidden('old_name',$buildings->name,['class'=>'form-control','placeholder'=>'old_name']); ?>

                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                <?php echo Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info ']); ?>

                <?php echo Form::reset(trans('admin.RESET'),['class' => 'btn btn-default ']); ?>

                        </div>
                    </div>
                </div>
                
            </div>
        <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<!-- END Page Content -->
<?php echo Html::script(asset('admin/js/plugins/ckeditor/ckeditor.js')); ?>


<script>
    $(function() {
        $('select[name=country_id]').change(function() {

            //alert($(this).val());

            var url = '<?php echo e(url("admin/country")); ?>'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name= state_id]');

                select.empty();
                $('form select[name= city_id]').empty();
                $('form select[name= city_id]').append('<option value="">--- Select City ---</option>');
                select.append('<option value="">--- Select State ---</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });
    });
</script>

<script>
    $(function() {
        $('select[name=state_id]').change(function() {

            //alert($(this).val());

            var url = '<?php echo e(url("admin/state")); ?>'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name= city_id]');

                select.empty();
                select.append('<option value="">--- Select City ---</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>