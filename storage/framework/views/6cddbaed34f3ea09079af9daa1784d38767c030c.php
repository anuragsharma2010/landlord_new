<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo e(isset($title) ? config('settings.CONFIG_SITE_TITLE')." :: ".$title : config('settings.CONFIG_SITE_TITLE')); ?></title>

    <meta charset="UTF-8" name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.png')); ?>">

    <meta name="Description" content="VlGrocery" />
    <meta name="robots" content="index, follow" />
    <meta name="Language" content="English" />

    <link rel="shortcut icon" href="<?php echo e(asset('public/admin/img/favivo.png')); ?>">

    <meta property="og:title" content="Your title here" />
    <meta property="og:url" content="http://www.yourdomain.com" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Your 160-character description here" />
    <meta property="og:image" content="http://www.yourdomain.com /image-name.jpg" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Your title here" />
    <meta name="twitter:description" content="Your 160-character description here" />
    <meta name="twitter:url" content="http://www.yourdomain.com" />
    <meta name="twitter:image" content="http://www.yourdomain.com /image-name.jpg" />

    <link rel="canonical" href="https://www.matrix.com/" />

    <?php echo Html::style( asset('public/front/css/bootstrap.min.css')); ?>

    <?php echo Html::style( asset('public/front/css/font-awesome.min.css')); ?>

    <?php echo Html::style( asset('public/front/css/owl.theme.default.min.css')); ?>

    <?php echo Html::style( asset('public/front/css/owl.carousel.min.css')); ?>

    <?php echo Html::style( asset('public/front/css/jquery.noty.css')); ?>

    <?php echo Html::style( asset('public/front/css/noty_theme_default.css')); ?>

    <?php echo Html::style( asset('public/front/css/style.css')); ?>

    <?php echo Html::style( asset('public/front/css/custom.css')); ?>


    <script src="<?php echo e(asset('public/front/js/jquery.min.js')); ?>"></script>
</head>