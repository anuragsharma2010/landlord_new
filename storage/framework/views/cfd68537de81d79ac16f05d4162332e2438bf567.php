<?php $__env->startSection('content'); ?>  

<!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1><?php echo e($pageTitle); ?></h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo Form::open(['method'=>'delete','method'=>'get','route'=>['admin.manage_user']]); ?>                         
            <div class="search_table clearfix" style="margin-bottom: 30px; ">
                <span style="float: left; width: 90%;">
                    <?php echo Form::text('name', $form_data['name'], ['class'=>'form-control', 'placeholder'=>'Name', 'style'=>"float:left;width: 20%; margin-right:10px;"]); ?>

                    <?php echo Form::text('email', $form_data['email'], ['class'=>'form-control', 'placeholder'=>'Email', 'style'=>"float:left; width: 20%; margin-right:10px;"]); ?>

                    <?php echo Form::text('contact', $form_data['contact'], ['class'=>'form-control', 'placeholder'=>'Phone', 'style'=>"float:left; width: 20%; margin-right:10px;"]); ?>

                    <button class="btn btn-primary" style="float: left" type="submit">Search</button>
                </span>
                
                <h3 class="pull-right" style="margin: 0; float: right;">  
                    <?php echo Html::decode(Html::link(route('admin.create_user'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?>

                    <!-- <?php echo Html::decode(Html::link(route('admin.users.exportData', 'xlsx'),"<i class='fa fa-file-excel-o' aria-hidden='true'></i>",['class'=>'btn btn-info', 'title'=> 'Export to excel'])); ?>

                    <?php echo Html::decode(Html::link(route('admin.users.exportData', 'csv'),"<i class='fa fa-file-text' aria-hidden='true'></i>",['class'=>'btn btn-info', 'title'=> 'Export to csv'])); ?> -->
                </h3>
            </div>
            <?php echo Form::close(); ?>


            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('title', trans('admin.NAME')));?></th>
                            <th width="10%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('role', trans('admin.ROLE')));?></th>
                            <th width="10%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('email', trans('admin.EMAIL')));?></th>
                            <th width="10%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('phone', trans('admin.PHONE')));?></th>
                            <th width="10%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('created_at', trans('admin.CREATED_AT')));?></th>
                            <th width="10%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('updated_at', trans('admin.UPDATED_AT')));?></th>
                            <th  width="15%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php if(!$users->isEmpty()): ?>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(ucfirst($user->name)); ?></td>
                                <td><?php if($user->role_id==2): ?> Caterer <?php endif; ?> <?php if($user->role_id==3): ?> Preparer <?php endif; ?> <?php if($user->role_id==4): ?> Deliverer <?php endif; ?> <?php if($user->role_id==5): ?> User <?php endif; ?></td>
                                <td><?php echo e($user->email); ?></td>
                                <td><?php echo e($user->contact); ?></td>
                                <td><?php echo e(date_val($user->created_at,MDY_FORMATE )); ?></td>
                                <td><?php echo e(date_val($user->updated_at,MDY_FORMATE )); ?></td>
                                <td>
                                    <?php if($user->status == 1): ?>
                                        <?php echo Html::decode(Html::link(route('admin.status_change_user',['id' => $user->id,'status'=>$user->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])); ?>

                                    <?php else: ?>
                                        <?php echo Html::decode(Html::link(route('admin.status_change_user',['id' => $user->id,'status'=>$user->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])); ?>

                                    <?php endif; ?>

                                    <?php echo Html::decode(Html::link(route('admin.edit_user', $user->slug),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])); ?>

                                    
                                    <!-- <?php echo Html::decode(Html::link(route('admin.address.index', $user->id),"<i class='fa  fa-plus'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.ADD_ADDRESS')])); ?> -->

                                </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php echo $users->appends(Input::all('page'))->render(); ?>

            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
    <!-- END Page Content -->
    <style type="text/css">
        #page-content .block form {
            padding: 15px 0px;
        }

        table.search_table td{
            padding: 1px!important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>