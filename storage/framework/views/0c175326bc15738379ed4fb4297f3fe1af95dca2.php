<?php $__env->startSection('content'); ?>
<section class="about-wrapper pt-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-page-title text-center py-sm-5">
                    <h2 class="font45 font2 color20 fontSemiBold"><?php echo e($title); ?></h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pagination-outer">
                    <div class="pagi-inner">
                        <?php echo $__env->make('includes.front.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $cmsData->description; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>