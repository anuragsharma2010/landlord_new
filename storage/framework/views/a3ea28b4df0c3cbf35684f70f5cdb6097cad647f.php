<?php $__env->startSection('content'); ?>
<section class="about-wrapper pt-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-page-title text-center py-sm-5">
                    <h2 class="font45 font2 color20 fontSemiBold"><?php echo e($title); ?></h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pagination-outer">
                    <div class="pagi-inner">
                        <?php echo $__env->make('includes.front.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us-content pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="keep-in-touch">
                        <h2 class="font45 color20 fontBold keep-line pb-2">Get In Touch</h2>
                        <?php echo Form::open(['route'=>'front.contact.store','files'=>true, 'class'=>'contact-details']); ?>

                            <div class="form-row">
                                <div class="form-group col-md-6 pl-0 pr-3">
                                    <?php echo Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name']); ?>

                                    <div class="error"><?php echo e($errors->first('first_name')); ?></div>
                                </div>
                                <div class="form-group col-md-6 pl-0 ">
                                    <?php echo Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name']); ?>

                                    <?php echo Form::hidden('status',1,['class'=>'form-control']); ?>

                                    <div class="error"><?php echo e($errors->first('last_name')); ?></div>
                                </div>
                                 <div class="form-group col-md-6 px-0 pr-3">
                                    <?php echo Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']); ?>

                                    <div class="error"><?php echo e($errors->first('email')); ?></div>
                                </div>
                                <div class="form-group col-md-12 px-0">
                                    <?php echo Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Message']); ?>

                                    <div class="error"><?php echo e($errors->first('description')); ?></div>
                                </div>
                                <div class="form-group col-md-12 px-0">
                                    <?php echo Form::submit(trans('admin.SUBMIT'),['class' => 'btn-one font1 mt-2 ']); ?>

                                </div>    
                            </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="keep-in-touch">
                        <h2 class="font45 color20 fontBold keep-line pb-2">Contact Info</h2>
                        <div class="corporate-office">
                            <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-phone font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Call us now</small>
                                    <a href="tel:516-707-3773" class="font18 color20 font1"><?php echo e(config('settings.CONFIG_CONTACT_SUPPORT_PHONE')); ?></a>
                                </div>
                            </div>  
                        </div>
                        <div class="corporate-office mt-4">
                           <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-envelope font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Send us message</small>
                                    <a href="mailto:info@mymatrixrent.com" class="font18 color20 font1"><?php echo e(config('settings.CONFIG_CONTACT_SUPPORT_EMAIL')); ?></a>
                                </div>
                            </div>  
                       </div>
                       <div class="corporate-office mt-4">
                           <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-map-marker font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Visit our office</small>
                                   <address class="font18 color20 font1"><?php echo e(config('settings.CONFIG_CONTACT_SUPPORT_TITLE')); ?></address>
                                </div>
                            </div>  
                       </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>