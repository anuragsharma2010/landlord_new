<?php $__env->startSection('content'); ?>  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1><?php echo e($pageTitle); ?></h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(['route'=>'admin.locations.store']); ?>

            <div class="block full">

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.NAME'),null,['class'=>'required_label']); ?>

                            <?php echo Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]); ?>

                            <div class="error"><?php echo e($errors->first('name')); ?></div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo Form::label(trans('admin.STATUS'),null,['class'=>'required_label']); ?>

                            <?php $status_list = Config::get('global.status_list'); ?>
                            <?php echo Form::select('status', $status_list, null, ['class' => 'form-control']); ?>

                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            <?php echo Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info ']); ?>

                            <?php echo Form::reset(trans('admin.RESET'),['class' => 'btn btn-default ']); ?>

                        </div>
                    </div>
                </div>
               
            </div>
        <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<!-- END Page Content -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>