<?php if(isset($breadcrumb)): ?>
<ul class="d-flex justify-content-center">
    <?php $__currentLoopData = $breadcrumb['pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(is_array($pages)): ?>
            <li><?php echo Html::decode(Html::linkAsset(route($pages[0],$pages[1]), $key, array('class'=>'font18 font1 color20'))); ?></li>
        <?php else: ?>
            <li><?php echo Html::decode(Html::linkAsset(route($pages), $key, array('class'=>'font18 font1 color20'))); ?></li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <li><a href="" class="font18 font1 color20 px-2">></a></li>
    <li><a href="javascript:void(0);" class="font18 font1 color20 active-pg"><?php echo e($breadcrumb['active']); ?></a></li>
</ul>
<?php endif; ?>