<?php $__env->startSection('content'); ?>

<!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
                <i class="fa fa-history"></i> <strong>Password Reminder</strong>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuickInv">
            <p class="login-box-msg">You need to provide your registered email here.</p>
                <!-- Login Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <!--<?php echo Html::link(route('admin.forgot_password'), '<i class="fa fa-exclamation-circle"></i>', array('class' => 'btn btn-effect-ripple btn-primary', 'data-toggle'=>'tooltip', 'data-placement'=>'left', 'title'=>'Forgot your password?')); ?>-->

                        <a href="<?php echo e(route('admin.login')); ?>" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Back to login"><i class="fa fa-user"></i></a>
                        <!--<a href="page_ready_register.html" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Create new account"><i class="fa fa-plus"></i></a>-->
                    </div>
                    <h2>Forgot Password</h2>
                </div>
                <!-- END Login Title -->

                <div class="row">
                    <div class="col-md-12">
                        <?php if(session('error')): ?>
                          <div class="alert alert-danger">
                              <?php echo e(session('error')); ?>

                          </div>
                        <?php endif; ?>
                          <?php if(session('success')): ?>
                              <div class="alert alert-success">
                                  <?php echo e(session('success')); ?>

                              </div>
                          <?php endif; ?>
                    </div>
                </div>

                <!-- Login Form -->
                <?php echo Form::open(array('route' =>  'admin.forgot_password', 'id' => 'form-reminder', 'class'=>'form-horizontal')); ?>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <?php echo Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','id'=>'email']); ?>

                            <div class="error" class="server-side help-block animation-slideUp"><?php echo e($errors->first('email')); ?></div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-6">
                            <?php echo Html::link(route('admin.login'), 'Cancel', array('class' => 'btn btn-default   btn-flat pull-left')); ?>

                        </div>
                        <div class="col-xs-6 text-right">
                            <?php echo Form::submit('Remind Password',['class'=>'btn btn-effect-ripple btn-sm btn-primary']); ?>

                        </div>
                    </div>
                <?php echo Form::close(); ?>

                <!-- END Login Form -->
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="javascript:void(0);" target="_blank"><?php echo e(config('settings.CONFIG_SITE_TITLE')); ?></a></small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->

    <!-- jQuery 2.1.4 -->
    <!--<?php echo Html::script(asset('backend/bootstrap/js/bootstrap.min.js')); ?>

    <?php echo Html::script(asset('backend/js/jquery.noty.js')); ?>-->
<style type="text/css">
.error{
    font-size: 12px;
    color: red;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.blank', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>