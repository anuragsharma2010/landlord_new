<?php $__env->startSection('content'); ?>  
<style type="text/css">
    .search_table{
        width: 100%
    }
    .td_class{
        width: 20%;
    }
</style>
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1><?php echo e($pageTitle); ?></h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo Form::open(['method'=>'delete','method'=>'get','route'=>['admin.cms.index']]); ?>                         
                <table class="search_table">
                    <tbody>
                        <tr>
                            <td class="td_class"><?php echo Form::text('title', $form_data['title'], ['class'=>'form-control', 'placeholder'=>'title']); ?></td>
                            <td class="td_class"><?php echo Form::text('description', $form_data['description'], ['class'=>'form-control', 'placeholder'=>'Description']); ?></td>
                            <td><button class="btn btn-primary" type="submit">Search</button></td>
                            <td>
                                <h3 class="pull-right">  
                                    <?php echo Html::decode(Html::link(route('admin.cms.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?>

                                </h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php echo Form::close(); ?>


            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('title', trans('admin.TITLE')));?></th>
                            <th width="45%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('description', trans('admin.DESCRIPTION')));?></th>
                            <th width="12%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('created_at', trans('admin.CREATED_AT')));?></th>
                            <th width="12%"><?php echo \Kyslik\ColumnSortable\SortableLink::render(array ('updated_at', trans('admin.UPDATED_AT')));?></th>
                            <th  width="15%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!$cmslist->isEmpty()): ?>
                            <?php $__currentLoopData = $cmslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(ucfirst($cms->title)); ?></td>
                                    <td><?php echo e(str_limit($cms->description, 80, '...')); ?></td>
                                    <td><?php echo e(date_val($cms->created_at,MDY_FORMATE )); ?></td>
                                    <td><?php echo e(date_val($cms->updated_at,MDY_FORMATE )); ?></td>
                                    <td align="center">
                                        <?php if($cms->status == 1): ?>
                                            <?php echo Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])); ?>

                                        <?php else: ?>
                                            <?php echo Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])); ?>

                                        <?php endif; ?>

                                        <?php echo Html::decode(Html::link(route('admin.cms.edit', $cms->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])); ?>


                                        <?php echo Html::decode(Html::link(route('admin.cms.delete', $cms->slug),"<i class='fa  fa-trash'></i>",['class'=>'btn btn-danger','data-toggle'=>'tooltip','title'=>trans('admin.DELETE')])); ?>

                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php echo $cmslist->appends(Input::all('page'))->render(); ?>

            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>