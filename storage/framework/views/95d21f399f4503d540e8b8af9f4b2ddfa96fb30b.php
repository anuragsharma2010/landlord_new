<?php if(isset($breadcrumb)): ?>
<ul class="breadcrumb breadcrumb-top">
    <?php $__currentLoopData = $breadcrumb['pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(is_array($pages)): ?>
            <li><?php echo Html::decode(Html::linkAsset(route($pages[0],$pages[1]), $key)); ?></li>
        <?php else: ?>
            <li><?php echo Html::decode(Html::linkAsset(route($pages), $key)); ?></li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <li class="active"><?php echo e($breadcrumb['active']); ?></li>
</ul>
<?php endif; ?>