<?php $__env->startSection('content'); ?>

<!-----------Banner-Start---------------->

    <section class="banner-wrapper">
        <div id="carouselExampleIndicators" class="carousel slide position-relative" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active position-relative">
                <div class="overlay"></div>
                  <img class="d-block w-100" src="<?php echo e(asset('public/front/images/banner-one.png')); ?>" alt="banner-one">
                  <div class="banner-title">
                        <div class="banner-title-inner">
                            <h1 class="bnr-ttle font2">Matrix Rental Solutions</h1>
                            <p class="font20 colorWhite font1">The first universal rental application built to help both renters and landlords</p>
                        </div>
                        <div class="btn-link">
                          <a href="" class="btn-one font1">Apply Now</a>
                        </div>
                  </div>
                </div>
                <div class="carousel-item">
                    <div class="overlay"></div>
                  <img class="d-block w-100" src="<?php echo e(asset('public/front/images/banner-one.png')); ?>" alt="banner-two">
                  <div class="banner-title">
                        <div class="banner-title-inner">
                            <h1 class="bnr-ttle font2">Matrix Rental Solutions</h1>
                            <p class="font20 colorWhite font1">The first universal rental application built to help both renters and landlords</p>
                        </div>
                        <div class="btn-link">
                          <a href="" class="btn-one font1">Apply Now</a>
                        </div>
                  </div>
                </div>
                <div class="carousel-item">
                    <div class="overlay"></div>
                  <img class="d-block w-100" src="<?php echo e(asset('public/front/images/banner-one.png')); ?>" alt="banner-three">
                  <div class="banner-title">
                        <div class="banner-title-inner">
                            <h1 class="bnr-ttle font2">Matrix Rental Solutions</h1>
                            <p class="font20 colorWhite font1">The first universal rental application built to help both renters and landlords</p>
                        </div>
                        <div class="btn-link">
                          <a href="" class="btn-one font1">Apply Now</a>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bnr-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="up-section">
                        <div class="row">
                            <div class="col-md-6 mb-sm-3 col-xl-4 col-lg-4">
                                <div class="btm-box-one">
                                    <div class="img-section">
                                        <span class="allinone bet-one"></span>
                                    </div> 
                                    <div class="content-btm">
                                        <h4 class="font22 font2">Better Experience</h4>
                                        <p class="font16 font1">
                                            Matrix provides a fully digital experience that can be completed in 30 minutes or less, from your mobile, tablet or desktop. Login, send a pic, upload a document. Your call!
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-sm-3 col-xl-4 col-lg-4">
                                <div class="btm-box-one">
                                    <div class="img-section">
                                        <span class="allinone bet-two"></span>
                                    </div> 
                                    <div class="content-btm">
                                        <h4 class="font22 font2">Faster Turnaround</h4>
                                        <p class="font16 font1">
                                            Once your application is completed, our technology reads your documents and sends the complete readable file directly to a decision maker.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xl-4 col-lg-4">
                                <div class="btm-box-one">
                                    <div class="img-section">
                                        <span class="allinone bet-three"></span>
                                    </div> 
                                    <div class="content-btm">
                                        <h4 class="font22 font2">1 Application & 1 Credit Check</h4>
                                        <p class="font16 font1">
                                            You want your dream unit sooner than later and if you don’t get it, no sweat! Find a building in our network... at no additional cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <!-----------Banner-End---------------->


    <section class="how-it-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="how-title text-center">
                        <h2 class="font2 pb-3">How It Works</h2>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <div class="how-box-one text-center">
                        <div class="how-img">
                            <span class="allinone h-two"></span>
                            <h4 class="font2">Application</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3 align-self-end">
                    <div class="how-box-one text-center">
                        <div class="how-img">
                            <span class="allinone h-one"></span>
                            <h4 class="font2">Credit Check</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <div class="how-box-one text-center">
                        <div class="how-img">
                            <span class="allinone h-three"></span>
                            <h4 class="font2">Fee Period</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 my-4">
                    <div class="no-circle">
                        <ul class="d-flex justify-content-center">
                            <li class="font2">
                                1
                            </li>
                             <li class="font2 mid">
                                2
                            </li>
                             <li class="font2">
                                3
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 mt-5">
                    <div class="row">
                        <div class="col-md-4">
                                <ul class="right-wrong">
                                    <li class="d-flex">
                                        <span class="green"></span>
                                        <p class="font1 align-self-center">
                                            If Approved, Great! Finalize the details to move into your new home
                                        </p>
                                    </li>
                                    <li class="d-flex">
                                        <span class="red-cross font2">×</span>
                                        <p class="font1 align-self-center">If Not Approved, No Problem!</p>
                                    </li>
                                </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="btm-text">
                                <p class="font1 font18 m-0">
                                    Based on your profile and our network of buildings, we will suggest buildings that you may be a match for.
                                </p>
                                <p class="font1 font18 m-0 py-2">
                                    Simply identify the building you wish to apply for, communicate with a rental agent and submit your application through Matrix. 
                                </p>
                                <p class="font1 font18 m-0">
                                    No additional cost, no additional paperwork!
                                </p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="services-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="how-title text-center">
                        <h2 class="font2 mb-5 pb-3">Our Services</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="services-inner" style="background-image: url('<?php echo e(asset('public/front/images/ser-bg.png')); ?>')">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ser-content">
                            <div class="img-ser">
                                <img src="<?php echo e(asset('public/front/images/Application.png')); ?>" alt="">
                            </div>
                            <div class="title-ser how-img">
                                <h4 class="font2">One Application</h4>
                                <p class="font1 font18">
                                    The universal application gives you access to apartment buildings across the United States. 
                                </p>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ser-content">
                            <div class="img-ser">
                                <img src="<?php echo e(asset('public/front/images/Fee.png')); ?>" alt="">
                            </div>
                            <div class="title-ser how-img">
                                <h4 class="font2">One Fee</h4>
                                <p class="font1 font18">
                                    The universal application gives you access to apartment buildings across the United States. 
                                </p>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ser-content">
                            <div class="img-ser">
                                <img src="<?php echo e(asset('public/front/images/Credit Check.png')); ?>" alt="">
                            </div>
                            <div class="title-ser how-img">
                                <h4 class="font2">One Credit Check</h4>
                                <p class="font1 font18">
                                    The universal application gives you access to apartment buildings across the United States. 
                                </p>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </section>
    <section class="feedback-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="how-title text-center">
                        <h2 class="font2 mb-5 pb-3">What People Say About Us</h2>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="feed-owl">
                        <div class="fe-owl owl-carousel owl-theme">
                            <div>
                                <div class="row-name d-flex">
                                    <div class="col-name w-50 bg-slider"></div>
                                    <div class="col-name w-50">
                                        <div class="client-info">
                                            <div class="name">
                                                <h3 class="font22 font2">Dummy Name</h3>
                                                <small class="pb-2 pt-2">Designation</small>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    Lorem ipsum dolor sit amet, sed do utmx drao eiusmod ut. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row-name d-flex">
                                    <div class="col-name w-50 bg-slider"></div>
                                    <div class="col-name w-50">
                                        <div class="client-info">
                                            <div class="name">
                                                <h3 class="font22 font2">Dummy Name</h3>
                                                <small class="pb-2 pt-2">Designation</small>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    Lorem ipsum dolor sit amet, sed do utmx drao eiusmod ut. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row-name d-flex">
                                    <div class="col-name w-50 bg-slider"></div>
                                    <div class="col-name w-50">
                                        <div class="client-info">
                                            <div class="name">
                                                <h3 class="font22 font2">Dummy Name</h3>
                                                <small class="pb-2 pt-2">Designation</small>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    Lorem ipsum dolor sit amet, sed do utmx drao eiusmod ut. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row-name d-flex">
                                    <div class="col-name w-50 bg-slider"></div>
                                    <div class="col-name w-50">
                                        <div class="client-info">
                                            <div class="name">
                                                <h3 class="font22 font2">Dummy Name</h3>
                                                <small class="pb-2 pt-2">Designation</small>
                                            </div>
                                            <div class="content">
                                                <p>
                                                    Lorem ipsum dolor sit amet, sed do utmx drao eiusmod ut. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>