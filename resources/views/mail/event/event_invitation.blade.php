<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Event Invitation Matrix</title>

</head>

<body style="background:#eee; font-family:arial; margin: 0px;">

	<div style="max-width: 800px; margin: 0 auto; text-align: center;">
 
  <table border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:20px;box-shadow: 2px 2px 5px #ccc; width: 100%; display: inline-block; margin: 0 auto">
  		
		<div class="bdr">
			<tr>
				<td colspan="4" style="text-align: center;"> <img src="{{ PUBLIC_FOLDER_URL . 'images/logo_120_138.png' }}" alt="Matrix"></td>
			</tr>
			<!-- <tr>
				<td colspan="4" style="font-size: 14px; text-align:left; padding:10px; background:#db1522; margin-bottom: 10px; color:#fff; float: left; font-weight: bold; border-radius:2px;">TOMORROW</td>
			</tr> -->
			<tr>
				<td colspan="4" style="width:100%; font-size:20px; padding-bottom: 10px; font-weight: bold; color: #000;">{{ ucwords($event_data['event_data']['title']) }}</td>				
			</tr>
			<tr>
				<td colspan="4" style="width:100%; font-size:13px; padding-bottom: 10px; border-bottom: solid 1px #a2a2a2; font-weight: bold; color: #a2a2a2;">{{ $event_data['event_data']['description'] }}</td>				
			</tr>
			<tr>
				<td colspan="4" style="width:100%; font-size:13px;line-height:20px; font-weight: bold; color: #000;">
					@php 
            $event_date = date('l, M d, Y', strtotime($event_data['event_data']['event_date']));

            $event_time_12hr = date('h', strtotime($event_data['event_data']['event_time']));

            if($event_time_12hr == '12'){
              $event_time_12hr = $event_time_12hr . ' Noon';
            } else {
              $event_time_12hr = date('h:i A', strtotime($event_data['event_data']['event_time']));
            }
            
          @endphp
					
					{{ $event_date }} <br> 

          {{ $event_time_12hr }}
				</td>					
			</tr>			
			<tr>
				<td colspan="4" style="width:100%; font-size:13px;line-height:20px; color: padding-bottom: 10px; font-weight: bold; color: #000;">{{ $event_data['event_data']['event_location']['name'] }}<br>
					<!-- <span style="color: #3194d2">Got one?</span> --></td>					
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;background: #eee;width:160px; box-shadow: 2px 2px 5px #ccc;margin: 15px auto 0 auto; padding:10px;  display: block; line-height:25px;">Are you going? <br> <a href="{{ $event_data['attending_url'] }}" style="text-decoration: none;"><span style="background:#db1522; color: #fff; margin-top: 5px; display:inline-block; border:none; padding:3px 10px; margin: 0px; border-radius:2px; width: 30%;">Yes</span></a>  <a href="{{ $event_data['not_attending_url'] }}" style="text-decoration: none;"><span style="background:#db1522; color: #fff; margin-top: 5px; display:inline-block; border:none; padding:3px 10px; margin: 0px; border-radius:2px; width: 30%;">No</span></a></td>
			</tr>
		</div>
	</table>
</body>
</html>
