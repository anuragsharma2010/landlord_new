<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Event Invitation Matrix</title>
<style type="text/css">
	.bdre{ line-height:30px; font-size:14px;vertical-align: top; width: 100%; }
	.spc_top{margin: 20px 0px;}
</style>
</head>

<body style="background:#eee; font-family:arial; margin: 0px;">

	<div style="width: 800px; margin: 0 auto; text-align: center;">
 
  <table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:20px;box-shadow: 2px 2px 5px #ccc;">
  		
		<div class="bdr">
			<tr>
				<td colspan="4" style="text-align: center;"> <img src="http://206.189.116.120/matrix/public/images/logo.png" alt="logo"></td>
			</tr>
			<tr>
				<!-- <td colspan="4" style="font-size: 14px; text-align:left; padding:10px; background:#db1522; margin-bottom: 10px; color:#fff; float: left; font-weight: bold; border-radius:2px;"></td> -->
			</tr>
			<tr>
				<td colspan="4" style="width:100%; font-size:20px; padding-bottom: 10px; font-weight: bold; color: #000;">{{ ucwords($event_data['event_data']['title']) }}</td>				
			</tr>
			<tr>
				<td colspan="4" style="width:100%; font-size:13px; padding-bottom: 10px;display: ruby-text; border-bottom: solid 1px #a2a2a2; font-weight: bold; color: #a2a2a2;">
					<div style="word-wrap: break-word; width:100px;">
						{{ $event_data['event_data']['description'] }}
					</div>
				</td>				
			</tr>
			<tr>
				<td colspan="4" style="width:100%; font-size:13px;line-height:20px; font-weight: bold; color: #000;">
          @php 
            $event_date = date('l, M d, Y', strtotime($event_data['event_data']['event_date']));

            $event_time_12hr = date('h:i', strtotime($event_data['event_data']['event_time']));

            if($event_time_12hr == '12:00'){
              $event_time_12hr = $event_time_12hr . ' Noon';
            } else {
              $event_time_12hr = date('h:i A', strtotime($event_data['event_data']['event_time']));
            }
            
          @endphp
          {{ $event_date }} <br> 

          {{ $event_time_12hr }}
        </td>					
			</tr>			
			<tr>
				<td colspan="4" style="width:100%; font-size:13px;line-height:20px; color: padding-bottom: 10px; font-weight: bold; color: #000;">{{ $event_data['event_data']['event_location']['name'] }}<br></td>					
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;background: #eee;width:20%; box-shadow: 2px 2px 5px #ccc;margin: auto; padding:10px;  display: block; line-height:25px;">Are you going? <br> <a href="javascript:void(0);" style="text-decoration: none;"><span style="background:#db1522; color: #fff; margin-top: 5px; display:inline-block; border:none; padding:3px 10px; margin: 0px; border-radius:2px; width: 30%;">Yes</span></a>  <a href="javascript:void(0);" style="text-decoration: none;"><span style="background:#db1522; color: #fff; margin-top: 5px; display:inline-block; border:none; padding:3px 10px; margin: 0px; border-radius:2px; width: 30%;">No</span></a></td>
			</tr>
		</div>
	</table>
</body>
</html>
