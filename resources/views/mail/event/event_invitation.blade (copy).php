<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<div style="background: #f2f2f2; margin: 0 auto; max-width: 640px; padding:20px">
  <table cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div style="width: 96%; margin: auto; padding: 5px 0 0px 0"> 
          <img src="http://206.189.116.120/matrix/public/images/logo.png" style="float: left; margin: 20px 0 0 0" alt="matrix" width="90" height="76"> </div>
          <div style="background: #fff; color: #5b5b5b; border-radius: 4px; font-family: arial; font-size: 13px; padding: 10px 20px; width: 90%; margin: 20px auto; line-height: 17px; border: 1px #ddd solid; border-top: 0; clear: both">
            <p>Hi,</p>
            <p> You are invited for an event by {{ ucwords($event_data['user_name']) }} on Matrix</p>
            <p> <strong>Event Details -</strong> </p>
            <p> <strong>Event Name          : </strong>{{ ucwords($event_data['event_data']['title']) }} </p>
            @php 
              $event_date_time = $event_data['event_data']['event_date'] . ' ' . $event_data['event_data']['event_time']; 
              $event_date_time_12hr = date('Y-m-d h:i A', strtotime($event_date_time));
            @endphp
            <p> <strong>Event Date & Time   : </strong>{{ $event_date_time_12hr }} </p>
            <p> <strong>Event Location      : </strong>{{ $event_data['event_data']['event_location']['name'] }} </p>
            <p> <strong>Event Type          : </strong>{{ ucwords($event_data['event_data']['event_type']) }} </p>
            <p> <strong>Event Description   : </strong>{{ $event_data['event_data']['description'] }} </p>
            <p> <strong>Event Guest Count   : </strong>{{ ucwords($event_data['event_data']['guest_count']) }} </p>
            <p> <strong>Event Cuisine        : </strong>{{ ucwords($event_data['event_data']['event_cuisine']['name']) }} </p>
            <p> <strong>Event Budget        : </strong>${{ $event_data['event_data']['event_budget']['amount'] }} per person </p>

            <p>Should you need any further assistance, contact us at <a href="mailto:care@matrix.com?Subject=Registration%20With%20Matrix" target="_top"  style="font-size: 13px; color: #00baf0; text-decoration: none" > care@matrix.com</a> </p>
            <p>Look forward to see you again at Matrix.</p>
            <p> Matrix Care Team </p>
          </div>
          <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
         
              <tr>
                <td height="35">&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 15px" width="82%"><span style="font-weight: bold"> 24x7 </span> Customer Care </td>
                        <td rowspan="2" width="18%" valign="top"><table width="102" cellspacing="0" cellpadding="0" border="0" align="right">
                            <tbody>
                              <tr>
                                <td align="center"><a href="https://www.facebook.com" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/facebook.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                                <td align="center"><a href="https://twitter.com/" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/twitter.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                                <td align="right"><a href="https://www.blogger.com" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/blog.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 8.5pt"> We would love to hear from you. Just write to us at <a href="mailto:care@matrix.com" style="color: #000; font-weight: bold; text-decoration: none" target="_top"> care@matrix.com</a>.
                        </td>
                      </tr>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 8.5pt"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>
