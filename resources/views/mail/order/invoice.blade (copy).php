<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoice Matrix</title>
<style type="text/css">
	.bdre{ line-height:30px; font-size:14px;vertical-align: top; width: 100%; }
	.spc_top{margin: 20px 0px; }
</style>
</head>

<body style="background:#d6d6d6; font-family:arial; margin: 0px;">
	@php

	if($invoice_data['order_type'] == 'event') {
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['event_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['event_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = $i_time_12hr . ' Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['event_time']));
		}

		$order_type = 'Event';
		$delivery_time = 'Event';
		$location = $invoice_data['invoice_data']['event_location']['name'];
		$guest_count = $invoice_data['invoice_data']['guest_count'];
		
		$menu_name = $invoice_data['invoice_data']['event_food_menu']['menu_name'];
		
		$budget_amount = $invoice_data['invoice_data']['event_budget']['amount'];

	} else if($invoice_data['order_type'] == 'order_food') {

		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['delivery_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['delivery_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = $i_time_12hr . ' Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['delivery_time']));
		}

		$order_type = 'Food Order';
		$delivery_time = 'Delivery Order';
		$location = $invoice_data['invoice_data']['order_location']['name'];

		if($invoice_data['invoice_data']['order_type'] == 'individual') {
			$guest_count = 1;
		} else {
			$guest_count = $invoice_data['invoice_data']['guest_count'];
		}
		
		$menu_name = $invoice_data['invoice_data']['order_food_menu']['menu_name'];
		
		if(empty($invoice_data['invoice_data']['order_budget'])){
			$budget_amount = 1;
		} else {
			$budget_amount = $invoice_data['invoice_data']['order_budget']['amount'];
		}
		
	}


	@endphp
	<div style="width: 800px; margin: 0 auto; text-align: center;">
	<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
  				<thead>
  					<tr>
					    <th style="text-align:left; width:50%; float:left; font-size: 14px;">Customer: {{ ucwords($invoice_data['user_name']) }}</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
					 	<td style="text-align: center; width:100%; float:left; font-weight: bold; margin-top: 20px;">{{ $order_type }} scheduled for {{ $i_date }}, at {{ $i_time_12hr }}!</td>
    					<td style="text-align: center; width:100%; float:left;margin-bottom: 20px;">Please make sure you start preparing this at the appropriate time</td>
					 </tr>
					  <tr>
					 	<td style="float:left;"> <img src="http://206.189.116.120/matrix/public/images/logo.png" alt="logo"></td>
					 	<td style="float:right;"> {{ $location }}<br>Invoice {{ $invoice_data['invoice_no'] }} <br>Placed on {{ date('M d, Y', strtotime($invoice_data['order_date'])) }}</td>					 
					 </tr>											  				
				</tbody>				
				  		
	</table>
	<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
		<thead>
			<tr>
			    <th style="text-align: left; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">Customer</th>
				<th style="text-align: left; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">{{ $delivery_time }} Time</th>
				<th style="text-align: right; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">Payment</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="text-align: left; padding:10px 0 0 0px;float: left; width: 33%; float: left; width: 33%;  line-height: 25px;">{{ ucwords($invoice_data['user_name']) }}<br> @if(!empty($invoice_data['user_phone'])) {{ $invoice_data['user_phone'] }} @endif</td>
				<td style="text-align: left; padding:10px 0 0 0px;float: left; width: 33%; line-height: 25px;">
                    {{ $i_date }}<br>at {{ $i_time_12hr }}
                </td>
				<td style="text-align: right; padding:10px 0 0 0px;float: left; width: 33%; line-height: 25px;"> $ {{ $invoice_data['total_amount'] }}</td>
			</tr>
			<!-- <tr>
				<a href="#" style="text-decoration:none;">
					<td style="background: #000; width: 100%; margin: 20px 0px; display: block; border:none; padding:15px 0px; text-align: center; color: #fff; cursor:pointer;">Group order! Please label items for each person.</td>
				</a>
			</tr> -->						
		</tbody>		
	</table>
  <table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
  	<thead>
	    <tr>
	      <th style="text-align: left; padding:0 0 10px 0px;  width: 5%; border-bottom: solid 3px #000;">Qty</th>
	      <th style="text-align: left; padding:0 0 10px 0px; width: 60%; border-bottom: solid 3px #000;">Item</th>
	      <th style="text-align: left; padding:0 0 10px 0px;  width: 15%; border-bottom: solid 3px #000;">Price</th>
	      <th style="text-align: right; padding:0 0 10px 0px;  width: 15%; border-bottom: solid 3px #000;">Subtotal</th>     
	    </tr>
	</thead>
		
	<tbody>
		<tr>
			<td style=" background: #fff; padding: 10px; margin-top: 10px;height: 15px," colspan="4"></td>
		</tr>
		<tr class="bdre">
			<td>{{ $guest_count }}x</td>
			<td><span style="font-weight:bold;">{{ $menu_name }}</span></td>
			<td>${{ $budget_amount }}</td>
			<td style="font-weight:bold;text-align: right;">${{ $invoice_data['order_amount'] }}</td>
		</tr>
		<!-- <tr class="bdre">
			<td>1x</td>
			<td><span style="font-weight:bold;">BBQ Chicken </span> (in Toasted Specialty Sandwiches)<br>Side Choice Hand-Made Slaw</td>
			<td>$9.85</td>
			<td style="font-weight:bold;text-align: right;">$9.85</td>
		</tr> -->
		<!-- <tr>
			<td style=" background: #eee; padding: 10px; margin-top: 10px;" colspan="4">Please label: Austin (1 item)</td>
		</tr>
		<tr class="bdre">
			<td>1x</td>
			<td><span style="font-weight:bold;">Chocolate Chip Crunch Cooki</span> (in Desserts/Snacks) </td>
			<td>$2.50</td>
			<td style="font-weight:bold;text-align: right;">$2.50</td>
		</tr> -->
	</tbody>	
  </table>
  <!-- <table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
		<div class="bdr">
			<tr>
				<td colspan="4" style="font-size: 16px; text-align: center; padding:20px 0px; border-top: solid 3px #000; font-weight: bold;">For any urgent order adjustment, contact us at 1-(855) 973-1040</td>
			</tr>
			<tr class="spc_top">
				<td width="33%"><img src="images/close.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Set special hours for an<br> upcoming holiday on <br>Matrix Support</span></td>

				<td width="33%"><img src="images/food.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Look out for any allergies<br>by double checking order<br>for special instructions</span></td>
			
				<td width="33%"><img src="images/phone.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">For menu adjustments,<br>please visit<br>Matrix Support</span></td>
			</tr>
		</div>
	</table> -->

<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
 
	<tbody>
		<!-- <tr>
			<td style=" background: #eee; padding: 10px; margin-top: 10px;" colspan="4">Please label: Josiah (1 item)</td>
		</tr>
		<tr class="bdre">
			<td>1x</td>
			<td style="text-align: left; padding:0 0 10px 0px; width: 65%;"><span style="font-weight:bold;">Miso Kale Chicken</span> (in Fresh Salads)<br>
				Dressing Choice <span style="font-weight:bold;">Balsamic Vinaigrette</span>
			</td>
			<td>$12.00</td>
			<td style="font-weight:bold;text-align:right;">$12.00</td>
		</tr>
		
		<tr>
			<td style=" background: #eee; padding: 10px; margin-top: 10px;" colspan="4">Please label: Rohan Mehra (1 item)</td>
		</tr>
		<tr class="bdre">
			<td>1x</td>
			<td><span style="font-weight:bold;">Sandwich & Salad</span>  (in Choose Two)<br>
				Sandwich Choice <span style="font-weight:bold;">Grilled Chicken</span><br>
				Small Salad Choice<span style="font-weight:bold;">Miso Kale Chicken</span>
			 </td>
			<td>$10.95</td>
			<td style="font-weight:bold; text-align: right;">$10.95</td>
		</tr> -->

		<tr>
				<a href="#" style="text-decoration:none;">
					<td colspan="4" style="background: #eee; width: 100%; margin: 20px 0px; border:none; padding:15px 0px; text-align: center; color: #000; cursor:pointer;"><strong>~ End of Order ~</strong></td>
				</a>
		</tr>	

		<tr class="bdre">
			<td style="color:#ff3008; font-weight: bold;"></td>
			<td style="color:#ff3008; font-weight: bold;padding:0 0 10px 0px; width: 65%;"></td>
			<td style="font-weight:bold; text-align: left; float: left;">Subtotal:<br>	<span style="text-align: left;">+Tax(9%):</span><br> Total:</td>
			<td style="font-weight:bold; text-align: right;">${{ $invoice_data['order_amount'] }}<br><span style="text-align: right; margin-left: 10px;">${{ $invoice_data['tax_amount'] }}</span><br>${{ $invoice_data['total_amount'] }}</td>			
		</tr>
		<tr>
			<td colspan="4" style="text-align: right; line-height: 25px; font-weight: bold;">Commission will be deducted from the total amount based on your agreement. For full payment
details, visit Matrix Support</td>
		</tr>
	</tbody>	
  </table>
  <table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 20px;">
		<div class="bdr">
			<tr>
				<td colspan="4" style="font-size: 16px; text-align: center; padding:20px 0px; border-top: solid 3px #000; font-weight: bold;">For any urgent order adjustment, contact us at 1-(855) 973-1040</td>
			</tr>
			<tr class="spc_top">
				<td width="33%"><img src="http://206.189.116.120/matrix/public/images/close.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Set special hours for an<br> upcoming holiday on <br>Matrix Support</span></td>

				<td width="33%"><img src="http://206.189.116.120/matrix/public/images/food.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Look out for any allergies<br>by double checking order<br>for special instructions</span></td>
			
				<td width="33%"><img src="http://206.189.116.120/matrix/public/images/phone.png" style="float:left;"><span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">For menu adjustments,<br>please visit<br>Matrix Support</span></td>
			</tr>
		</div>
	</table>
</body>
</html>
