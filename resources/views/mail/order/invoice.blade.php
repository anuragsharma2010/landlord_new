<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Invoice Matrix</title>
<style type="text/css">
	.ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
    table {border-collapse: collapse;}

    @media only screen and (max-width: 600px)  {
		.deviceWidth {width:500px!important; padding:0;}
		.center {text-align: center!important;}
	}

    @media only screen and (max-width: 480px) {
        .deviceWidth {width:280px!important; padding:0;}
        .center {text-align: center!important;}
        .cut-fnt {
			font-size: 12px !important;
			padding: 0px;
			float: left !important;
			width: 25% !important;
			min-height: 40px;
		}
    }
	
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="font-family: arial;">
	@php

	$menu_items = '';
	$budget = 0;
	$individualOrderFoodResponse = array();
	$haveSomeOrderFoodResponse = array();

	$discount_amount = !empty($invoice_data['discount_amount']) ? '$' . $invoice_data['discount_amount'] : "NA";
	$promo_code_name = !empty($invoice_data['promo_code_name']) ? "(" . $invoice_data['promo_code_name'] . ")" : "";

	if($invoice_data['order_type'] == 'event') {
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['event_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['event_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = '12 Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['event_time']));
		}

		$order_type = 'Event';

		$time_for = 'Event Time';
		
		$location = $invoice_data['invoice_data']['event_location']['name'];

		$budget = $invoice_data['invoice_data']['event_budget']['amount'];

		if(!empty($invoice_data['invoice_data']['order_menu_items'])){
            foreach($invoice_data['invoice_data']['order_menu_items'] as $order_menu_item){
				if(!empty($order_menu_item['event_menu_item'])) {
					$menu_items.= $order_menu_item['event_menu_item']['item_name'] . ', ';
				}
            }
			
			$menu_items = substr($menu_items, 0, -2);
            
        }
	} else if($invoice_data['order_type'] == 'food_bar') { 
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['food_bar_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['food_bar_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = '12 Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['food_bar_time']));
		}

		$order_type = 'Food Bar';

		$time_for = 'Food Bar Time';
		
		$location = $invoice_data['invoice_data']['food_bar_location']['name'];

	} else if($invoice_data['order_type'] == 'have_some') {
		$i = 0;
		if(!empty($invoice_data['invoice_data']['order_detail'])){
			foreach($invoice_data['invoice_data']['order_detail'] as $key => $have_some_menu_item) {

				$haveSomeOrderFoodResponse[$key]['item_id'] =  $have_some_menu_item['have_some_menu_item']['id'];
				$haveSomeOrderFoodResponse[$key]['item_name'] = $have_some_menu_item['have_some_menu_item']['item_name'];

				$haveSomeOrderFoodResponse[$key]['price'] = $have_some_menu_item['have_some_menu_item']['price'];
				$haveSomeOrderFoodResponse[$key]['order_quantity'] = $have_some_menu_item['order_quantity'];
				$haveSomeOrderFoodResponse[$key]['order_price'] = $have_some_menu_item['order_price'];
			}
		}

	} else {
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['delivery_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['delivery_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = '12 Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['delivery_time']));
		}

		$order_type = 'Food Order';
		
		$time_for = 'Delivery Time';
		
		$location = $invoice_data['invoice_data']['order_location']['name'];

		if($invoice_data['invoice_data']['order_type'] == 'individual') {
			$i = 0;
			if(!empty($invoice_data['invoice_data']['order_menu_items'])){
				foreach($invoice_data['invoice_data']['order_menu_items'] as $key => $order_menu_item) {
					if(!empty($order_menu_item['order_food_menu_item'])){
						if(!empty($order_menu_item['order_food_menu_item']['food_menus'])){
							$menu_id = $order_menu_item['order_food_menu_item']['food_menus']['id'];
							$individualOrderFoodResponse[$menu_id]['menu_id'] = $menu_id;
							$individualOrderFoodResponse[$menu_id]['menu_name'] = $order_menu_item['order_food_menu_item']['food_menus']['menu_name'];

							if(!empty($order_menu_item['order_food_menu_item']['sub_food_menus'])){
								$price = $order_menu_item['order_food_menu_item']['sub_food_menus']['price'];
							} else {
								$price = 0;
							}

							unset($order_menu_item['order_food_menu_item']['food_menus']);
                            unset($order_menu_item['order_food_menu_item']['sub_food_menus']);

							$individualOrderFoodResponse[$menu_id]['food_menu_items'][$i] = $order_menu_item['order_food_menu_item'];
							$individualOrderFoodResponse[$menu_id]['food_menu_items'][$i]['price'] = $price;
							$individualOrderFoodResponse[$menu_id]['food_menu_items'][$i]['quantity'] = !empty($order_menu_item['quantity']) ? $order_menu_item['quantity'] : 1;

							$individualOrderFoodResponse[$menu_id]['food_menu_items'] = array_values($individualOrderFoodResponse[$menu_id]['food_menu_items']);
							
							$i++;
						}

					}
				}

				$individualOrderFoodResponse = array_values($individualOrderFoodResponse);
			}

		} else {

			$budget = $invoice_data['invoice_data']['order_budget']['amount'];

			if(!empty($invoice_data['invoice_data']['order_menu_items'])){
				foreach($invoice_data['invoice_data']['order_menu_items'] as $order_menu_item){
					if(!empty($order_menu_item['order_food_menu_item'])) {
						$menu_items.= $order_menu_item['order_food_menu_item']['item_name'] . ', ';
					}
				}
				
				$menu_items = substr($menu_items, 0, -2);
        	}
		}

	}

	@endphp
	
	<!-- Wrapper -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td width="100%" valign="top" bgcolor="#d6d6d6">
				<!-- Start Header-->
				<table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="margin:0 auto;">
					<tr>
						<td width="100%" bgcolor="#ffffff">

							<!-- Logo -->
							<table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
								<tr>
									<td class="center" style="font-size: 13px; color: #272727; font-weight: light; text-align: right;  line-height: 20px; vertical-align: middle; padding:10px 20px;">
										<p style="margin: 0px; font-size: 14px; font-weight: 700;"> Customer: {{ ucwords($invoice_data['user_name']) }} </p>
									</td>
								</tr>
							</table><!-- End Logo -->

						</td>
					</tr>
				</table><!-- End Header -->

				<!-- One Column -->
				<table width="580" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed" style="margin:0 auto;">
					@if($invoice_data['order_type'] != 'have_some')
					<tr>
						<td valign="top" style="line-height: 20px; vertical-align: middle; padding:10px 20px 5px 20px;" bgcolor="#ffffff" align="center" >
							<p style="margin: 0px; font-size: 18px; font-weight: 700;"> {{ $order_type }} scheduled for {{ $i_date }}, at {{ $i_time_12hr }}!</p>
						</td>
					</tr>
					@endif
					@if($invoice_data['order_type'] != 'food_bar') 
						<tr>
							<td valign="top" style="line-height: 20px; vertical-align: middle; padding:0px 20px 20px 20px;" bgcolor="#ffffff" align="center" >
								<p style="margin: 0px; font-size: 14px;"> Please make sure you start preparing this at the appropriate time</p>
							</td>
						</tr>
					@endif
				</table><!-- End One Column -->

				<table class="deviceWidth" style="margin:0 auto;" width="580" cellspacing="0" 	 cellpadding="0" border="0">
					<tbody>
                    	<tr>
                        	<td bgcolor="#ffffff">
								<!-- Logo -->
                                <table class="deviceWidth" cellspacing="0" cellpadding="0" border="0" align="left">
                                    <tbody>
                                        <tr>
                                            <td style="padding:10px 20px" class="center">
                                                <a href="#"><img src="{{ PUBLIC_FOLDER_URL . 'images/logo_120_138.png' }}" alt="" border="0"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table><!-- End Logo -->

								<!-- Nav -->
                                <table class="deviceWidth" cellspacing="0" cellpadding="0" border="0" align="right">
                                    <tbody>
                                        <tr>
                                            <td class="center" style="font-size: 14px; font-weight: bold; color: #272727; font-weight: light; text-align: left; line-height: 20px; vertical-align: middle; padding:10px 20px;">
											@if($invoice_data['order_type'] != 'have_some')
												{{ $location }}
											@endif
											<br>Invoice {{ $invoice_data['invoice_no'] }} <br>Placed on {{ date('M d, Y', strtotime($invoice_data['order_date'])) }} <br>Payment Type : {{ $invoice_data['payment_type'] }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table><!-- End Nav -->

								<table class="table-responsive" style="background:#fff; padding:10px 20px; width:85%; margin: auto;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <thead>
                                        <tr>
                                            <th class="cut-fnt" style="text-align: left; padding-left: 12px; font-weight:700; font-size: 14px; float: left;width: 30.66%;border-bottom: solid 1px #000; text-align: left; min-height: 30px;">Customer</th>
											@if($invoice_data['order_type'] != 'have_some')
                                            <th class="cut-fnt" style="text-align: left; padding-left: 12px; font-weight:700; font-size: 14px; float: left;width: 30%;border-bottom: solid 1px #000; text-align: left; min-height: 30px;">{{ $time_for }}</th>
											@else
											<th class="cut-fnt" style="text-align: left; padding-left: 12px; font-weight:700; font-size: 14px; float: left;width: 30%;border-bottom: solid 1px #000; text-align: left; min-height: 30px;"></th>
											@endif
                                            <th class="cut-fnt" style="text-align: left; padding-left: 12px; font-weight:700; font-size: 14px; float: left;width: 30.66%;border-bottom: solid 1px #000; text-align: right; min-height: 30px;">Payment</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="cut-fnt" style="text-align: left; padding:10px 8px; float: left; font-size: 12px; float: left; line-height: 20px; width: 33%;">{{ ucwords($invoice_data['user_name']) }}<br> @if(!empty($invoice_data['user_phone'])) {{ $invoice_data['user_phone'] }} @endif</td>
											@if($invoice_data['order_type'] != 'have_some')
                                            <td class="cut-fnt" style="text-align: left; padding:10px 0px; float: left; font-size: 12px;  width: 30%; line-height: 20px;">{{ $i_date }} at {{ $i_time_12hr }}</td>
											@else
											<td class="cut-fnt" style="text-align: left; padding:10px 0px; float: left; font-size: 12px;  width: 30%; line-height: 20px;"></td>
											@endif
                                            <td class="cut-fnt" style="padding:10px; float: left; font-size: 12px;  width:33%; line-height: 20px; font-weight:bold;text-align: right; padding: 5px 0px"> ${{ $invoice_data['total_amount'] }}</td>
                                        </tr>
                                        <!-- <tr>
                                            <a href="#" style="text-decoration:none;">
                                                <td style="background: #000; width: 100%; margin: 20px 0px; display: block; border:none; padding:15px 0px; text-align: center; color: #fff; cursor:pointer;">Group order! Please label items for each person.</td>
                                            </a>
                                        </tr> -->                       
                                    </tbody>        
                                </table>

								<table class="table-responsive" style="background:#fff; width:85%; margin:auto; padding:0px;display: block;" cellspacing="0" cellpadding="0" border="0">
                                    <thead>
                                        <tr>
                                          <th style="text-align: left; padding: 30px 0 10px 0px; width: 10%; border-bottom: solid 3px #000;">Qty</th>
                                          <th style="text-align: left; padding: 30px 0 10px 10px; width: 65%; border-bottom: solid 3px #000;">Item</th>
                                          <th style="text-align: left; padding: 30px 0 10px 10px; width: 25%; text-align: right; border-bottom: solid 3px #000;">Price</th> 
										  <th style="text-align: left; padding: 30px 0 10px 10px; width: 25%; text-align: right; border-bottom: solid 3px #000;">Subtotal</th>  
                                        </tr>
                                    </thead>
                                        
                                    <tbody>
                                        <tr>
                                            <td style=" background: #fff; padding: 10px; margin-top: 10px;height: 15px," colspan="4"></td>
                                        </tr>

										@if(!empty($individualOrderFoodResponse))
											@foreach($individualOrderFoodResponse as $individualOrderFood)
												<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
													<td style="padding: 5px 0px"></td>
													<td colspan="3" style="padding: 5px 0px"><span style="font-weight:bold;">{{ $individualOrderFood['menu_name'] }}</span></td>
												</tr>
												@php $subtotal = 0; @endphp
												@if(!empty($individualOrderFood['food_menu_items']))
													@foreach($individualOrderFood['food_menu_items'] as $food_menu_item)
														@php $subtotal = $subtotal + ($food_menu_item['quantity'] * $food_menu_item['price']); @endphp
														<tr style="line-height:20px; font-size:14px;vertical-align: top;">
															<td style="padding: 5px 0px">{{ $food_menu_item['quantity'] }}x</td>
															<td style="padding: 5px 0px">{{ $food_menu_item['item_name'] }}</td>
															<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $food_menu_item['price'] }}</td>
														</tr>
													@endforeach
												@endif
												<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
													<td style="padding: 5px 0px"></td>
													<td style="padding: 5px 0px"></td>
													<td style="font-weight:bold;text-align: right; padding: 5px 0px"></td>
													<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $subtotal }}</td>
												</tr>
											@endforeach
										@elseif($invoice_data['order_type'] == 'food_bar')
											<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
												<td style="padding: 5px 0px">1x</td>
												<td style="padding: 5px 0px"><span style="font-weight:bold;">{{ $invoice_data['invoice_data']['title'] }}</span></td>
												<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $invoice_data['invoice_data']['price'] }}</td>
												<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $invoice_data['order_amount'] }}</td>
											</tr>
										@elseif(!empty($haveSomeOrderFoodResponse))
												@php $subtotal = 0; @endphp
												@foreach($haveSomeOrderFoodResponse as $haveSomeOrderFood)
													@php $subtotal = $subtotal + ($haveSomeOrderFood['order_quantity'] * $haveSomeOrderFood['price']); @endphp
													<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
														<td style="padding: 5px 0px">{{ $haveSomeOrderFood['order_quantity'] }}x</td>
														<td colspan="3" style="padding: 5px 0px"><span style="font-weight:bold;">{{ $haveSomeOrderFood['item_name'] }}</span></td>
														<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $haveSomeOrderFood['price'] }}</td>
													</tr>
												@endforeach

												<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
													<td style="padding: 5px 0px"></td>
													<td style="padding: 5px 0px"></td>
													<td style="font-weight:bold;text-align: right; padding: 5px 0px"></td>
													<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $subtotal }}</td>
												</tr>
										@else
											<tr style="line-height:20px; font-size:14px; padding-bottom: 10px; vertical-align: top;">
												<td style="padding: 5px 0px">{{ $invoice_data['invoice_data']['guest_count'] }}x</td>
												<td style="padding: 5px 0px"><span style="font-weight:bold;">{{ $menu_items }}</span></td>
												<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $budget }}</td>
												<td style="font-weight:bold;text-align: right; padding: 5px 0px">${{ $invoice_data['order_amount'] }}</td>
											</tr>
										@endif

                                    </tbody>    
                                </table>
							</td>
						</tr>
					</tbody>
				</table>

				<table class="deviceWidth" style="margin:0 auto;" bgcolor="#fff" width="580" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td colspan="4" style="width: 100%; margin: 20px 0px; padding:0px 15px; border:none; text-align: center; color: #000; cursor:pointer;">
							<strong style="background: #eee; float: left; padding:15px 0px;   width: 100%; margin: 20px 0px;">~ End of Order ~</strong>
						</td>
							
					</tr>
				</table>
				
				@if($invoice_data['order_type'] != 'food_bar')
					<table class="deviceWidth" style="margin:0 auto;" bgcolor="#fff" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
						<tbody>
							<tr>
								<td style="padding:10px 0">
									<table class="deviceWidth" width="10%" cellspacing="0" cellpadding="0" border="0" align="left">
										<tbody>
											<tr>
												<td style="color:#ff3008; font-weight: bold; text-align: center; width: 10%;"></td>
												<td style="color:#ff3008; font-weight: bold;padding:0px; width: 40%;"></td>
											</tr>
										</tbody>
									</table>
									<table class="deviceWidth" width="100%" cellspacing="0" cellpadding="0" border="0" align="right">
										<tbody>
											<tr>
												<td style=" text-align: left; line-height: 24px; padding-right: 15px; padding-left: 15px; vertical-align: top;">

													<table class="" width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; text-align: right;">
														<tbody>
															@if(!empty($invoice_data['invoice_data']['cooking_instruction']))
																<tr>
																	<td style="font-weight:bold; text-align: left; width: 40%; float: left;">
																		<strong>Special Instructions:</strong><br>
																	</td>
																	<td style="font-weight:bold; width: 60%; text-align: left; float: left;">
																		<span style="text-align: right; font-size:10px;">
																			<strong>{{ $invoice_data['invoice_data']['cooking_instruction'] }}</strong>
																		</span><br>   
																	</td>
																</tr>
															@endif
															<tr>
																<td style="font-weight:bold; text-align: left; width: 40%; float: left;">
																	<strong>Tableware</strong>(Napkins, Plates & Utensils)<strong>:</strong><br>
																</td>
																<td style="font-weight:bold; width: 60%; text-align: right; float: left;">
																	<span style="text-align: right;">
																		<strong>
																			@if(isset($invoice_data['tableware']))
																				{{ ($invoice_data['tableware'] == 1) ? 'Yes' : 'No' }}
																			@else
																				{{ ($invoice_data['invoice_data']['tableware'] == 1) ? 'Yes' : 'No' }}
																			@endif
																		</strong>
																	</span><br>   
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
					
						</tbody>
					</table>
				@endif

				<table class="deviceWidth" style="margin:0 auto;" bgcolor="#fff" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
                	<tbody>
						<tr>
                    		<td style="padding:10px 0">
								<table class="deviceWidth" width="10%" cellspacing="0" cellpadding="0" border="0" align="left">
									<tbody>
										<tr>
											<td style="color:#ff3008; font-weight: bold; text-align: center; width: 10%;"></td>
											<td style="color:#ff3008; font-weight: bold;padding:0px; width: 40%;"></td>
										</tr>
									</tbody>
								</table>
                            	<table class="deviceWidth" width="100%" cellspacing="0" cellpadding="0" border="0" align="right">
                                	<tbody>
                                    	<tr>
                                    		<td style=" text-align: left; line-height: 24px; padding-right: 15px; padding-left: 15px; vertical-align: top;">

												<table class="" width="70%" cellspacing="0" cellpadding="0" border="0" style="float: left; text-align: right;">
													<tbody>
														<tr>
															<td style="font-weight:bold; text-align: left; width: 100%; float: left;">
																<strong>Subtotal:</strong><br>
																<span style="text-align: left;">
																	<strong>-Discount Applied <span style="font-size:8px;">{{ $promo_code_name }}</span>:</strong>
																</span><br>  
																<span style="text-align: left;">
																	<strong>+Tax({{ $invoice_data['tax_percentage'] }}%):</strong>
																</span><br>
																<strong>Total:</strong>
															</td> 
														</tr>
													</tbody>
												</table>

												<table class="" width="30%" bgcolor="#fff" cellspacing="0" cellpadding="0" border="0" style="float: left; text-align: right;">
													<tbody>
														<tr>
															<td style="font-weight:bold; width: 100%; text-align: right; float: left;">
																<span style="text-align: right;">
																	<strong>${{ $invoice_data['order_amount'] }}</strong>
																</span><br>   
																<span style="text-align: right;">
																	<strong>{{ $discount_amount }}</strong>
																</span><br>
																<span style="text-align: right;">
																	<strong>${{ $invoice_data['tax_amount'] }}</strong>
																</span><br>
																<span style="text-align: right; margin-right:1px; padding:0px;">
																	<strong>${{ $invoice_data['total_amount'] }}</strong>
																</span>
															</td> 
														</tr>
													</tbody>
												</table>
                                    		</td>
                                		</tr>
                            		</tbody>
                        		</table>
                    		</td>
                		</tr>
                
            		</tbody>
        		</table>

				<table class="deviceWidth" style="margin:0 auto;" bgcolor="#fff" width="580" cellspacing="0" cellpadding="0" border="0" bgcolor="" align="center">
                	<tbody>
						<tr>
							<td style="font-weight: normal; text-align: center; vertical-align: top; padding:10px 8px 10px 8px">
								<p style="font-size: 12px;font-weight: 700;text-align: left;padding: 10px;border-bottom: solid 2px #000">
									Commission will be deducted from the total amount based on your agreement. For full payment details, visit Matrix Support
								</p>
							</td>
						</tr>
             
						<tr>
							<td style="font-weight: normal; text-align: center; vertical-align: top; padding:10px 8px 10px 8px">
								<p style="font-size: 16px; font-weight: 700; text-align: center;">
									For any urgent order adjustment, contact us at 1-(855) 973-1040
								</p>
							</td>
						</tr>
            		</tbody>
        		</table>
			</td>
		</tr>
	</table>
	
</body>
</html>
