<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Invoice Matrix</title>
<style type="text/css">
	.ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
    table {border-collapse: collapse;}

    @media only screen and (max-width: 600px)  {
		.deviceWidth {width:500px!important; padding:0;}
		.center {text-align: center!important;}
	}

    @media only screen and (max-width: 480px) {
        .deviceWidth {width:280px!important; padding:0;}
        .center {text-align: center!important;}
        .cut-fnt {
			font-size: 12px !important;
			padding: 0px;
			float: left !important;
			width: 25% !important;
			min-height: 40px;
		}
    }
	
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="font-family: arial;">
	@php

	$menu_items = '';
	$individualOrderFoodResponse = array();

	if($invoice_data['order_type'] == 'event') {
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['event_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['event_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = '12 Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['event_time']));
		}

		$order_type = 'Event';

		$time_for = 'Event Time';
		
		$location = $invoice_data['invoice_data']['event_location']['name'];

		if(!empty($invoice_data['invoice_data']['order_menu_items'])){
            foreach($invoice_data['invoice_data']['order_menu_items'] as $order_menu_item){
				if(!empty($order_menu_item['event_menu_item'])) {
					$menu_items.= $order_menu_item['event_menu_item']['item_name'] . ', ';
				}
            }
			
			$menu_items = rtrim($menu_items,',');
            
        }
	} else {
		$i_date = date('l, M d, Y', strtotime($invoice_data['invoice_data']['delivery_date']));

		$i_time_12hr = date('h:i', strtotime($invoice_data['invoice_data']['delivery_time']));

		if($i_time_12hr == '12:00'){
			$i_time_12hr = '12 Noon';
		} else {
			$i_time_12hr = date('h:i A', strtotime($invoice_data['invoice_data']['delivery_time']));
		}

		$order_type = 'Food Order';
		
		$time_for = 'Delivery Time';
		
		$location = $invoice_data['invoice_data']['order_location']['name'];

		if($invoice_data['invoice_data']['order_type'] == 'individual') {
			$i = 0;
			if(!empty($invoice_data['invoice_data']['order_menu_items'])){
				foreach($invoice_data['invoice_data']['order_menu_items'] as $key => $order_menu_item) {
					if(!empty($order_menu_item['order_food_menu_item'])){
						if(!empty($order_menu_item['order_food_menu_item']['food_menus'])){
							$menu_id = $order_menu_item['order_food_menu_item']['food_menus']['id'];
							$individualOrderFoodResponse[$menu_id]['menu_id'] = $menu_id;
							$individualOrderFoodResponse[$menu_id]['menu_name'] = $order_menu_item['order_food_menu_item']['food_menus']['menu_name'];

							if(!empty($order_menu_item['order_food_menu_item']['sub_food_menus'])){
								$price = $order_menu_item['order_food_menu_item']['sub_food_menus']['price'];
							} else {
								$price = 0;
							}

							unset($order_menu_item['order_food_menu_item']['food_menus']);
                            unset($order_menu_item['order_food_menu_item']['sub_food_menus']);

							$individualOrderFoodResponse[$menu_id]['food_menu_items'][$i] = $order_menu_item['order_food_menu_item'];
							$individualOrderFoodResponse[$menu_id]['food_menu_items'][$i]['price'] = $price;

							$individualOrderFoodResponse[$menu_id]['food_menu_items'] = array_values($individualOrderFoodResponse[$menu_id]['food_menu_items']);
							
							$i++;
						}

					}
				}

				$individualOrderFoodResponse = array_values($individualOrderFoodResponse);
			}

		} else {
			if(!empty($invoice_data['invoice_data']['order_menu_items'])){
				foreach($invoice_data['invoice_data']['order_menu_items'] as $order_menu_item){
					if(!empty($order_menu_item['event_menu_item'])) {
						$menu_items.= $order_menu_item['event_menu_item']['item_name'] . ', ';
					}
				}
				
				$menu_items = rtrim($menu_items,',');
        	}
		}

	}

	@endphp
	
	<div style="width: 800px; margin: 0 auto; text-align: center; padding:10px 20px;">
		<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 0px;">
					<thead>
						<tr>
							<td colspan="2" style="text-align:left; width:50%; float:left; font-size: 14px; margin-bottom: 20px;">Customer: {{ ucwords($invoice_data['user_name']) }}</td>
						</tr>
					</thead>
					
					<tbody>
						<tr>
							<td colspan="2" style="text-align: center; width:100%;  font-weight: bold; margin:20px 0px;">{{ $order_type }} scheduled for {{ $i_date }}, at {{ $i_time_12hr }}!<br><span style="font-weight: normal;">Please make sure you start preparing this at the appropriate time</span></td>
							<!--<td style="text-align: center; width:100%; float:left;margin-bottom: 20px;"></td>-->
						</tr>
						<tr>
							<td style="float:left;"> <img src="http://206.189.116.120/matrix/public/images/logo.png" alt="logo"></td>
							<td style="margin-top: 20px; width:150px; text-align: right; float:right"> {{ $location }}<br>Invoice {{ $invoice_data['invoice_no'] }} <br>Placed on {{ date('M d, Y', strtotime($invoice_data['order_date'])) }}</td>					 
						</tr>											  				
					</tbody>				
							
		</table>
		<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff;  padding:10px 0px;">
			<thead>
				<tr>
					<th style="text-align: left; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">Customer</th>
					<th style="text-align: left; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">{{ $time_for }}</th>
					<th style="text-align: right; padding:0 0 10px 0px; float: left; width: 33%; border-bottom: solid 1px #000;">Payment</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align: left; padding:10px 0 0 0px;float: left; width: 33%; float: left; width: 33%;  line-height: 25px;">{{ ucwords($invoice_data['user_name']) }}<br> @if(!empty($invoice_data['user_phone'])) {{ $invoice_data['user_phone'] }} @endif</td>
					<td style="text-align: left; padding:10px 0 0 0px;float: left; width: 33%; line-height: 25px;">
						{{ $i_date }} at {{ $i_time_12hr }}
					</td>
					<td style="text-align: right; padding:10px 0 0 0px;float: left; width: 33%; line-height: 25px;"> $ {{ $invoice_data['total_amount'] }}</td>
				</tr>					
			</tbody>		
		</table>
		<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff;">
			<thead>
				<tr>
				<td style="text-align: left; padding:15px 0 10px 0px;  width: 5%; border-bottom: solid 3px #000;">Qty</td>
				<td style="text-align: left; padding:15px 0 10px 0px; width: 60%; border-bottom: solid 3px #000;">Item</td>
				<td style="text-align: left; padding:15px 0 10px 0px;  width: 15%; border-bottom: solid 3px #000;">Price</td>
				<td style="text-align: right; padding:15px 0 10px 0px;  width: 15%; border-bottom: solid 3px #000;">Subtotal</td>     
				</tr>
			</thead>
				
			<tbody>
				<tr>
					<td style=" background: #fff; padding:0px; margin-top: 10px;height: 15px," colspan="4"></td>
				</tr>
				@if(!empty($individualOrderFoodResponse))
					@foreach($individualOrderFoodResponse as $individualOrderFood)
						<tr class="bdre">
							<td></td>
							<td colspan="3" style="padding: 10px 0px; text-align:left;">{{ $individualOrderFood['menu_name'] }}</td>
						</tr>
						@php $subtotal = 0; @endphp
						@if(!empty($individualOrderFood['food_menu_items']))
							@foreach($individualOrderFood['food_menu_items'] as $food_menu_item)
								@php $subtotal = $subtotal + $food_menu_item['price']; @endphp
								<tr class="bdre">
									<td>1x</td>
									<td style="padding: 10px 0px; text-align:left;">{{ $food_menu_item['item_name'] }}</td>
									<td colspan="2">${{ $food_menu_item['price'] }}</td>
								</tr>
							@endforeach
						@endif
						<tr class="bdre">
							<td></td>
							<td></td>
							<td></td>
							<td>${{ $subtotal }}</td>
						</tr>
					@endforeach
				@else
					<tr class="bdre">
						<td style="padding: 10px 0px; text-align:left;">{{ $invoice_data['invoice_data']['guest_count'] }}x</td>
						<td style="text-align:left;"><span style="font-weight:bold;">{{ $menu_items }}</span></td>
						<td>${{ $invoice_data['invoice_data']['event_budget']['amount'] }}</td>
						<td style="font-weight:bold;text-align: right;">${{ $invoice_data['order_amount'] }}</td>
					</tr>
				@endif
				
			</tbody>	
		</table>

		<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding: padding:10px 0px;">

				<tr>
					<td colspan="4" style="background: #ddd; width: 100%; padding: 10px 0px; border:none; text-align: center; color: #000; cursor:pointer;"><strong>~ End of Order ~</strong></td>
				</tr>	

				<tr class="bdre">
					<td style="color:#ff3008; font-weight: bold;"></td>
					<td style="color:#ff3008; font-weight: bold;padding:0 0 10px 0px; width: 65%;"></td>
					<td style="font-weight:bold; text-align: left; float: left; padding: 10px 0px">Subtotal:<br>	<span style="text-align: left;">+Tax(9%):</span><br> Total:</td>
					<td style="font-weight:bold; text-align: right; padding: 10px 0px;">${{ $invoice_data['order_amount'] }}<br><span style="text-align: right; margin-left: 10px;">${{ $invoice_data['tax_amount'] }}</span><br>${{ $invoice_data['total_amount'] }}</td>			
				</tr>
				<tr>
					<td colspan="4" style="text-align: center; line-height: 25px; font-weight: bold;">Commission will be deducted from the total amount based on your agreement. For full payment
		details, visit Matrix Support</td>
				</tr>
		</table>
		<table class="table-responsive" width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff; padding:10px 0px;">
			<div class="bdr">
				<tr>
					<td colspan="4" style="font-size: 16px; text-align: center; padding:20px 0px; border-top: solid 3px #000; font-weight: bold;">For any urgent order adjustment, contact us at 1-(855) 973-1040</td>
				</tr>
				<tr style="width: 80%; display: block; margin: 0 auto; text-align: center;">
					<td style="width: 33.33%; text-align: center; float: left;">
						<div style="text-align:center; width:100%; float: left;">
							<img src="http://206.189.116.120/matrix/public/images/close.png" style="float:left;">
							<span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Set special hours for an<br> upcoming holiday on <br>Matrix Support</span>
						</div>
					</td>

					<td style="width: 33.33%; text-align: center; float: left;">
						<div style="text-align:center; width:100%; float: left;">
							<img src="http://206.189.116.120/matrix/public/images/food.png" style="float:left;">
							<span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">Look out for any allergies<br>by double checking order<br>for special instructions</span>
						</div>	
					</td>
				
					<td style="width: 33.33%; text-align: center; float: left;">
						<div style="text-align:center; width:100%; float: left;">
							<img src="http://206.189.116.120/matrix/public/images/phone.png" style="float:left;">
							<span  style="float: left; font-size: 13px;margin: 0px 0px 20px 0;">For menu adjustments,<br>please visit<br>Matrix Support</span>
						</div>	
					</td>
				</tr>
			</div>
		</table>
	</div>
	
</body>
</html>
