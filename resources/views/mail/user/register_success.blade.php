<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<div style="background: #f2f2f2; margin: 0 auto; max-width: 640px; padding:20px">
  <table cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div style="width: 96%; margin: auto; padding: 5px 0 0px 0"> 
        <img src="{{ PUBLIC_FOLDER_URL . 'images/logo_120_138.png' }}" style="float: left; margin: 20px 0 0 0" alt="Matrix"> </div>
          <div style="background: #fff; color: #5b5b5b; border-radius: 4px; font-family: arial; font-size: 13px; padding: 10px 20px; width: 90%; margin: 20px auto; line-height: 17px; border: 1px #ddd solid; border-top: 0; clear: both">
            <p>Hi {{$user->name}}!</p>
            <p> Thanks for getting linked with Matrix!  </p>
            <p> Your account has been register successfully!  </p>
            
            <p>If you need any futher assistance, feel free to contact us at <a href="mailto:support@matrix.com?Subject=Registration%20With%20Matrix" target="_top"  style="font-size: 13px; color: #00baf0; text-decoration: none" > support@matrix.com</a> </p>
            <!-- <p>Glad to serve you the Appetitzing food from Matrix</p>
            <p>Stay Linked!</p> -->
            <p> Matrix Team </p>
          </div>
          <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
         
              <tr>
                <td height="35">&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 15px" width="82%"><span style="font-weight: bold"> 24x7 </span> Customer Care </td>
                        <td rowspan="2" width="18%" valign="top">
                          <!-- <table width="102" cellspacing="0" cellpadding="0" border="0" align="right">
                            <tbody>
                              <tr>
                                <td align="center"><a href="https://www.facebook.com" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/facebook.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                                <td align="center"><a href="https://twitter.com/" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/twitter.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                                <td align="right"><a href="https://www.blogger.com" target="_blank" rel="noreferrer"> <img src="http://cdn2.paytm.com/merchantimage/cms/blog.png" alt="matrix" width="31" height="31" border="0"> </a></td>
                              </tr>
                            </tbody>
                          </table> -->
                        </td>
                      </tr>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 8.5pt"> We would love to hear from you. Please feel free to write us at <a href="mailto:support@matrix.com" style="color: #000; font-weight: bold; text-decoration: none" target="_top"> support@matrix.com</a>.
                        </td>
                      </tr>
                      <tr>
                        <td style="font-family: 'Open Sans',Arial,sans-serif; font-size: 8.5pt"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>
