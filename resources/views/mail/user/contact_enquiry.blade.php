<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Untitled Document</title>
	</head>

	<body>
		<div style="background: #f2f2f2; margin: 0 auto; max-width: 640px; padding:20px">
		  	<table cellspacing="0" cellpadding="0" border="0" align="center">
			    <tbody>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<div style="width: 96%; margin: auto; padding: 5px 0 0px 0"> 
								<img src="{{ PUBLIC_FOLDER_URL . 'images/logo_120_138.png' }}" style="float: left; margin: 20px 0 0 0" alt="Matrix"> 
							</div>
							<div style="background: #fff; color: #5b5b5b; border-radius: 4px; font-family: arial; font-size: 13px; padding: 10px 20px; width: 90%; margin: 20px auto; line-height: 17px; border: 1px #ddd solid; border-top: 0; clear: both">
								<p>Hi Admin!</p>
								<p> Contact enquiry:  </p>
								<p> Name 		:	{{ $enquiry_data['name'] }}</p>
								<p> Email 		:  	{{ $enquiry_data['email'] }} </p>
								<p> Phone No. 	:  	{{ $enquiry_data['phoneno'] }} </p>
								<p> Message 	:  	{{ $enquiry_data['message'] }} </p>

								<p>Stay Linked!</p>
								<p> Matrix Team </p>
							</div>
						</td>
					</tr>
			    </tbody>
		  	</table>
		</div>
	</body>
</html>
