<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<table width="640px" cellspacing="0" cellpadding="0" border="0" align="center">
  <tbody>
    <tr>
      <td bgcolor="#f3f7f8" align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; font-size: 0em; border: 0px" width="10" height="17"></td>
    </tr>
    <tr>
      <td bgcolor="#f3f7f8" align="center"><table style="background:#fff url({{ PUBLIC_FOLDER_URL . 'images/bg.png' }}) no-repeat top left; border: 1px solid #f4f4f4;padding-top: 35px;" width="340" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td><table style="font-family: 'Open Sans',Arial,Helvetica,sans-serif" width="290" cellspacing="0" cellpadding="0" align="center">
                  <tbody>
                  <tr style="display: block; white-space: nowrap">
                    <td align="center"><table width="290" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                          <tr>
                            <td valign="top" align="right"><a href="#"> <img src="{{ PUBLIC_FOLDER_URL . 'images/logo_120_138.png' }}" alt="Matrix" title="Matrix"> </a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; font-size: 0em; border: 0px" width="10" height="30"></td>
                  </tr>
                  <tr>
                    <td style="font-size: 21px; font-weight: 600; line-height: 30px; color: #222" align="left">Amount Added of</td>
                  </tr>
                  <tr>
                    <td style="font-size: 24px; line-height: 30px; color: #222" align="left"><strong>${{number_format($user['amount'], 2, '.', ',')}}</strong></td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="10"></td>
                  </tr>
                  <tr>
                    <td style="font-size: 16px; line-height: 24px; color: #222; letter-spacing: .3px; font-weight: 600" align="left">to your Matrix account. <br>
                    </td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="border: 0px" width="10" height="30"></td>
                  </tr>
                  <tr>
                    <td style="border: 1px solid #e2ebee" align="center"></td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="border: 0px" width="10" height="30"></td>
                  </tr>
                  <tr>
                    <td style="font-size: 14px; color: #222; letter-spacing: .3px; font-weight: 700; line-height: 20px" align="left">Your updated Matrix Wallet Balance</td>
                  </tr>
                  <tr>
                    <td style="font-size: 21px; color: #222; letter-spacing: .4px; font-weight: 600; line-height: 30px" align="left">${{number_format($user['total'], 2, '.', ',')}}</td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="border: 0px" width="10" height="5"></td>
                  </tr>
                 
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="border: 0px" width="10" height="20"></td>
                  </tr>
                  <tr>
                    <td style="color: #666; font-size: 12px; letter-spacing: .3px; line-height: 18px" align="left">Date : {{date("d-m-Y", strtotime($user['date'])) }} </td>
                  </tr>
                  <tr>
                    <td style="color: #666; font-size: 12px; letter-spacing: .3px; line-height: 18px" align="left"> Matrix Reference Number : 123456789 </td>
                  </tr>
                  <tr>
                    <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="40"></td>
                  </tr>
                </tbody></table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr style="white-space: nowrap">
      <td bgcolor="#f3f7f8"><table width="340" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="5"></td>
            </tr>
            <tr>
              <td bgcolor="#db1522" align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="5"></td>
            </tr>
            <tr>
              <td bgcolor="#000" align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="5"></td>
            </tr>
            <tr>
              <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; font-size: 0em; border: 0px" width="10" height="13"></td>
            </tr>
          
            <tr>
              <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; font-size: 0em; border: 0px" width="10" height="17"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; font-weight: 600; font-family: 'Open Sans',Arial,Helvetica,sans-serif">Need Help?  <a href="#" style="color: #db1522; text-decoration: none" target="_blank" rel="noreferrer">Reach us here</a></td>
            </tr>
            <tr>
              <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; border: 0px" width="10" height="8"></td>
            </tr>
            <tr>
              <td><table cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                    <tr>
                      <td style="font-size: 11px; font-weight: 600; font-family: 'Open Sans',Arial,Helvetica,sans-serif; color: #666666; line-height: 19px; text-align: justify; width: 100%; white-space: normal">Please do not share your Matrix password, Matrix passcode, OTP, Credit/Debit card number and CVV or any other confidential information with anyone even if he/she claims to be from Matrix. We advise our customers to completely ignore such communications.</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
              <td align="center"><img src="{{ PUBLIC_FOLDER_URL . 'images/blank.png' }}" alt="" style="display: block; font-size: 0em; border: 0px" width="10" height="24"></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table></body>
</html>
