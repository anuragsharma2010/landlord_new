<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('includes.front.head')
    <body>
        <!-- Header -->
        @include('includes.front.header')
        <!-- Main Content -->
        @yield('content')
    
        <!-- Footer -->
        @include('includes.front.footer')
        @include('includes.front.message')
    </body>
</html>
