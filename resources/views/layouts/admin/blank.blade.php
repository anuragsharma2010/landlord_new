<!DOCTYPE html>
<html lang="en">
    @include('includes.admin.head')
    @include('includes.admin.message')
    <body class="login-page">
        @yield('content')
        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.3.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        {!! Html::script( asset('admin/js/vendor/bootstrap.min.js')) !!}
        {!! Html::script( asset('admin/js/plugins.js')) !!}
        {!! Html::script( asset('admin/js/app.js')) !!}

        <!-- Load and execute javascript code used only in this page -->
        {!! Html::script( asset('admin/js/pages/readyLogin.js')) !!}
        <script>
            $(function () {
                ReadyLogin.init();
            });
        </script>
        
        {!! Html::script( asset('admin/js/pages/readyReminder.js')) !!}
        <script>
            $(function () {
                ReadyReminder.init();
            });
        </script>
    </body>
</html>