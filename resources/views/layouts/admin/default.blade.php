<!DOCTYPE html>
<html lang="en">
    @include('includes.admin.head')
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="loading-cntant" >
                <div class="loader"></div>
            </div>

            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>

            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                @include('includes.admin.altsidebar')
                @include('includes.admin.sidebar')

                <!-- Main Container -->
                <div id="main-container">
                    @include('includes.admin.header')
                    @include('includes.admin.message')
                    @yield('content')
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!--add custom scripts there-->
        <?php /*@include('includes.admin.footer-script')*/ ?>
    </body>
</html>