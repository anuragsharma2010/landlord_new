Dear {{$user->name}},<br>

You recently requested your password for your parking adminstrator account.
<br>
<b>Password : </b> {{$new_pass}} 

<p style=" font-size: 16px; font-weight: 200 ; color: #F5070B">You are advised to change the password at next login to ensure safety of your account and data.</p>
<strong>To login, please click</strong> <a href="{{url('/')}}" style="text-decoration: none ;">{{url('/')}}</a>

<p><strong>Note:</strong> In case you have not requested a password change / forgotten your password, please ensure that your account remains safe by logging in to check.</p>