@extends('layouts.front.home')
@section('content')
<section class="mid-bx">
    <div class="categories-bx">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="main-categories">
                        <div class="banner_menu">
                            <div class="col-md-3">
                                <div class="side_menu">
                                    <ul class="groupmenu">
                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-music"></i> 
                                                <span><span>Appliances</span></span>
                                            </a>
                                        </li>

                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-tv"></i> 
                                                <span><span>TV & Home Theater</span></span>
                                            </a>
                                            <ul class="cat-tree groupmenu-drop">
                                                <li class="item level1 nav-1 first">
                                                    <a class="menu-link" href=""><span>Foods</span></a>
                                                </li>
                                                <li class="item level1 nav-2">
                                                    <a class="menu-link" href=""><span>Drinks</span></a>
                                                </li>
                                                <li class="item level1 nav-3 last">
                                                    <a class="menu-link" href=""><span>Grocery &amp; Gourmet</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-mobile-phone"></i> 
                                                <span><span>Cell Phones</span></span>
                                            </a>
                                            <ul class="cat-tree groupmenu-drop">
                                                <li class="item level1 nav-1 first">
                                                    <a class="menu-link" href=""><span>Cell 1</span></a>
                                                </li>
                                                <li class="item level1 nav-2">
                                                    <a class="menu-link" href=""><span>Cell 2</span></a>
                                                </li>
                                                <li class="item level1 nav-3 last">
                                                    <a class="menu-link" href=""><span>Cell 3</span></a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-laptop"></i>
                                                <span><span>Computers & Tablets</span></span>
                                            </a>
                                        </li>

                                        <li class="item level0  level-top parent cat-tree" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-wrench"></i>
                                                <span><span>Tools and Hardware</span></span>
                                            </a>
                                        </li>

                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-map-pin"></i>
                                                <span><span>Toys, Games & Drones</span></span>
                                            </a>
                                        </li>

                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-book"></i>
                                                <span><span>Books & Audible</span></span>
                                            </a>
                                        </li>

                                        <li class="item level0  level-top parent" >
                                            <a class="menu-link" href="">
                                                <i class="menu-icon fa fa-stethoscope"></i>
                                                <span><span>Health, Fitness & Beauty</span></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="col-md-9">
                                <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#myCarousel2" data-slide-to="0" class="active">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </li>
                                        <li data-target="#myCarousel2" data-slide-to="1">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </li>
                                        <li data-target="#myCarousel2" data-slide-to="2">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </li>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="{{asset('public/front/img/banner1.jpg')}}" alt="Los Angeles" style="width:100%;">
                                        </div>

                                        <div class="item">
                                            <img src="{{asset('public/front/img/banner2.jpg')}}" alt="Chicago" style="width:100%;">
                                        </div>

                                        <div class="item">
                                            <img src="{{asset('public/front/img/banner2.jpg')}}" alt="New york" style="width:100%;">
                                            <div class="banner_matter">
                                                <h3>Fresh Vegetables</h3>
                                                <h4>We Supply FARM</h4>
                                                <p>Loream ipsum set amet condused Loream ipsum set amet condused set amet condused</p>
                                                <div class="shop">
                                                    <a href="">SHOP NOW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mid-bx">
    <div class="categories-bx">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="main-categories">
                        <ul class="text-center">
                            <li> 
                                <a class="active" href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon1.png')}}" alt="img-1">
                                    </i>
                                    <div class="menu-icon-txt active"> Fruit &  Vegetables </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon2.png')}}" alt="img-2">
                                    </i>
                                    <div class="menu-icon-txt"> Meat Fish & Poultry </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon3.png')}}" alt="img-3">
                                    </i>
                                    <div class="menu-icon-txt"> Bakery & Cookies </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon4.png')}}" alt="img-4">
                                    </i>
                                    <div class="menu-icon-txt"> Frozen & Ice cream </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon5.png')}}" alt="img-5">
                                    </i>
                                    <div class="menu-icon-txt"> Wine & Drinks </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon6.png')}}" alt="img-6">
                                    </i>
                                    <div class="menu-icon-txt"> Food  Cupboard </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon7.png')}}" alt="img-7">
                                    </i>
                                    <div class="menu-icon-txt"> Beverages </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon8.png')}}" alt="img-8">
                                    </i>
                                    <div class="menu-icon-txt"> Hot Foods </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon9.png')}}" alt="img-9">
                                    </i>
                                    <div class="menu-icon-txt"> Heath & Beauty </div>
                                </a>
                            </li>
                            
                            <li> 
                                <a href="#">
                                    <i class="menu-icon">
                                        <img src="{{asset('public/front/img/icon10.png')}}" alt="img-10">
                                    </i>
                                    <div class="menu-icon-txt"> Household </div>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="talk-new-product">
                        <div class="tbl-hder">
                            <h3>New Product</h3>

                            <nav>
                                <ul class="control-box pager">
                                    <li><a data-slide="prev" href="#myCarousel" class=""><i class="fa fa-chevron-left"></i></a></li>
                                    <li><a data-slide="next" href="#myCarousel" class=""><i class="fa fa-chevron-right"></i></a></li>
                                </ul>
                            </nav>

                        </div>
                        <div class="new-prouct">
                            <div class="carousel slide" id="myCarousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <ul class="thumbnails">
                                            <li class="col-md-2 col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="item">
                                        <ul class="thumbnails">
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p class="product_value">Loream Ipsum set</p>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>




                                <!-- /.control-box -->   

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div> 
</section>
<div class="categories-bx">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <section class="fruit_veg">
                    <div class="top_head">
                        <div class="left_section">
                            <h2 class="heading_fruit">Fruit & Vegetables</h2>
                        </div>
                        <div class="right_section">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#home">Apples</a></li>
                                <li><a data-toggle="pill" href="#menu1">Juice</a></li>
                                <li><a data-toggle="pill" href="#menu2">Bean Pie</a></li>
                                <li><a data-toggle="pill" href="#menu3">Celery</a></li>
                                <li><a data-toggle="pill" href="#menu4">Apples</a></li>
                                <li><a data-toggle="pill" href="#menu5">Juice</a></li>
                                <li><a data-toggle="pill" href="#menu6">Bean Pie</a></li>
                                <li><a data-toggle="pill" href="#menu7">Celery</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="veg_all">
                        <div class="veg_side">
                            <a href="">
                                <img src="{{asset('public/front/img/veg_side.jpg')}}" class="img-reaponsive">
                                <div class="veg_overlay">
                                    <h2>FRUIT & <br/>VEGETABLES</h2>
                                </div>
                            </a>
                            <div class="shop">
                                <a href="">SHOP NOW</a>
                            </div>

                        </div>

                        <div class="veg_side_right tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <ul class="thumbnails corner_left">
                                    <li class="col-sm-3 col-md-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu1">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$42.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu2">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$43.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu3">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$44.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu4">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$45.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu5">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$46.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu6">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$47.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu7">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$48.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="col-sm-3">
                                        <div class="single-items">
                                            <div class="thumbnail">
                                                <a href="#">
                                                    <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                    <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                </a>
                                            </div>
                                            <div class="product_matter">
                                                <h2>Loream Ipsum</h2>
                                                <p class="product_value">Loream Ipsum set</p>
                                                <p>$41.00</p>
                                                <div class="overlay-item">
                                                    <div class="quantity">
                                                        <div class="mobile_btn">
                                                            <div class="increase_btn">
                                                                <button class="decrease" value="-">-</button>
                                                            </div>
                                                            <input maxlength="12" value="1" class="input-text qty" type="text">
                                                            <div class="decrease_btn">
                                                                <button class="decrease" value="+">+</button>
                                                            </div>
                                                        </div>
                                                        <div class="add_cart">
                                                            <a href="" data-toggle="tooltip" title="Add to cart">
                                                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wish_list">
                                                        <a href="" data-toggle="tooltip" title="Add to wish list">
                                                            <i class="fa fa-heart" aria-hidden="true"></i>

                                                        </a>
                                                    </div>
                                                    <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                    <span>Add to Cart</span></button> -->
                                                </div>
                                            </div>

                                            <div class="overlay-eye">
                                                <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                    <div>
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="categories-bx">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <section class="drink_wine">
                    <div class="drink_wine_block">
                        <div class="col-md-3 col-sm-3">
                            <div class="drink_main">
                                <div class="top_drink">
                                    <P>Drink & Wine</P>
                                </div>
                                <div class="main-categories">
                                    <ul class="text-center nav nav-pills">
                                        <li class=""> 
                                            <a class="" data-toggle="pill" href="#drink1">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice1.jpg')}}" alt="img-1">
                                                </i>
                                                <div class="menu-icon-txt"> Ice Cream </div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#drink2">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice2.jpg')}}" alt="img-2">
                                                </i>
                                                <div class="menu-icon-txt"> Treats </div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#drink3">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice3.jpg')}}" alt="img-3">
                                                </i>
                                                <div class="menu-icon-txt"> Fruit</div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#drink4">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice4.jpg')}}" alt="img-4">
                                                </i>
                                                <div class="menu-icon-txt"> Hard Soda </div>
                                            </a>
                                        </li>
                                        <li>  
                                            <a data-toggle="pill" href="#drink5">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice5.jpg')}}" alt="img-5">
                                                </i>
                                                <div class="menu-icon-txt"> Domestic</div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#drink6">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice6.jpg')}}" alt="img-6">
                                                </i>
                                                <div class="menu-icon-txt"> Imported </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="wine_product">
                                <div class="tab-content">
                                    <div id="drink1" class="tab-pane fade in active">
                                        <ul  class="thumbnails">
                                            <li class="col-md-4 col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-md-4 col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-md-4 col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="drink2" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$47.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="drink3" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$45.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="drink4" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$47.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="drink5" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="drink6" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$49.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3">
                            <div class="best-seller">
                                <div class="top_seller">
                                    <p>Best Seller</p>
                                </div>
                                <div class="bottom_seller">
                                    <marquee direction="up" height="400" onmouseover="this.stop()" onmouseout="this.start()">   
                                        <ul class="pro_list">
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </marquee>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="categories-bx">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <section class="drink_wine">
                    <div class="drink_wine_block">
                        <div class="col-md-3 col-sm-3">
                            <div class="drink_main">
                                <div class="top_drink">
                                    <P>Grocery & Gourmet</P>
                                </div>
                                <div class="main-categories">
                                    <ul class="text-center nav nav-pills">
                                        <li class=""> 
                                            <a class="" data-toggle="pill" href="#grocery1">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice1.jpg')}}" alt="img-1">
                                                </i>
                                                <div class="menu-icon-txt"> Ice Cream </div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#grocery2">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice2.jpg')}}" alt="img-2">
                                                </i>
                                                <div class="menu-icon-txt"> Treats </div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#grocery3">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice3.jpg')}}" alt="img-3">
                                                </i>
                                                <div class="menu-icon-txt"> Fruit</div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#grocery4">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice4.jpg')}}" alt="img-4">
                                                </i>
                                                <div class="menu-icon-txt"> Hard Soda </div>
                                            </a>
                                        </li>
                                        <li>  
                                            <a data-toggle="pill" href="#grocery5">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice5.jpg')}}" alt="img-5">
                                                </i>
                                                <div class="menu-icon-txt"> Domestic</div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a data-toggle="pill" href="#grocery6">
                                                <i class="menu-icon">
                                                    <img src="{{asset('public/front/img/ice6.jpg')}}" alt="img-6">
                                                </i>
                                                <div class="menu-icon-txt"> Imported </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="wine_product">
                                <div class="tab-content">
                                    <div id="grocery1" class="tab-pane fade in active">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="grocery2" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$47.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="grocery3" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$45.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="grocery4" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$47.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="grocery5" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div id="grocery6" class="tab-pane fade">
                                        <ul  class="thumbnails">
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$49.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/product1.jpg')}}" alt="">
                                                            <img class="img-reaponsive product-new" src="{{asset('public/front/img/product1.1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product_matter">
                                                        <h2>Loream Ipsum</h2>
                                                        <p>$41.00</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                            <!-- <button data-toggle="tooltip" data-original-title="Add to Cart" type="button" title="" class="button btn-cart">
                                                            <span>Add to Cart</span></button> -->
                                                        </div>
                                                    </div>

                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3">
                            <div class="best-seller">
                                <div class="top_seller">
                                    <p>Best Seller</p>
                                </div>
                                <div class="bottom_seller">
                                    <marquee direction="up" height="400" onmouseover="this.stop()" onmouseout="this.start()">  
                                        <ul class="pro_list">
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="bottom-inner">
                                                    <div class="img_product">
                                                        <img src="{{asset('public/front/img/seller1.jpg')}}" class="img-responsive"/>
                                                    </div>
                                                    <div class="product-name">
                                                        <p>Mixed Berry</p>
                                                        <p class="price">$51</p>
                                                        <div class="overlay-item">
                                                            <div class="quantity">
                                                                <div class="mobile_btn">
                                                                    <div class="increase_btn">
                                                                        <button class="decrease" value="-">-</button>
                                                                    </div>
                                                                    <input maxlength="12" value="1" class="input-text qty" type="text">
                                                                    <div class="decrease_btn">
                                                                        <button class="decrease" value="+">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="add_cart">
                                                                    <a href="" data-toggle="tooltip" title="Add to cart">
                                                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="wish_list">
                                                                <a href="" data-toggle="tooltip" title="Add to wish list">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>

                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="overlay-eye">
                                                        <a href="" data-toggle="tooltip" title="Quick Shop" class="eye">
                                                            <div>
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </marquee>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="categories-bx">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="hot_brands">
                    <div class="talk-new-product">
                        <div class="tbl-hder">
                            <h3>Hot Brands</h3>

                            <nav>
                                <ul class="control-box pager">
                                    <li><a data-slide="prev" href="#myCarousel1" class=""><i class="fa fa-chevron-left"></i></a></li>
                                    <li><a data-slide="next" href="#myCarousel1" class=""><i class="fa fa-chevron-right"></i></a></li>
                                </ul>
                            </nav>

                        </div>
                        <div class="new-prouct">
                            <div class="carousel slide" id="myCarousel1">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <ul class="thumbnails">
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand2.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand2.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand2.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>


                                        </ul>
                                    </div>
                                    <div class="item">
                                        <ul class="thumbnails">
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="col-sm-2">
                                                <div class="single-items">
                                                    <div class="thumbnail">
                                                        <a href="#">
                                                            <img class="img-reaponsive product1" src="{{asset('public/front/img/brand1.png')}}" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>


                                        </ul>
                                    </div>
                                </div>




                                <!-- /.control-box -->   

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="categories-bx">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="categories_pro">
                    <div class="col-md-3">
                        <div class="categories_sec">
                            <div class="cate_img">
                                <i class="pe-7s-piggy" aria-hidden="true"></i>
                            </div>
                            <div class="cate_matter">
                                <h4>+15.000 products</h4>
                                <p class="text hidden-sm">Sports , Movies etc</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="categories_sec">
                            <div class="cate_img">
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <div class="cate_matter">
                                <h4>More then 20 Categories</h4>
                                <p class="text hidden-sm">Fashion , Electronics, Book</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="categories_sec">
                            <div class="cate_img">
                                <i class="fa fa-car" aria-hidden="true"></i>
                            </div>
                            <div class="cate_matter">
                                <h4>Free Shipping</h4>
                                <p class="text hidden-sm">Free shipping on the world</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="categories_sec">
                            <div class="cate_img">
                                <i class="fa fa-user-secret" aria-hidden="true"></i>
                            </div>
                            <div class="cate_matter">
                                <h4>Refund in 2 days</h4>
                                <p class="text hidden-sm">100% guarantee</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop