@extends('layouts.front.home')
@section('content')
<section class="mid-bx product-mid">
    <div class="categories-bx">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="side_menu">
                        <h3>All Categories</h3>
                        <ul class="groupmenu">
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>New & Clearance...</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Own Label & Diet</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Beer, Cider...</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Biscuits</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Bread & Cakes</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Confectionery</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Crisps, Snacks & Dips</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Food & Drink...</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Fresh Food</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Frozen Food</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Greengrocery</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Grocery - Catering</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Grocery - Retail</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Health, Beauty & Baby</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Hot Drinks</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Household, Cleaning...</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Meat, Fish & Poultry</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Non-Food</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Pet Food</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Seasonal</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Soft Drinks</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Spirits & Liqueurs</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Tobacco & Cigarettes</span></span></a>

                            </li>
                            <li class="item level0  level-top parent" >
                                <a class="menu-link" href=""><i class="menu-icon fa fa-music"></i> <span><span>Wine</span></span></a>

                            </li>

                        </ul></div>
                    <div class="side_menu">
                        <h3>Search Brands</h3>
                        <input type="text" name="" placeholder="Type brand here">
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="sublist-details clearfix">
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                        <li><a href="#"><img src="{{asset('public/front/img/New Lines.jpg')}}"> New & Clearance L...</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@stop