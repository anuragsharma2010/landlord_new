@extends('layouts.front.home')
@section('content')
<section class="about-wrapper pt-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-page-title text-center py-sm-5">
                    <h2 class="font45 font2 color20 fontSemiBold">{{$title}}</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pagination-outer">
                    <div class="pagi-inner">
                        @include('includes.front.breadcrumb')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us-content pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="keep-in-touch">
                        <h2 class="font45 color20 fontBold keep-line pb-2">Get In Touch</h2>
                        {!! Form::open(['route'=>'front.contact.store','files'=>true, 'class'=>'contact-details']) !!}
                            <div class="form-row">
                                <div class="form-group col-md-6 pl-0 pr-3">
                                    {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name']) !!}
                                    <div class="error">{{ $errors->first('first_name') }}</div>
                                </div>
                                <div class="form-group col-md-6 pl-0 ">
                                    {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}
                                    {!! Form::hidden('status',1,['class'=>'form-control']) !!}
                                    <div class="error">{{ $errors->first('last_name') }}</div>
                                </div>
                                 <div class="form-group col-md-6 px-0 pr-3">
                                    {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email']) !!}
                                    <div class="error">{{ $errors->first('email') }}</div>
                                </div>
                                <div class="form-group col-md-12 px-0">
                                    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Message']) !!}
                                    <div class="error">{{ $errors->first('description') }}</div>
                                </div>
                                <div class="form-group col-md-12 px-0">
                                    {!! Form::submit(trans('admin.SUBMIT'),['class' => 'btn-one font1 mt-2 '])!!}
                                </div>    
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="keep-in-touch">
                        <h2 class="font45 color20 fontBold keep-line pb-2">Contact Info</h2>
                        <div class="corporate-office">
                            <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-phone font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Call us now</small>
                                    <a href="tel:516-707-3773" class="font18 color20 font1">{{config('settings.CONFIG_CONTACT_SUPPORT_PHONE')}}</a>
                                </div>
                            </div>  
                        </div>
                        <div class="corporate-office mt-4">
                           <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-envelope font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Send us message</small>
                                    <a href="mailto:info@mymatrixrent.com" class="font18 color20 font1">{{config('settings.CONFIG_CONTACT_SUPPORT_EMAIL')}}</a>
                                </div>
                            </div>  
                       </div>
                       <div class="corporate-office mt-4">
                           <div class="add-info d-flex">
                                <div class="icons pr-3 pt-2"><i class="fa fa-map-marker font18" aria-hidden="true"></i></div>
                                <div class="info">
                                    <small class="d-block font16 font1">Visit our office</small>
                                   <address class="font18 color20 font1">{{config('settings.CONFIG_CONTACT_SUPPORT_TITLE')}}</address>
                                </div>
                            </div>  
                       </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@stop