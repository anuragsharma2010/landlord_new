@extends('layouts.front.home')
@section('content')

<section class="about-wrapper pt-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-page-title text-center py-sm-5">
                    <h2 class="font45 font2 color20 fontSemiBold">{{$title}}</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pagination-outer">
                    <div class="pagi-inner">
                        @include('includes.front.breadcrumb')
                    </div>
                </div>
            </div>
        </div>
        <div class="row position-relative">
            <div class="col-md-8 order-first">
                <div class="the-facts">
                    <div class="facts-title">
                        <h2 class="font45 font2 color20 fontSemiBold comman-t">{{$title}}</h2>
                        <ul class="spcl-points">
                            <li class="font1">
                                {{$subtitle}}
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="about-img-outer">
                <!-- <div class="about-img">
                    <img src="{{asset('public/front/images/abt-img.png')}}" alt="">
                </div> -->
            </div>
        </div>
    </div>
</section>

@stop