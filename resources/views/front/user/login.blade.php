@extends('layouts.front.home')
@section('content')
<section>
    <div class="container form_signup">
        <center>
            <div class="col-xs-12 col-sm-12 col-md-12 signup_form">
                <div class="col-sm-6 logn_heading">
                    <h1>login</h1>
                    <p>Don't have an account? <span>Create your account</span></p>                          
                    <div class="col-md-12 one_spc">      
                        <input type="text" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label>USER NAME*</label>
                    </div>
                    <div class="col-md-12 one_spc">      
                        <input type="text" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label>PASSWORD*</label>
                    </div>
                    <p>Forgot Password?  <button type="button" class="btn btn-success pull-right">LOGIN</button></p>
                    <div class="food_pic"><center><img src="{{asset('public/front/img/foods_pic.png')}}" class="img-responsive"></center></div>

                </div>
                <div class="col-sm-6 logbox_right_site">
                    <div class="triangle-down"></div>
                    <h4>Lorem Ipsum is simply 
                        dummy text</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

        </center>
    </div>
</section>
@stop