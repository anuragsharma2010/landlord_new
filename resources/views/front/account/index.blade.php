@extends('layouts.front.home')
@section('content')
<section class="about-wrapper pt-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-page-title text-center py-sm-5">
                    <h2 class="font45 font2 color20 fontSemiBold">{{$title}}</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pagination-outer">
                    <div class="pagi-inner">
                        @include('includes.front.breadcrumb')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-wrp end-user padding-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <div class="ser-left">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                          <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">My Profile</a>
                          <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">My Applications</a>
                          <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">My Documents</a>
                          <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <div class="row">
                            <!-- <div class="col-md-12">
                                <div class="edit-btn">
                                    <button class="btn-one chngps mx-0 font1">
                                            Edit
                                    </button>
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="ac-form">
                                    <div class="keep-in-touch">
                                        {!! Form::model($user,['method'=>'patch','route'=>['front.profile_update',$user->slug],'files'=>true, 'class'=>'contact-details']) !!}
                                            <div class="form-row">
                                                <div class="form-group col-md-6 pl-0 pr-3">
						                            {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name']) !!}
						                            <div class="error">{{ $errors->first('first_name') }}</div>
                                                </div>
                                                <div class="form-group col-md-6 px-0">
                                                    {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}
						                            <div class="error">{{ $errors->first('last_name') }}</div>
                                                </div>
                                                 <div class="form-group col-md-6 pl-0 pr-3">
                                                    {!! Form::text('email',null,['id'=>'email','class'=>'form-control','placeholder'=>trans('admin.EMAIL'), 'readonly'=>true]) !!}
                            						<div class="error">{{ $errors->first('email') }}</div>
                                                </div>
                                                <div class="form-group col-md-6 px-0">
                                                    {!! Form::text('contact',null,['id'=>'contact','minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE'), 'readonly'=>false]) !!}
                            						<div class="error">{{ $errors->first('contact') }}</div>
                                                </div>
                                                <div class="form-group typm col-md-6 pl-0 pr-3">
                                                	{!! Form::text('dob',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control dateofbirth','placeholder'=>trans('admin.DOB'), 'format'=>'MM/dd/yyyy']) !!}
                                                	<label for="date" class="dtp"></label>
                            						<div class="error">{{ $errors->first('dob') }}</div>
                                                    <!-- <input type="date" required placeholder="Your Date" class="form-control"> -->
                                                </div>
                                                <div class="form-group col-md-6 px-0">
                                                    <!-- <input type="text" required placeholder="John" class="form-control"> -->
                                                    <?php $marital_status_list = Config::get('global.marital_status'); ?>
						                            {!! Form::select('marital_status', $marital_status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
						                            <div class="error">{{ $errors->first('marital_status') }}</div>
                                                    <!-- <label for="m-sts" class="ar-st"></label> -->
                                                </div>
                                                <div class="form-group col-md-6 pl-0 pr-3">
                                                    {!! Form::text('social_security_number',null,['minlength'=>1,'class'=>'form-control','placeholder'=>trans('admin.SOCIAL_SECURITY_NUMBER')]) !!}
                            						<div class="error">{{ $errors->first('social_security_number') }}</div>
                                                </div>
                                                <div class="form-group col-md-6 px-0">
                                                    {!! Form::text('zip_code',null,['minlength'=>1,'class'=>'form-control','placeholder'=>'Zip Code']) !!}
                            						<div class="error">{{ $errors->first('zip_code') }}</div>
                                                </div>
                                                <div class="form-group col-md-6 pl-0 pr-3">
                                                    {!! Form::select('country_id', [''=>'--- Select Country ---'] + $countries, null, ['class' => 'form-control select2 autocomplete']) !!}
                                					<div class="error">{{ $errors->first('country_id') }}</div>
                                                </div>
                                                <div class="form-group col-md-6 px-0">
                                                    {!! Form::select('state_id', $states, null, ['class' => 'form-control select2 autocomplete', 'placeholder'=>'--- Select State ---']) !!}
                                					<div class="error">{{ $errors->first('state_id') }}</div>
                                                </div>
                                                 <div class="form-group col-md-6 pl-0 pr-3">
                                                    {!! Form::select('city_id', [''=>'--- Select City ---'] + $cities, null, ['class' => 'form-control select2 autocomplete']) !!}
                                					<div class="error">{{ $errors->first('city_id') }}</div>
                                                </div>
                                                 
                                                <div class="form-group col-md-12 px-0">
                                                    <div class="gov-id">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="gov-inner">
                                                                    <h6 class="font18 font1">Government ID</h6>
                                                                    <div class="document">
                                                                        <div class="demo-box">
                                                                        	@if(!empty($user->uploadDocument->doc_name) && $user->uploadDocument->doc_name=='gov_document')
													                            <img src="{{ GOVID_IMAGE_URL . $user->uploadDocument->file_path }}" alt="Government ID" />
													                        @else
													                            <br> &nbsp;No ID Uploaded
													                        @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="upload text-right">
                                                                    <input type="file" name="upload_doc" id="upld" class="upin" accept ="image/*">
                                                                    <label for="upld" class="font1">Browse</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="pr-btns pt-sm-5 d-flex justify-content-center">
	                                            <a href="javascript:void(0);" class="btn-one chngps font1 mt-2 align-self-cente" data-toggle="modal" data-target="#change-profile-pass">
	                                                 Change Password
	                                            </a>
	                                            {!! Form::submit('Update',['class' => 'btn-one mx-0 font1'])!!}
                                        	</div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="application-content table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th class="font2 fontSemiBold color20 font22">Application Title</th>
                                             <th class="font2 fontSemiBold color20 font22">Date</th>
                                              <th class="font2 fontSemiBold color20 font22" colspan="2">Status</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Application Name1</td>
                                                <td>22 oct,2019</td>  
                                                <td>
                                                    <a href="" class="gren">Accept</a>
                                                </td>
                                                <td>
                                                    <div class="edit-btn">
                                                        <a href="" class="text-center btn-one chngps mx-0 font1">
                                                                View
                                                        </a>
                                                    </div>
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td>Application Name1</td>
                                                <td>22 oct,2019</td>  
                                                <td>
                                                    <a href="" class="rej">Reject</a>
                                                </td>
                                                <td>
                                                    <div class="edit-btn">
                                                        <a href="" class="text-center btn-one chngps mx-0 font1">
                                                                View
                                                        </a>
                                                    </div>
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td>Application Name1</td>
                                                <td>22 oct,2019</td>  
                                                <td>
                                                    <a href="" class="rej">Reject</a>
                                                </td>
                                                <td>
                                                    <div class="edit-btn">
                                                        <a href="" class="text-center btn-one chngps mx-0 font1">
                                                                View
                                                        </a>
                                                    </div>
                                                </td>   
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tb-btn text-center">
                                    <a href="lease-info.html" class="btn-one mx-0 font1">
                                        New Application
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="row">
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Personal Information & Government ID</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div>
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Income, Employment History and Employment Letter</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div>
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Tax Documents</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div> 
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Bank Information & Transaction History</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div>
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Residence History</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div> 
                                   <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Pay Stubs</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div>
                                    <div class="col-md-9 mb-2">
                                       <div class="id-names">
                                           <p class="font1 font18">Credit Report</p>
                                       </div>
                                   </div>
                                   <div class="col-md-3 mb-2">
                                       <div class="id-links">
                                           <div class="edit-btn">
                                                <a href="" class="text-center btn-one chngps mx-0 font1">
                                                        View
                                                </a>
                                            </div>
                                       </div>
                                   </div>   
                               </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    $(function() {
        $('select[name=country_id]').change(function() {

            //alert($(this).val());

            var url = '{{ url("country") }}'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name=state_id]');

                select.empty();
                $('form select[name=city_id]').empty();
                $('form select[name=city_id]').append('<option value="">--- Select City ---</option>');
                select.append('<option value="">--- Select State ---</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });

        $('select[name=state_id]').change(function() {

            //alert($(this).val());

            var url = '{{ url("state") }}'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name=city_id]');

                select.empty();
                select.append('<option value="">--- Select City ---</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });
    });
</script>
@stop