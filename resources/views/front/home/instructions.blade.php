@extends('layouts.front.home')
@section('content')
    <div class="clearfix"></div>
    <section class="choose-us-sec">
    	<div class="container">
            <div class="row"> 
                <div class="choose-us-dtl">
                    <div class="col-md-12 col-sm-12">
                        <h3 class="title">Instructions</h3>
                        <p>For your review we have uploaded the Matrix app on iTunes (TestFlight) and you can install it by first installing TestFlight App from App Store (Link to the App is given below) <a target="_blank" href="https://apps.apple.com/us/app/testflight/id899247664">https://apps.apple.com/us/app/testflight/id899247664</a></p>
                        <p>After installing the TestFlight App, you can click on the Link below and the beta version of Matrix app will be installed in your iPhone.<br><a target="_blank" href="https://testflight.apple.com/join/f09tlA3H">Matrix Application Link for Apple Users</a> </p>
                        <p>Also, if you have an Android phone here is the APK Link of the Matrix Application for Android users. Just Download the APK on the device by clicking on the link given below:<a target="_blank" href="https://drive.google.com/open?id=1cAK3KLU53i6Mp9SZl4vF14xYHP3Ry6EI">Matrix Application Link for Android Users</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@stop