@extends('layouts.front.home')
@section('content')
    <div class="clearfix"></div>
   <!--  <section class="banner">
        <div class="main-bnr-img">
            <img src="{{ PUBLIC_FOLDER_URL . 'front/images/banner-sah.png' }}" alt="">
        </div>
        <div class="app-dwd-info">
            <div class="apps-craft-pstn-rel">
                <div id="apps_craft_animation">
                    <div class="apps_craft_layer1">
                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/app-layout-1.png' }}" alt="img-1">
                    </div>
                    <div class="apps_craft_layer">
                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/app-layout-2.png' }}" alt="img-1">
                    </div>
                </div>
            </div>
        </div>
        <div class="app-load-dtl">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 cl-sm-6">
                        <div class="app-dlaod-info">
                            <h2>
                            <span> STAY LYNKED TO PEOPLE AND FOODS.  </span>
                            <p>Download Now !</p></h2>
                            <div class="dload-bnt-prt">
                                <div class="button raised hoverable">
                                    <div class="anim"></div>
                                    <img src="{{ PUBLIC_FOLDER_URL . 'front/images/appstore.png' }}">
                                </div>
                            </div>
                            <div class="dload-bnt-prt">
                                <div class="button raised hoverable">
                                    <div class="anim"></div>
                                    <img src="{{ PUBLIC_FOLDER_URL . 'front/images/googleplay.png' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pull-right">
                        <div class="app-demo">
                            <div class="app-demo-scn">
                                <img src="{{ PUBLIC_FOLDER_URL . 'front/images/app-demo-img.png' }}" alt="img">
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </section> -->
    <div class="clearfix"></div>

    <section class="choose-us-sec">
    	<div class="container-fluid">
        	<!-- <div class="row">
            	<h2 class="title-choose-us">why choose us</h2>
                <div class="divider"></div>
            </div> -->
            
            <div class="row">   
         		<div class="choose-us-dtl">
                    <div class="col-md-7 col-sm-12">
                    	<div class="app-screen">
                        	<div class="cascade-slider_container" id="cascade-slider">
                                <div class="cascade-slider_slides">
                                    <div class="cascade-slider_item">
                                        <!-- <h3>Pic 1</h3>-->
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-1.jpg' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                    
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-2.jpg' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                    
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-3.jpg' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                    
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-4.jpg' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                    
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-5.png' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-6.jpg' }}" alt="">
                                    </div>
                                    <div class="cascade-slider_item">
                                
                                        <img src="{{ PUBLIC_FOLDER_URL . 'front/images/slider-7.jpg' }}" alt="">
                                    </div>        
                                </div>

                                <span class="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev"><i class="fa fa-chevron-left"></i></span>
                                <span class="cascade-slider_arrow cascade-slider_arrow-right" data-action="next"><i class="fa fa-chevron-right"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="apps-craft-why-choose">
                        	<ul>
                            	<li>
                                	<!-- <div class="apps-why-choose-icn">
                                    	<i class="why-choose-icn">
                                        	<img src="{{ PUBLIC_FOLDER_URL . 'front/images/user-friendaly-icn.png' }}" alt="img">
                                         	<span class="apps-dash-border"></span> 
                                        </i>
                                    </div> -->
                                    <div class="apps-why-choose-txt">
                                    	<h3> Organize Event </h3>
                                        <p>Organize your gatherings using Matrix Application and order a mélange of international foods from our network of award-winning and dedicated caterers.</p>
                                    </div>
                                </li>
                                <li>
                                	<!-- <div class="apps-why-choose-icn">
                                    	<i class="why-choose-icn">
                                        	<img src="{{ PUBLIC_FOLDER_URL . 'front/images/calendar-icnn.png' }}" alt="img">
                                         	<span class="apps-dash-border"></span> 
                                        </i>
                                    </div> -->
                                    <div class="apps-why-choose-txt">
                                    	<h3> Order Food </h3>
                                        <p>Placing INDIVIDUAL or GROUP OFFICE LUNCH as well as CATERING ORDERS is literally 1,2,3 with sumptuous results.</p>
                                    </div>
                                </li>
                                <li>
                                	<!-- <div class="apps-why-choose-icn">
                                    	<i class="why-choose-icn">
                                        	<img src="{{ PUBLIC_FOLDER_URL . 'front/images/maping-icn.png' }}" alt="img">
                                         	<span class="apps-dash-border"></span> 
                                        </i>
                                    </div> -->
                                    <div class="apps-why-choose-txt">
                                    	<h3>Join Event</h3>
                                        <p>Join the private or public lunch events and stay linked with people and foods in the Matrix network.</p>
                                    </div>
                                </li>
                                <li>
                                	<!-- <div class="apps-why-choose-icn">
                                    	<i class="why-choose-icn">
                                        	<img src="{{ PUBLIC_FOLDER_URL . 'front/images/basket-icn.png' }}" alt="img">
                                         	
                                        </i>
                                    </div> -->
                                    <div class="apps-why-choose-txt">
                                    	<h3>Munch More, Pay Less</h3>
                                        <p>Our Mission is to <a target="blank" href="{{ PUBLIC_FOLDER_URL . 'front/images/Matrix-banner.png' }}">bring back the lost art of socializing over meals.</a> Steep discounts are given to Shared-Lunch Group Orders exalting in tastes but muted in prices!</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="app-dlaod-info clearfix">
        <a href="{{ route('instructions') }}">Test Flight Matrix iOS or Android App</a>
        <div class="app-download-box clearfix">
        <div class="dload-bnt-prt">
            <div class="button hoverable">
                <div class="anim"></div>
                <a href="https://testflight.apple.com/join/f09tlA3H" target="_blank"><img src="{{ PUBLIC_FOLDER_URL . 'front/images/appstore.png' }}"></a>
            </div>
        </div>
        <div class="dload-bnt-prt">
            <div class="button hoverable">
                <div class="anim"></div>
                <a href="https://drive.google.com/open?id=1cAK3KLU53i6Mp9SZl4vF14xYHP3Ry6EI" target="_blank"><img src="{{ PUBLIC_FOLDER_URL . 'front/images/googleplay.png' }}"></a>
            </div>
        </div>
        </div>
    </div>
@stop