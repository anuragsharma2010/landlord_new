@extends('layouts.front.home')
@section('content')
    <div class="clearfix"></div>
    <section class="choose-us-sec">
    	<div class="container">
            <div class="row"> 
                <div class="choose-us-dtl terms-privacy">
                    <div class="col-md-12 col-sm-12">
                        <h3>FRESH &amp; FAIR</h3>
                        <p>We've been around the food industry long enough to know that there's no harder job for the same pay than working in a restaurant kitchen. That's why we hardly find any Americans in those jobs. People who work in commercial kitchens endure long hours with tough working conditions while getting a paycheck that doesn't even cover monthly rent. Most have no choice but to juggle a second job to barely survive in the Silicon Valley. To our despair, technologies are now deployed to further enslave these food makers. Food ordering apps collect food orders and shove them to restaurant for 30% commission. Restaurants then push these orders to the kitchen workers to essentially enslaving them to produce more and more for the same pay. In modern day slavery you don't need a whip or a chain. All you need is an app. As pioneering engineers in ecommerce we feel a special obligation to lend our technical resources to help food creators get a fair shake, to empower them.</p>

                        <p>As a matter of fact, we will empower everyone equally in our Matrix network of event organizers (office lunches or special events), event participants (office workers, event attendees), caterers and food creators:</p>

                        <p>1/ By drawing all revenue from our food service activities and zero dollar from advertising we don’t have to mine any personal information. Our members can safely share their career or hobby interestsfor the sole purpose of getting to know each other before coming to an event. Organizers can promote their events to the right people without resorting to spam. There is absolutely no fee associated with event organizing activities.</p>

                        <p>2/ Despite our extensive technical training we actually have more experiences in the food sector as well as a unique analytical perspective of the industry. We distill all that into the <strong>fresh &amp; fair Matrix</strong>. For a 20% commission on the food orders we not only help our catering partners significantly extend their market share but also reduce their operating cost by 30% (-15% labor, -10% cost of supplies leveraging on purchasing power of the network, -5% on food waste with efficient procedures). We take 20% but route back 5% to the people who actually create the wonderful foods(the kitchen staffs and not restaurant owners). We also heavily promote tipping to food makers and delivery associates. The combined 5% plus tips would more than double their salaries. We empower everyone with more mobility, flexibility, and opportunities in the Matrix network. Mutually beneficial activities always cement the foundation for lasting relationships (hence our name).</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@stop