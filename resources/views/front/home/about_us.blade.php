@extends('layouts.front.home')
@section('content')
    <div class="clearfix"></div>
    <section class="choose-us-sec">
    	<div class="container">
            <div class="row"> 
                <div class="choose-us-dtl terms-privacy">
                    <div class="col-md-12 col-sm-12">
                        <h3>About Us</h3>
                        <p>Matrix is a platform for event organizers, attendees and caterers. Events range from daily office lunches to concerts or conferences. {!! Html::decode(Html::link(route('fresh_fair'),"Our network greatly enhances the mutually beneficial relationships among participants")) !!}, creating lasting connections.</p>

                        <p>Our mission is to bring back the lost art of socializing over meals. With mobile apps, mobile kitchens, traveling buffet tables, and riveting international fares we are pioneering in bringing “virtual cafeteria” to offices. We’ll go to places where lunch gathering had never been with manic energy not seen since Lewis & Clark.</p>

                        <p>We’ve been at the frontiers before. Matrix is found by the team who started the iconic food truck <a href="{{ PUBLIC_FOLDER_URL . 'front/images/MIT_Truck1.JPG' }}" target="_blank">MoMoGoose at MIT</a> in 1990 and the world’s first online supermarket <a href="https://youtu.be/oznsDvPMW1Q" target="_blank">https://youtu.be/oznsDvPMW1Q</a> in 1993.</p>

                        <p>Hook us up to your outpost. We have great foods and more than willing to travel!</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@stop