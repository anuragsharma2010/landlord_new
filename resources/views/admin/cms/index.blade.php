@extends('layouts.admin.default')
@section('content')  
<style type="text/css">
    .search_table{
        width: 100%
    }
    .td_class{
        width: 20%;
    }
</style>
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1>{{$pageTitle}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            @include('includes.admin.breadcrumb')
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::open(['method'=>'delete','method'=>'get','route'=>['admin.cms.index']]) !!}                         
                <table class="search_table">
                    <tbody>
                        <tr>
                            <td class="td_class">{!! Form::text('title', $form_data['title'], ['class'=>'form-control', 'placeholder'=>'title']) !!}</td>
                            <td class="td_class">{!! Form::text('description', $form_data['description'], ['class'=>'form-control', 'placeholder'=>'Description']) !!}</td>
                            <td><button class="btn btn-primary" type="submit">Search</button></td>
                            <td>
                                <h3 class="pull-right">  
                                    {!!  Html::decode(Html::link(route('admin.cms.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}
                                </h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
            {!! Form::close() !!}

            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%">@sortablelink('title', trans('admin.TITLE'))</th>
                            <th width="45%">@sortablelink('description', trans('admin.DESCRIPTION'))</th>
                            <th width="12%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                            <th width="12%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                            <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$cmslist->isEmpty())
                            @foreach ($cmslist as $cms)
                                <tr>
                                    <td>{{ ucfirst($cms->title) }}</td>
                                    <td>{{ str_limit($cms->description, 80, '...') }}</td>
                                    <td>{{ date_val($cms->created_at,MDY_FORMATE ) }}</td>
                                    <td>{{ date_val($cms->updated_at,MDY_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($cms->status == 1)
                                            {!!  Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @else
                                            {!!  Html::decode(Html::link(route('admin.cms.status_change',['id' => $cms->id,'status'=>$cms->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @endif

                                        {!!  Html::decode(Html::link(route('admin.cms.edit', $cms->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                        {!!  Html::decode(Html::link(route('admin.cms.delete', $cms->slug),"<i class='fa  fa-trash'></i>",['class'=>'btn btn-danger','data-toggle'=>'tooltip','title'=>trans('admin.DELETE')])) !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {!! $cmslist->appends(Input::all('page'))->render() !!}
            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
@stop