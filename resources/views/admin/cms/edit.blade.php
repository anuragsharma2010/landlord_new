@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($cms,['method'=>'patch','route'=>['admin.cms.update',$cms->id],'files'=>true]) !!}
            <div class="block full">
               
                <div class="row">

                    <div class="col-md-12">

                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.TITLE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('title',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE')]) !!}
                            <div class="error">{{ $errors->first('title') }}</div>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-12 ">
                            {!! Form::label(trans('admin.DESCRIPTION'),null,['class'=>'required_label    ']) !!}
                            {!! Form::textarea('description',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.DESCRIPTION')]) !!}
                            <div class="error">{{ $errors->first('description') }}</div>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'required_label    ']) !!}
                            {!! Form::textarea('meta_keywords',null,['class'=>'form-control ','placeholder'=>trans('admin.META_KEYWORDS')]) !!}
                            <div class="error">{{ $errors->first('meta_keywords') }}</div>
                        </div><!-- /.form-group -->  
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'required_label']) !!}
                            {!! Form::textarea('meta_description',null,['class'=>'form-control ','placeholder'=>trans('admin.META_DESCRIPTION')]) !!}
                            <div class="error">{{ $errors->first('meta_description') }}</div>
                        </div><!-- /.form-group -->
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.META_TITLE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('meta_title',null,['class'=>'form-control ','placeholder'=>trans('admin.META_TITLE')]) !!}
                            <div class="error">{{ $errors->first('meta_title') }}</div>
                        </div><!-- /.form-group --> 
                        <div class="form-group col-md-6">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                        </div><!-- /.form-group -->

                    </div><!-- /.col -->

                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
                
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->

@stop

