@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block full">
               
                <div class="row">

                    <div class="col-md-12">

                        <div class="row form-group">
                            <div class="col-md-2">
                                {!! Form::label(trans('admin.LANDLORD_NAME'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->user->name}} ({{$buildings->user->email}})
                            </div>
                        
                            <div class="col-md-2">
                                {!! Form::label(trans('admin.BUILDING'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->name}}
                            </div>
                        </div>
                        <!-- /.form-group -->

                        <div class="row form-group">
                            <div class="col-md-2">
                                {!! Form::label(trans('admin.COUNTRY'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->country->name}}
                            </div>

                            <div class="col-md-2">
                                {!! Form::label(trans('admin.STATE'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->state->name}}
                            </div>
                        </div> 

                        <div class="row form-group">
                            <div class="col-md-2">
                                {!! Form::label(trans('admin.CITY'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->city->name}}
                            </div>

                            <div class="col-md-2">
                                {!! Form::label(trans('admin.ZIP_CODE'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                {{$buildings->zip_code}}
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-2">
                                {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            </div>
                            <div class="col-md-4">
                                <?php $status_list = Config::get('global.status_list'); ?>
                                {{$status_list[$buildings->status]}}
                            </div>
                        </div>

                    </div><!-- /.col -->

                </div><!-- /.row -->
                
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
{!! Html::script(asset('admin/js/plugins/ckeditor/ckeditor.js')) !!}

@stop

