@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Config Management</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

        {!!Form::model($setting, ['route' => ['admin.settings.update', $setting->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH',  'files' => true  , 'id' => 'edit-settings']) !!}    

            <!-- Block Tabs -->
            <div class="block full">
                <!-- Block Tabs Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#block-tabs-home">Global Setting</a></li>
                        <li><a href="#block-tabs-profile">Contact Setting</a></li>
                    </ul>
                </div>
                <!-- END Block Tabs Title -->

                <!-- Tabs Content -->
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="block-tabs-home">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.SITE_TITLE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('site_title',null,['class'=>'form-control','placeholder'=>trans('admin.SITE_TITLE')]) !!}

                                        <div class="error">{{ $errors->first('site_title') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.ADMIN_PAGE_LIMIT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.ADMIN_PAGE_LIMIT')]) !!}

                                        <div class="error">{{ $errors->first('page_limit') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->

                        <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-6 form-group ">
                                        {!! Form::label(trans('admin.ADMIN_EMAIL'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('admin_email',null,['class'=>'form-control','placeholder'=>trans('admin.ADMIN_EMAIL')]) !!}

                                            <div class="error">{{ $errors->first('admin_email') }}</div>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-6 ">
                                        
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.col -->

                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.FRONT_PAGE_LIMIT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('front_page_limit',null,['class'=>'form-control','placeholder'=>trans('admin.FRONT_PAGE_LIMIT')]) !!}

                                        <div class="error">{{ $errors->first('front_page_limit') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.STAFF_MAIL'),null,['class'=>'col-sm-4 control-label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('staff_mail',null,['class'=>'form-control','placeholder'=>trans('admin.STAFF_MAIL')]) !!}

                                        <div class="error">{{ $errors->first('staff_mail') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                            </div>
                        </div><!-- /.col -->

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.FROM_NAME'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('from_name',null,['class'=>'form-control','placeholder'=>trans('admin.FROM_NAME')]) !!}

                                        <div class="error">{{ $errors->first('from_name') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.REPLY_TO_EMAIL'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('reply_to_email',null,['class'=>'form-control','placeholder'=>trans('admin.REPLY_TO_EMAIL')]) !!}

                                        <div class="error">{{ $errors->first('reply_to_email') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->
                            
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.FROMEMAIL'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                    {!! Form::text('fromemail',null,['class'=>'form-control','placeholder'=>trans('admin.FROMEMAIL')]) !!}

                                    <div class="error">{{ $errors->first('fromemail') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.COPYRIGHT'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('copyright',null,['class'=>'form-control','placeholder'=>trans('admin.COPYRIGHT')]) !!}

                                        <div class="error">{{ $errors->first('copyright') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->

                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.META_TITLE'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('meta_title',null,['class'=>'form-control','placeholder'=>trans('admin.META_TITLE')]) !!}

                                        <div class="error">{{ $errors->first('meta_title') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.META_KEYWORDS'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('meta_keywords',null,['class'=>'form-control','placeholder'=>trans('admin.META_KEYWORDS')]) !!}

                                        <div class="error">{{ $errors->first('meta_keywords') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->
                            
                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.META_DESCRIPTION'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::textarea('meta_description',null,['class'=>'form-control','placeholder'=>trans('admin.META_DESCRIPTION')]) !!}

                                        <div class="error">{{ $errors->first('meta_description') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.EMAIL_SIGNATURE'),null,['class'=>'col-sm-4 control-label required_label    ']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::textarea('email_signature',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL_SIGNATURE')]) !!}

                                        <div class="error">{{ $errors->first('email_signature') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                            </div>
                        </div><!-- /.col -->
                            
                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.FOOTER_CONTACT_INFO'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>

                                        {!! Form::textarea('address',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS')]) !!}

                                        <div class="error">{{ $errors->first('address') }}</div>

                                        {!! Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('admin.PHONE_NUMBER')]) !!}

                                        <div class="error">{{ $errors->first('mobile') }}</div>

                                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL')]) !!}

                                        <div class="error">{{ $errors->first('email') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="col-md-6 form-group ">
                                    {!! Form::label(trans('admin.SOCIAL_ICON'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class="col-md-12 form-group ">
                                        {!! Form::label(trans('admin.FACEBOOK_ICON'),null,['class'=>'col-sm-4 control-label required_label']) !!}    
                                        <div class='col-sm-8'>
                                            {!! Form::text('facebook_icon',null,['class'=>'form-control','placeholder'=>trans('admin.FACEBOOK_ICON')]) !!}
                                            <div class="error">{{ $errors->first('facebook_icon') }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group ">
                                        {!! Form::label(trans('admin.TWITTER_ICON'),null,['class'=>'col-sm-4 control-label required_label']) !!} 
                                        <div class='col-sm-8'>
                                            {!! Form::text('twitter_icon',null,['class'=>'form-control','placeholder'=>trans('admin.TWITTER_ICON')]) !!}
                                            <div class="error">{{ $errors->first('twitter_icon') }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group ">
                                        {!! Form::label(trans('admin.GOOGLE_PLUS_ICON'),null,['class'=>'col-sm-4 control-label required_label']) !!} 
                                        <div class='col-sm-8'>
                                            {!! Form::text('google_plus_icon',null,['class'=>'form-control','placeholder'=>trans('admin.GOOGLE_PLUS_ICON')]) !!}
                                            <div class="error">{{ $errors->first('google_plus_icon') }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group ">
                                        {!! Form::label(trans('admin.LINKEDIN_ICON'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                        <div class='col-sm-8'>
                                            {!! Form::text('linkedin_icon',null,['class'=>'form-control','placeholder'=>trans('admin.LINKEDIN_ICON')]) !!}

                                            <div class="error">{{ $errors->first('linkedin_icon') }}</div>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                            </div>
                        </div><!-- /.col -->

                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.TAX'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('tax',null,['class'=>'form-control','placeholder'=>trans('admin.TAX')]) !!}

                                        <div class="error">{{ $errors->first('tax') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-6 ">
                                    {!! Form::label(trans('admin.CUTOFF_TIME'),null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('cutoff_time',null,['class'=>'form-control','placeholder'=>trans('admin.CUTOFF_TIME')]) !!}

                                        <div class="error">{{ $errors->first('cutoff_time') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->

                    </div>
                    <div class="tab-pane" id="block-tabs-profile">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label('Title-1',null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('contact_title_1',null,['class'=>'form-control','placeholder'=>'title-1']) !!}

                                        <div class="error">{{ $errors->first('contact_title_1') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="col-md-6 form-group ">
                                    {!! Form::label('Title-2',null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('contact_title_2',null,['class'=>'form-control','placeholder'=>'title-2']) !!}

                                        <div class="error">{{ $errors->first('contact_title_2') }}</div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.col -->

                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label('Support Title',null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('contact_support_title',null,['class'=>'form-control','placeholder'=>'support title']) !!}

                                        <div class="error">{{ $errors->first('contact_support_title') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="col-md-6 form-group ">
                                    {!! Form::label('Support Phone',null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('contact_support_phone',null,['class'=>'form-control','placeholder'=>'support phone']) !!}

                                        <div class="error">{{ $errors->first('contact_support_phone') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                            </div>
                        </div><!-- /.col -->


                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6 form-group ">
                                    {!! Form::label('Support Email',null,['class'=>'col-sm-4 control-label required_label']) !!}
                                    <div class='col-sm-8'>
                                        {!! Form::text('contact_support_email',null,['class'=>'form-control','placeholder'=>'support email']) !!}

                                        <div class="error">{{ $errors->first('contact_support_email') }}</div>
                                    </div>
                                </div><!-- /.form-group -->

                            </div>
                        </div><!-- /.col -->

                    </div>
                </div>
                <!-- END Tabs Content -->
            </div>
            <!-- END Block Tabs -->
            <!-- nav-tabs-custom -->
            <div class="box-footer">
            <div class="row">
                <div class="col-md-7 col-md-offset-5">
                    
                
                    {!! Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info'])!!}

                    {!! Html::link(route('admin.settings.index'), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-info']) !!}
                </div>
            </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->

@stop

