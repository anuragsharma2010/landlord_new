@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block full">
               
                <div class="row">

                    <div class="col-md-12">

                        <div class="row form-group">

                        <center><h2>{{trans('admin.UNAUTHORIZED_ACCESS')}}</h2>
                        <h4>{{trans('admin.UNAUTHORIZED_ACCESS_DONT_PERMISSION')}}</h4></center>
                            
                        </div>
                        <!-- /.form-group -->

                    </div><!-- /.col -->

                </div><!-- /.row -->
                
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
{!! Html::script(asset('admin/js/plugins/ckeditor/ckeditor.js')) !!}

@stop

