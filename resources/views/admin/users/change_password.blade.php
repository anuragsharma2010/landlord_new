@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

          @if(session('error'))
              <div class="alert alert-danger">
                  {{ session('error') }}
              </div>
          @endif
              @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif



          {!! Form::open(array('method'=>'post','route'=>['admin.UpdateChangePassword'])) !!}
            <div class="block full">
               
                <div class="row">

                  <div class="col-md-6">

                    <div class="form-group">
                       {!! Form::label(trans('admin.OLD_PASSWORD'),null, array('class' => 'required_label')) !!}
                       {!! Form::password('old_password',['class'=>'form-control','placeholder'=>trans('admin.OLD_PASSWORD')]) !!}
                        <div class="error">{{ $errors->first('old_password') }}</div>
                   
                    </div><!-- /.form-group --> 
                    <div class="form-group">
                       {!! Form::label(trans('admin.NEW_PASSWORD'),null,array('class' => 'required_label')) !!}
                       {!! Form::password('new_password',['class'=>'form-control','placeholder'=>trans('admin.NEW_PASSWORD')]) !!}
                        <div class="error">{{ $errors->first('new_password') }}</div>
                   
                    </div><!-- /.form-group -->

                     <div class="form-group">
                       {!! Form::label(trans('admin.CONFIRM_PASSWORD') ,null,array('class' => 'required_label')) !!}
                       {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>trans('admin.CONFIRM_PASSWORD')]) !!}
                       <div class="error">{{ $errors->first('confirm_password') }}</div>

                    </div><!-- /.form-group -->


                  </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->

@stop

