@extends('layouts.admin.blank')
@section('content')

<!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
                <i class="fa fa-history"></i> <strong>Password Reminder</strong>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuickInv">
            <p class="login-box-msg">You need to provide your registered email here.</p>
                <!-- Login Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <!--{!! Html::link(route('admin.forgot_password'), '<i class="fa fa-exclamation-circle"></i>', array('class' => 'btn btn-effect-ripple btn-primary', 'data-toggle'=>'tooltip', 'data-placement'=>'left', 'title'=>'Forgot your password?')) !!}-->

                        <a href="{{ route('admin.login') }}" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Back to login"><i class="fa fa-user"></i></a>
                        <!--<a href="page_ready_register.html" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Create new account"><i class="fa fa-plus"></i></a>-->
                    </div>
                    <h2>Forgot Password</h2>
                </div>
                <!-- END Login Title -->

                <div class="row">
                    <div class="col-md-12">
                        @if(session('error'))
                          <div class="alert alert-danger">
                              {{ session('error') }}
                          </div>
                        @endif
                          @if (session('success'))
                              <div class="alert alert-success">
                                  {{ session('success') }}
                              </div>
                          @endif
                    </div>
                </div>

                <!-- Login Form -->
                {!! Form::open(array('route' =>  'admin.forgot_password', 'id' => 'form-reminder', 'class'=>'form-horizontal')) !!}
                    <div class="form-group">
                        <div class="col-xs-12">
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email','id'=>'email']) !!}
                            <div class="error" class="server-side help-block animation-slideUp">{{ $errors->first('email') }}</div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-6">
                            {!! Html::link(route('admin.login'), 'Cancel', array('class' => 'btn btn-default   btn-flat pull-left')) !!}
                        </div>
                        <div class="col-xs-6 text-right">
                            {!! Form::submit('Remind Password',['class'=>'btn btn-effect-ripple btn-sm btn-primary'])!!}
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END Login Form -->
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="javascript:void(0);" target="_blank">{{config('settings.CONFIG_SITE_TITLE')}}</a></small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->

    <!-- jQuery 2.1.4 -->
    <!--{!! Html::script(asset('backend/bootstrap/js/bootstrap.min.js')) !!}
    {!! Html::script(asset('backend/js/jquery.noty.js')) !!}-->
<style type="text/css">
.error{
    font-size: 12px;
    color: red;
}
</style>
@stop
