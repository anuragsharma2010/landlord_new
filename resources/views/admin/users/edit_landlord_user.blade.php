@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($user,['method'=>'patch','route'=>['admin.update_landlord_user',$user->slug],'files'=>true]) !!}
            <div class="block full">
               
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!}
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                        {!! Form::label(trans('admin.PROFILE_IMAGE'),null) !!}
                        @if(!empty($user->profile_image))
                            <img height="100" width="100" src="{{ USER_IMAGE_URL . $user->profile_image }}" alt="User Profile Image" />
                        @else
                            <br>No Image
                        @endif      
                        
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.EMAIL'),null) !!}
                            {!! Form::text('email',null,['id'=>'email','class'=>'form-control','placeholder'=>trans('admin.EMAIL'), 'readonly'=>true]) !!}
                            <div class="error">{{ $errors->first('email') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
             
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PHONE'),null) !!}
                            {!! Form::text('contact',null,['id'=>'contact','minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE'), 'readonly'=>true]) !!}
                            <div class="error">{{ $errors->first('contact') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
                
                <input type="hidden" name="role_id" value="{{Config::get('global.role_id.landlord')}}">
                <input type="hidden" name="old_email" value="{{ $user->email }}">
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                            {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>

                
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
<script type="text/javascript">
    $(document).ready(function(){
        $("#email").prop('disabled', true);
        $("#contact").prop('disabled', true);
    });

</script>
@stop
