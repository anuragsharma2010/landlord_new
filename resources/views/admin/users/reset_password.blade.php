@extends('layouts.admin.blank')
@section('content')

<!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
                <i class="fa fa-history"></i> <strong>Password Reset</strong>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuickInv">
                <!-- Login Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="{{ route('admin.login') }}" class="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="Back to login"><i class="fa fa-user"></i></a>
                    </div>
                    <h2>Reset Password</h2>
                </div>
                <!-- END Login Title -->

                <div class="row">
                    <div class="col-md-12">
                        @if(session('error'))
                          <div class="alert alert-danger">
                              {{ session('error') }}
                          </div>
                        @endif
                          @if (session('success'))
                              <div class="alert alert-success">
                                  {{ session('success') }}
                              </div>
                          @endif
                    </div>
                </div>

                <div class="login-box-body"> 
       
                {!! Form::open(array('route' =>  ['admin.reset_password',$email_token])) !!}

                <div class="form-group has-feedback">
                  {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                  <div class="error">{{ $errors->first('password') }}</div>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                  {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                  <div class="error">{{ $errors->first('confirm_password') }}</div>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                
                <div class="row">
                  <div class="col-xs-4">
                      {!! Html::link(route('admin.login'), 'Cancel', array('class' => 'btn btn-default   btn-flat pull-left')) !!}
                  </div>
                  <div class="col-xs-8">
                  {!! Form::submit('Submit',['class'=>'btn btn-primary      pull-right'])!!}
                  </div>
                </div>
              {!! Form::close() !!}
            </div>

              </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/RcsdAh" target="_blank">{{config('settings.CONFIG_SITE_TITLE')}}</a></small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->
<style type="text/css">
.error{
    font-size: 12px;
    color: red;
}
</style>
@stop
