@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route'=>'admin.users.store','files'=>true]) !!}
            <div class="block full">
               
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!}
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                        {!! Form::label('Gender',null,['class'=>'required_label']) !!}
                                
                        <div class="form-radio"> 
                        <?php $gender = Config::get('global.gender'); ?>
                        <label> {!! Form::radio('gender','male',1, array('class' => 'minimal category_type '))!!} &nbsp; Male</label>
                        <label> {!! Form::radio('gender',  'female',0, array('class' => 'minimal category_type' )) !!}  &nbsp;  Female</label>
                        </div>
                        <div class="error">{{ $errors->first('gender') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.EMAIL'),null,['class'=>'required_label']) !!}
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL')]) !!}
                            <div class="error">{{ $errors->first('email') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
             
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PHONE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('contact',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE')]) !!}
                            <div class="error">{{ $errors->first('contact') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('admin.PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('password') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.CONFIRM_PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>trans('admin.CONFIRM_PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('confirm_password') }}</div>          
                        </div><!-- /.form-group -->             
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.LOCATION'),null) !!}
                            {!! Form::text('location',null,['id'=>'searchInput','class'=>'form-control','placeholder'=>'Click On Map To Get Address']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" name="latitude" id="latitude">
                            <input type="hidden" name="longitude" id="longitude">
                                                
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="role_id" value="2">
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                            {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgIoKihA3jHH_ZftnjjZq3nF-ZTfDF9Us&libraries=places"></script>
 

<script type="text/javascript">
 /* script */
function initialize() {
    var latitude = 10.536421;
    var longitude = -61.311951;
    
    var latlng = new google.maps.LatLng(latitude,longitude);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latitude, lng: longitude},
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
        
        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);
       
    });
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {   
         // console.log(results[0]);     
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });

}

function bindDataToForm(address,lat,lng){
   document.getElementById('searchInput').value = address;
   document.getElementById('latitude').value = lat;
   document.getElementById('longitude').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize); 

</script>
@stop

