@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route'=>'admin.store_user','files'=>true]) !!}
            <div class="block full">
               
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!}
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.EMAIL'),null,['class'=>'required_label']) !!}
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL')]) !!}
                            <div class="error">{{ $errors->first('email') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.DOB'),null,['class'=>'required_label']) !!}
                            {!! Form::text('dob',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control dateofbirth','placeholder'=>trans('admin.DOB')]) !!}
                            <div class="error">{{ $errors->first('dob') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PHONE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('contact',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE')]) !!}
                            <div class="error">{{ $errors->first('contact') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('admin.PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('password') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">  
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.CONFIRM_PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>trans('admin.CONFIRM_PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('confirm_password') }}</div>          
                        </div><!-- /.form-group -->             
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
                
                <input type="hidden" name="role_id" value="5">
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                            {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
<script type="text/javascript">
$(document).ready(function(){
    $('.dateofbirth').datepicker({
        format: 'mm/dd/yyyy'
    });
});
</script>

@stop

