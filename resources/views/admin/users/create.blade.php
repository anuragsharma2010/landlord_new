@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <?php //pr($errors); die; ?>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route'=>'admin.users.store','files'=>true]) !!}
            <div class="block full">
               
                <div class="row">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.SELECT_ROLES'),null,['class'=>'required_label']) !!}
                            {!! Form::select('role_id', $roles, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div>
                    </div> --><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!}
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                        {!! Form::label('Gender',null,['class'=>'required_label']) !!}
                                
                        <div class="form-radio"> 
                        <?php $gender = Config::get('global.gender'); ?>
                        <label> {!! Form::radio('gender','male',1, array('class' => 'minimal category_type '))!!} &nbsp; Male</label>
                        <label> {!! Form::radio('gender',  'female',0, array('class' => 'minimal category_type' )) !!}  &nbsp;  Female</label>
                        </div>
                        <div class="error">{{ $errors->first('gender') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.EMAIL'),null,['class'=>'required_label']) !!}
                            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>trans('admin.EMAIL')]) !!}
                            <div class="error">{{ $errors->first('email') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.OTHER_EMAIL'),null) !!}
                            {!! Form::text('other_email',null,['class'=>'form-control','placeholder'=>'Other Email']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
             
                    
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('admin.PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('password') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.CONFIRM_PASSWORD'),null,['class'=>'required_label']) !!}
                            {!! Form::password('confirm_password',['class'=>'form-control','placeholder'=>trans('admin.CONFIRM_PASSWORD')]) !!}
                            <div class="error">{{ $errors->first('confirm_password') }}</div>          
                        </div><!-- /.form-group -->             
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.PHONE'),null,['class'=>'required_label']) !!}
                            {!! Form::text('contact',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>trans('admin.PHONE')]) !!}
                            <div class="error">{{ $errors->first('contact') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                     <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.OTHER_CONTACT'),null) !!}
                            {!! Form::text('other_contact',null,['minlength'=>1,'maxlength'=>10, 'class'=>'form-control','placeholder'=>'Other Contact']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 
                </div>    
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.ADDRESS'),null) !!}
                            {!! Form::text('address1',null,['id'=>'searchInput','class'=>'form-control','placeholder'=>'Address']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.OTHER_ADDRESS'),null) !!}
                            {!! Form::text('address2',null,['id'=>'searchInput','class'=>'form-control','placeholder'=>'Address']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label(trans('admin.COUNTRY'),null) !!}
                            {!! Form::select('country_id', ['0'=>'--- Select Country ---'] + $countries, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATE'),null) !!}
                            {!! Form::select('state_id', $states, null, ['class' => 'form-control select2 autocomplete', 'placeholder'=>'--- Select State ---']) !!}
                        </div>
                    </div>

                     <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label(trans('admin.CITY'),null) !!}
                            {!! Form::select('city_id', ['0'=>'--- Select City ---'] + $cities, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div>
                    </div>


                    
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.BUSINESS_NAME'),null) !!}
                            {!! Form::text('business_name',null,['class'=>'form-control','placeholder'=>'Business Name']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.BANK_ACCOUNT'),null) !!}
                            {!! Form::text('bank_account',null,['class'=>'form-control','placeholder'=>'Bank Account']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 

                    
                </div><!-- /.row -->
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.ONLINE_ACCOUNT1'),null) !!}
                            {!! Form::text('online_account1',null,['class'=>'form-control','placeholder'=>'Online Account 1']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 
                
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.ONLINE_ACCOUNT2'),null) !!}
                            {!! Form::text('online_account2',null,['class'=>'form-control','placeholder'=>'Online Account 2']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col --> 
                </div><!-- /.row -->

                

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control select2 autocomplete']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            
                <input type="hidden" name="role_id" value="2">
                
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                            {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>

                
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
<script>
    $(function() {
        $('select[name=country_id]').change(function() {

            //alert($(this).val());

            var url = '{{ url("admin/country") }}'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name= state_id]');

                select.empty();
                $('form select[name= city_id]').empty();
                $('form select[name= city_id]').append('<option value="">Select city</option>');
                select.append('<option value="">Select state</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });
    });
</script>

<script>
    $(function() {
        $('select[name=state_id]').change(function() {

            //alert($(this).val());

            var url = '{{ url("admin/state") }}'+'/' + $(this).val() ;

            $.get(url, function(data) {
                var select = $('form select[name= city_id]');

                select.empty();
                select.append('<option value="">Select city</option>');
                $.each(data,function(key, value) {
                   /* console.log(key);
                    console.log(value); return false;*/
                    select.append('<option value=' + key + '>' + value + '</option>');
                });
            });
        });
    });
</script>

@stop

