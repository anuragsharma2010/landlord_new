@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
  <!-- Datatables Block -->
    <div class="block full">
        <div class="block-title">
            <h2>Admin Menus</h2>
            <div class="pull-right">
                {!!  Html::decode(Html::link(route('admin.adminmenus.create'),"<i class='fa  fa-plus'></i> ".trans('admin.ADD_NEW'),['class'=>'btn btn-success'])) !!} 

            </div>
        </div>

        <div class="table-responsive">
            <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                <thead>
                    <tr>
                        <th width="10%">@sortablelink('name', trans('admin.ICON'))</th>
                        <th width="10%">@sortablelink('name', trans('admin.NAME'))</th>
                        <th width="10%">@sortablelink('menu_order', trans('admin.ORDER'))</th>
                        <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                        <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                        <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!$admin_menu_list->isEmpty())
            @foreach ($admin_menu_list as $admin_menu)
            <tr>
                <td><i class="fa  {{ $admin_menu->icon}}"> </i></td>
                <td>{{ ucfirst($admin_menu->name) }}</td>
                <td>{{ ucfirst($admin_menu->menu_order) }}</td>
                <td>{{ date_val($admin_menu->created_at,MDY_FORMATE ) }}</td>
                <td>{{ date_val($admin_menu->updated_at,MDY_FORMATE ) }}</td>
                <td align="center">
                    @if($admin_menu->status == 1)
                    {!!  Html::decode(Html::link(route('admin.adminmenus.status_change',['id' => $admin_menu->id,'status'=>$admin_menu->status]),"<i class='fa  fa-check'></i>",['class'=>'confirm_link   btn btn-effect-ripple btn-sm btn-success','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                    @else
                    {!!  Html::decode(Html::link(route('admin.adminmenus.status_change',['id' => $admin_menu->id,'status'=>$admin_menu->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn-danger confirm_link  btn btn-effect-ripple btn-sm','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                    @endif

                    {!!  Html::decode(Html::link(route('admin.adminmenus.edit',['id'=>$admin_menu->id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-effect-ripple btn-sm btn-success','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                    {!!  Html::decode(Html::link(route('admin.adminmenus.child_menu',['id'=>$admin_menu->id]),"<i class='fa   fa-code-fork'></i>",['class'=>'btn-primary btn btn-effect-ripple btn-sm','data-toggle'=>'tooltip','title'=>trans('admin.CHILD_MENU')])) !!}

                </td>
                @endforeach
                @else

                <tr>
                    <td colspan="7"><div class="data_not_found"> Data Not Found </div></td>
                </tr>
                @endif
                </tbody>
            </table>
            {!! $admin_menu_list->appends(Input::all('page'))->render() !!}
        </div>
    </div>
    <!-- END Datatables Block -->
</div>
<!-- END Page Content -->
@stop