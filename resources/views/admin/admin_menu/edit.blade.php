@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Edit Menu</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2>&nbsp;</h2>
                </div>
                {!! Form::model($adminmenus,['method'=>'post','files'=>true,'route'=>['admin.adminmenus.update',$adminmenus->id,$parent_id], 'class'=>'form-horizontal form-bordered']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!} 
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div>
                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.MENU_ORDER'),null,['class'=>'required_label']) !!}
                            {!! Form::text('menu_order',null,['class'=>'form-control','placeholder'=>trans('admin.MENU_ORDER')]) !!}
                            <div class="error">{{ $errors->first('menu_order') }}</div>
                 
                        </div>
                    </div>

                    <div class="row">   
                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.ROUTE'),null,['class'=>'']) !!}
                            {!! Form::text('route',null,['class'=>'form-control','placeholder'=>trans('admin.ROUTE')]) !!}
                            <div class="error">{{ $errors->first('route') }}</div>
                        </div>  

                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.ICON'),null,['class'=>'required_label']) !!}
                        <?php 
                        $iconlist = array('' => trans('admin.PLEASE_SELECT')) +  getIcon();
                        ?>
                            
                            {!!   Html::decode(Form::select('icon', $iconlist, null, ['class' => 'select3 form-control'])); !!}
                            <div class="error">{{ $errors->first('icon') }}</div>
                        </div>
                    </div>

                    <div class="row">   
                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php 
                                $status_list = Config::get('global.status_list'); 
                            ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'select2 form-control']) !!}
                        </div>
                    </div>    

                    <div class="row">
                        <div class="col-md-12 form-group form-actions">
                            <div class="col-md-8 col-md-offset-3">
                                {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-effect-ripple btn-primary'])!!}
                                {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-effect-ripple btn-danger'])!!}
                            </div>
                        </div>
                    </div>                    
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->

<script type="text/javascript">
    
$(document).ready(function() {
    $('.select3').select2({
        templateResult: formateicon,
        templateSelection: formateicon,
    });
});

function formateicon(icon) {
    if(!icon.id) {
        return icon.text;
    }
    var $icon = $('<i class="fa  ' + icon.id + '"></i> ').text(" "+icon.text);
    return $icon;
}

</script>
@stop

