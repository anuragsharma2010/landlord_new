@extends('layouts.admin.default')
@section('content')  

    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1>User {{$pageTitle}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            @include('includes.admin.breadcrumb')
                        </div>
                    </div>
                </div>
                <h2>User Name :- {{ucfirst($username)}}</h2>
            </div>

            {!! Form::open(['method'=>'delete','method'=>'get','route'=>['admin.address.index',$slug]]) !!}
            
                <div class="search_table clearfix" style="margin-bottom: 30px; ">
                    <span style="float: left; width: 50%;">
                        {!! Form::text('title', $form_data['title'], ['class'=>'form-control', 'placeholder'=>'title', 'style'=>'float:left; width: 60%' ]) !!}
                        <button class="btn btn-primary" style="float: left" type="submit">Search</button>
                    </span>
                    
                    <h3 class="pull-right" style="margin: 0; float: right;">  
                        {!!  Html::decode(Html::link(route('admin.address.add',$slug),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}
                    </h3>
                </div>                         
                
            {!! Form::close() !!}

            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%">@sortablelink('address1', trans('admin.ADDRESS1'))</th>
                            <th width="15%">@sortablelink('address2', trans('admin.ADDRESS2'))</th>
                            <th width="15%">@sortablelink('landmark', trans('admin.LANDMARK'))</th>
                            <th width="12%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                            <th width="12%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                            <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($addresslist))
                            @foreach ($addresslist as $address)
                                <tr>
                                    <td>{{ ucfirst($address->address1) }}</td>
                                    <td>{{ ucfirst($address->address2) }}</td>
                                    <td>{{ ucfirst($address->landmark) }}</td>
                                    <td>{{ date_val($address->created_at,MDY_FORMATE ) }}</td>
                                    <td>{{ date_val($address->updated_at,MDY_FORMATE ) }}</td>
                                    <td align="center">
                                      
                                    {!!  Html::decode(Html::link(route('admin.address.edit',[$address->id, $address->user_id] ),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}
                                        
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                @if(!empty($addresslist))
                    {!! $addresslist->appends(Input::all('page'))->render() !!}
                @endif    
            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
@stop