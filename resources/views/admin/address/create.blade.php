@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb') 
                </div>
            </div>

               
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route'=>['admin.address.store', $slug],'files'=>true]) !!}
            <div class="block full">
                <div class="row">

                    <div class="col-md-12">

                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.ADDRESS1'),null,['class'=>'required_label']) !!}
                            {!! Form::text('address1',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS1')]) !!}
                            <div class="error">{{ $errors->first('address1') }}</div>
                        </div><!-- /.form-group -->

                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.ADDRESS2'),null,['class'=>'required_label']) !!}
                            {!! Form::text('address2',null,['class'=>'form-control','placeholder'=>trans('admin.ADDRESS2')]) !!}
                            <div class="error">{{ $errors->first('address2') }}</div>
                    
                        </div>
                    </div><!-- /.form-group -->
                    <div class="col-md-12">
                        <div class="col-md-6 form-group ">
                            {!! Form::label(trans('admin.LANDMARK'),null,['class'=>'required_label']) !!}
                            {!! Form::text('landmark',null,['class'=>'form-control','placeholder'=>trans('admin.LANDMARK')]) !!}
                            <div class="error">{{ $errors->first('landmark') }}</div>
                        </div><!-- /.form-group -->

                        <div class="col-md-6">
                            <div class="form-group">
                            {!! Form::label('Type',null,['class'=>'required_label']) !!}
                                    
                            <div class="form-radio"> 
                            <label> {!! Form::radio('type',1,1, array('class' => 'minimal category_type '))!!} &nbsp; Home</label>
                            <label> {!! Form::radio('type',2,0, array('class' => 'minimal category_type' )) !!}  &nbsp;  Work</label>
                            <label> {!! Form::radio('type',3,0, array('class' => 'minimal category_type' )) !!}  &nbsp;  Other</label>
                            </div>
                            <div class="error">{{ $errors->first('type') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.col -->
                        
                         
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    {!! Form::hidden('user_id',$slug,['class'=>'form-control']) !!}
                </div><!-- /.row -->
                
                
                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
               
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
{!! Html::script(asset('admin/js/plugins/ckeditor/ckeditor.js')) !!}
@stop

