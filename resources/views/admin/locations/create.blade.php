@extends('layouts.admin.default')

@section('content')  

<!-- Page content -->
<div id="page-content">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>{{$pageTitle}}</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                @include('includes.admin.breadcrumb')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route'=>'admin.locations.store']) !!}
            <div class="block full">

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.NAME'),null,['class'=>'required_label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>trans('admin.NAME')]) !!}
                            <div class="error">{{ $errors->first('name') }}</div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php $status_list = Config::get('global.status_list'); ?>
                            {!! Form::select('status', $status_list, null, ['class' => 'form-control']) !!}
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12 form-group form-actions">
                        <div class="col-md-8 col-md-offset-5">
                            {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                            {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!}
                        </div>
                    </div>
                </div>
               
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END Page Content -->
@stop

