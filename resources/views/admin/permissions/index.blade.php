@extends('layouts.admin.default')
@section('content')  
<style type="text/css">
    .nav-tabs-custom {
        margin-bottom: 20px;
        background: #fff;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        border-radius: 3px;
     }

    .nav-tabs-custom>.nav-tabs {
        margin: 0;
        border-bottom-color: #f4f4f4;
        border-top-right-radius: 3px;
        border-top-left-radius: 3px;
     }

    .nav-tabs-custom>.nav-tabs>li:first-of-type {
        margin-left: 0;
    }

    .nav-tabs-custom>.nav-tabs>li {
        border-top: 3px solid transparent;
        margin-bottom: -2px;
        margin-right: 5px;
    }

    ul.permissions_ul li {
        padding: 10px 15px;
        width: 180px;
       
    }
</style>
    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1>{{$pageTitle}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            @include('includes.admin.breadcrumb')
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                {!!Form::model($permissions, ['route' => ['admin.permissions.update', 1], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-settings']) !!}
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs permissions_ul">
                        <li>&nbsp;</li>
                        @foreach($roles as $role_key=>$role_value)
                            @if($role_key!=1)
                            <li>{{$role_value}}</li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        <!-- Start Global Settings -->
                        <div class="tab-pane active" id="global_settings">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $count = 1;?>
                                    @foreach($menus as $menu)
                                    <ul class="nav nav-tabs permissions_ul">
                                    <li>{{$menu->name}}</li>
                                    @foreach($roles as $role_key=>$role_value)
                                    @if($role_key!=1)
                                    <li>
                                    <?php   $can_index = BasicFunction::isAllowed($role_key,$menu->id,'can_index');
                                            $can_add = BasicFunction::isAllowed($role_key,$menu->id,'can_add');
                                            $can_edit = BasicFunction::isAllowed($role_key,$menu->id,'can_edit');
                                            $can_delete = BasicFunction::isAllowed($role_key,$menu->id,'can_delete');
                                            if($can_index==1 && $can_add==1 && $can_edit==1 && $can_delete==1){
                                                $all_check= 1;
                                            }else{
                                                $all_check= 0;
                                            }
                                    ?>
                                        {!! Form::checkbox('select_all'.$count,1,$all_check,array('class'=>'all_checkbox')) !!} All<br>
                                        {!! Form::checkbox('can_index'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_index')) !!} List<br>
                                        {!! Form::checkbox('can_add'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_add')) !!} Add<br>
                                        {!! Form::checkbox('can_edit'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_edit')) !!} Edit<br>
                                        {!! Form::checkbox('can_delete'.$count,1,BasicFunction::isAllowed($role_key,$menu->id,'can_delete')) !!} Delete<br>
                                        {!! Form::hidden('role_id'.$count,$role_key) !!}
                                        {!! Form::hidden('menu_id'.$count,$menu->id) !!}
                                    </li>
                                    <?php $count++;?>
                                    @endif
                                    @endforeach
                                    </ul>       
                                    @endforeach
                                    
                                </div>
                            </div><!-- /.col -->


                     </div>
                    <!-- END Global Settings -->

                    {!! Form::hidden('count',$count-1) !!}
                    {!! Html::link(route('admin.permissions.index'), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-left']) !!}
                    

                    {!! Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info pull-right'])!!}
                    </div>
                <!-- /.tab-content -->
                
            </div>
            <!-- nav-tabs-custom -->
         

            {!! Form::close() !!}

        </div><!-- /.col -->

           
        </div>
        <!-- END Datatables Block -->
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.all_checkbox').on('click',function(){
            if($(this).is(":checked")){
                $(this).parent().find('input[type=checkbox]').prop('checked',true);
            }else{
                $(this).parent().find('input[type=checkbox]').prop('checked',false);
            }
        });
    });
    </script>
@stop