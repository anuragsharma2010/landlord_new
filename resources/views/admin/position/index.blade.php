@extends('layouts.admin.default')
@section('content')  

    <!-- Page content -->
    <div id="page-content">
        <!-- Datatables Block -->
        <div class="block full">

            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="header-section">
                            <h1>{{$pageTitle}}</h1>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            @include('includes.admin.breadcrumb')
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::open(['method'=>'delete','method'=>'get','route'=>['admin.position.index']]) !!}

                <div class="search_table clearfix" style="margin-bottom: 30px; ">
                    <span style="float: left; width: 50%;">
                        {!! Form::text('title', $form_data['title'], ['class'=>'form-control', 'placeholder'=>'title', 'style'=>'float:left; width: 60%' ]) !!}
                        <button class="btn btn-primary" style="float: left" type="submit">Search</button>
                    </span>
                    
                    <h3 class="pull-right" style="margin: 0; float: right;">  
                        {!!  Html::decode(Html::link(route('admin.position.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}
                    </h3>
                </div>                         
                
            {!! Form::close() !!}

            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-vcenter table-condensed table-hover">
                    <thead>
                        <tr>
                            <th width="15%">@sortablelink('title', trans('admin.TITLE'))</th>
                            <th width="12%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                            <th width="12%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                            <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$positionlist->isEmpty())
                            @foreach ($positionlist as $position)
                                <tr>
                                    <td>{{ ucfirst($position->name) }}</td>
                                    <td>{{ date_val($position->created_at,MDY_FORMATE ) }}</td>
                                    <td>{{ date_val($position->updated_at,MDY_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($position->status == 1)
                                            {!!  Html::decode(Html::link(route('admin.position.status_change',['slug' => $position->slug,'status'=>$position->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @else
                                            {!!  Html::decode(Html::link(route('admin.position.status_change',['slug' => $position->slug,'status'=>$position->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @endif

                                        {!!  Html::decode(Html::link(route('admin.position.edit', $position->slug),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                        {!!  Html::decode(Html::link(route('admin.position.delete', $position->slug),"<i class='fa  fa-trash'></i>",['class'=>'btn btn-danger','data-toggle'=>'tooltip','title'=>trans('admin.DELETE')])) !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <div class="data_not_found"> Data Not Found </div>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {!! $positionlist->appends(Input::all('page'))->render() !!}
            </div>
        </div>
        <!-- END Datatables Block -->
    </div>
@stop