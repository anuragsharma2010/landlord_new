<!-------------Footer-------------->

<footer class="foterBg pt-4 d-flex">
    <div class="container">
        <div class="row gradLine py-lg-5 py-3">
            <div class="col-md-4">
                <div class="social">
                    <p class="mb-0 colorWhite font16">Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry.</p>
                    <div class="font2 colorWhite font22 fontSemiBold footerTitle pb-2 mt-4">Follow Us On</div>
                    <ul class="d-flex align-items-center mt-3">
                        <li class="mr-2"><a href="javascript:;"><i class="allinone fb"></i></a></li>
                        <li class="mr-2"><a href="javascript:;"><i class="allinone twit"></i></a></li>
                        <li class="mr-2"><a href="javascript:;"><i class="allinone linke"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="font2 colorWhite font22 fontSemiBold footerTitle pb-2">Contact Info</div>
                <ul class="mt-3 contactBlock">
                    <li class="font18 colorWhite d-flex align-items-center mb-3"><i class="allinone map mr-2"></i>33
                        Nassau Ave Brooklyn, NY 11211</li>
                    <li class="font18 colorWhite d-flex align-items-center mb-3"><i
                            class="allinone phone mr-2"></i>516-707-3773</li>
                    <li class="font18 colorWhite d-flex align-items-center mb-3"><i
                            class="allinone mail mr-2"></i>info@mymatrixrent.com</li>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="font2 colorWhite font22 fontSemiBold footerTitle pb-2">Useful Links</div>
                <ul class="mt-3 cmsPages">
                    <li><a href="index.html">For Renter</a></li>
                    <li><a href="{{ url('pages/contact-us') }}">Contact Us</a></li>
                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                    <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ url('for-management') }}">For Management</a></li>
                    <li><a href="term-condition.html">Terms & Conditions</a></li>
                </ul>
            </div>
        </div>

        <div class="copyRight d-flex justify-content-center my-3">
            <p class="font16 colorWhite text-center">Copyright © matrix, 2019-2019</p>
        </div>
    </div>
</footer>

<!-------------Footer-end-------------->




<!----------Login-Modal------------>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-image: url('images/modal-bg.png')">
        <div class="close-modal-btn pt-2">
          <button type="button" class="close m-0" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>   
        </div>
        <div class="modal-body costums">
            <ul class="nav nav-tabs justify-content-around mb-5" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="font22 txt-clr20 font700 active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sign In</a>
                  </li>
                  <li class="nav-item">
                    <a class="font22 txt-clr20 font700" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sign Up</a>
                  </li>
            </ul>
            <div class="tab-content mt-4" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="login-details">
                       
                        <form action="{{ route('login') }}" class="contact-details" id="formLogin" role="form" method="POST">
                            <div class="form-row">

                            <div class="form-group col-md-12">
                                <div class="alert alert-success alert-dismissible hidden">
                                    You are login successfully.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            @csrf
                            {!! Form::hidden('role_id',5,['class'=>'form-control','placeholder'=>'role_id']) !!}
                                <div class="form-group col-md-12">
                                    {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Email Address']) !!}
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group col-md-12 mb-5 text-right">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="check-content d-flex">
                                                <div class="in-contain align-self-center mr-3">
                                                    <input type="checkbox" id="remember" name="remember" class="d-none">
                                                    <label for="frgt" class="mlr"></label>
                                                    <span class="check"></span>
                                                </div>
                                                <div class="text">
                                                    <span class="font18 font1 d-inline-block">Remember me</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="font18 font1" onclick="OpenModal('#change-pass')" style="cursor: pointer">Forgot password ?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                     <button type="submit" class="btn-one font1 text-uppercase" style="border:none;">Sign In</button>
                                </div>
                                <!-- <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="br-lr text-center mt-4">
                                                <div class="br-title"><h5>Sign In With</h5></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 py-4">
                                            <a href=""><img src="{{asset('public/front/images/lg-facebook.png')}}" alt=""></a>
                                        </div>
                                        <div class="col-md-6 py-4">
                                            <a href=""><img src="{{asset('public/front/images/lg-google.png')}}" alt=""></a>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group col-md-12 text-center">
                                    <!-- <p class="font18 font1">Don’t have an account?<a class="abc"   onclick="OpenModal('#signup')" >Sign up here</a></p> -->
                                </div>
                            </div>
                        </form>
                    </div>
                    
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="login-details">
                        <form action="{{ route('register') }}" method="POST" class="contact-details" id="formRegister">
                        @csrf
                        {!! Form::hidden('role_id',5,['class'=>'form-control','placeholder'=>'role_id']) !!}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::text('first_name',null,['class'=>'form-control','placeholder'=>'First Name']) !!}
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::text('last_name',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Email Address']) !!}
                                    <small class="help-block"></small>
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class='input-group date' id='datetimepicker1'>
                                                {!! Form::text('dob',null,['class'=>'form-control chnged dateofbirth', 'id'=>'date', 'placeholder'=>'Date of Birth']) !!}
                                                <label for="date" class="dtp"></label>
                                            </div>
                                            <small class="help-block"></small>
                                            <!-- <label for="date" class="dtp"></label> -->
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::text('contact',null,['class'=>'form-control','placeholder'=>'Enter Phone Number']) !!}
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="form-group col-md-12 text-center mt-4">
                                     <button class="btn-one font1 text-uppercase" type="submit" style="border:none;">Sign Up</button>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <p class="font18 font1">Already have an account? <a class="abc"   onclick="OpenModal('#login', '#home-tab')" >Sign in here</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
  </div>
</div>

<!----------------Login-Modal-end----------------->


<!--------signup-modal------->

<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="signup" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-image: url('images/modal-bg.png')">
      <div class="modal-header justify-content-between flex-wrap">
        <h5 class="modal-title font35 txt-clr20 font700" id="exampleModalLabel">Sign In Here</h5>
        <button type="button" class="close m-0" data-dismiss="modal" aria-label="Close" id="newm">
          <span aria-hidden="true">&times;</span>
        </button>
        
      </div>

        <div class="modal-body costums">
                <!-- <ul class="nav nav-tabs justify-content-around" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="font22 txt-clr20 font700 active" id="signup-tab" data-toggle="tab" href="#signupone" role="tab" aria-controls="signupone" aria-selected="true">Sign In</a>
                      </li>
                      <li class="nav-item">
                        <a class="font22 txt-clr20 font700" id="signtwo-tab" data-toggle="tab" href="#signuptwo" role="tab" aria-controls="signuptwo" aria-selected="false">Sign Up</a>
                      </li>
                </ul> -->
                <div class="tab-content mt-4" id="myTabContent">
                    <div class="tab-pane fade show active" id="signupone" role="tabpanel" aria-labelledby="signupone-tab">
                        <div class="login-details">
                            <form action="" class="contact-details">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="email" required placeholder="Email Address" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="password" required placeholder="Password" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12 mb-5 text-right">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="check-content d-flex">
                                                    <div class="in-contain align-self-center mr-3">
                                                        <input type="checkbox" id="frgt1" class="d-none">
                                                        <label for="frgt1" class="mlr"></label>
                                                        <span class="check"></span>
                                                    </div>
                                                    <div class="text">
                                                        <span class="font18 font1 d-inline-block">Remember me</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="" class="font18 font1 abc" onclick="OpenModal('#change-again')">Forgot password ?</a>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 text-center">
                                         <button class="btn-one font1 text-uppercase" style="border:none;">Sign In</button>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="br-lr text-center mt-4">
                                                    <div class="br-title"><h5>Sign In With</h5></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 py-4">
                                                <a href=""><img src="{{asset('public/front/images/lg-facebook.png')}}" alt=""></a>
                                            </div>
                                            <div class="col-md-6 py-4">
                                                <a href=""><img src="{{asset('public/front/images/lg-google.png')}}" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 text-center">
                                        <!-- <p class="font18 font1">Don’t have an account?<a class="abc"   onclick="OpenModal('#signup')" >Sign up here</a></p> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="login-details">
                            <form action="">
                                <div class="email-add">
                                    <label for="">First Name</label>
                                    <input type="text" placeholder="John">
                                </div>
                                <div class="email-add">
                                    <label for="">Last Name</label>
                                    <input type="text" placeholder="Enter last name">
                                </div>
                                <div class="email-add">
                                    <label for="">Email</label>
                                    <input type="email" placeholder="Email address">
                                </div>
                                <div class="email-add">
                                    <label for="">Password</label>
                                    <input type="password" placeholder="Password">
                                </div>
                                <div class="email-add">
                                    <label for="">Confrim Password</label>
                                    <input type="password" placeholder="Password">
                                </div>
                                <!-- <div class="forget">
                                    <a href="">forgot password ?</a>
                                </div> -->  
                                <div class="search my-4">
                                    <button class="comn-btn w-100">Sign Up</button>
                                </div>
                                <div class="dont">
                                    <p>Already have an account?<a class="abc" onclick="OpenModal('#login')">Login</a></p>
                                </div>
                            </form>
                        </div>
                      </div>
                </div>
        </div>
    </div>
  </div>
</div>

<!--------signup-modal-end------>


<!---------Forget-password -------->

<div class="modal fade" id="change-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Forget Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="col-md-12">
                <div class="ac-form">
                    <div class="keep-in-touch">
                        <form action="{{ url('password/sendemail') }}" class="contact-details" method="POST" id="changePassForm">
                            @csrf
                            {!! Form::hidden('role_id',5,['class'=>'form-control','placeholder'=>'role_id']) !!}
                            <div class="form-row">
                                <div class="form-group col-md-12 px-0">
                                    {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Email Address']) !!}
                                    <small class="help-block"></small>
                                </div>   
                            </div>
                            <div class="pr-btns pt-sm-2 d-flex justify-content-center">
                                <button class="btn-one mx-0 font1" type="submit">
                                    SAVE
                                </button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="change-again" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h3>Reset Password</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="col-md-12">
                <div class="ac-form">
                    <div class="keep-in-touch">
                        <form action="{{ url('password/reset_password') }}" class="contact-details" method="POST" id="resetPassForm">
                            <div class="form-row">
                            <?php if(isset($email_token)){ $email_token = $email_token; } else {$email_token='';} ?>
                            @csrf
                            {!! Form::hidden('role_id',5,['class'=>'form-control','placeholder'=>'role_id']) !!}
                            {!! Form::hidden('email_token',$email_token,['class'=>'form-control','placeholder'=>'']) !!}
                                <div class="form-group col-md-12 px-0">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                    <small class="help-block"></small>
                                </div>
                                 <div class="form-group col-md-12 px-0">
                                    {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                                    <small class="help-block"></small>
                                </div> 
                            </div>
                            <div class="pr-btns pt-sm-2 d-flex justify-content-center">
                                <button class="btn-one mx-0 font1" type="submit">
                                    SAVE
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="change-profile-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h3>Change Password</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="col-md-12">
                <div class="ac-form">
                    <div class="keep-in-touch">
                        <form action="{{ url('password/change_password') }}" class="contact-details" method="POST" id="changeprofilePassForm">
                            <div class="form-row">
                            @csrf
                                <div class="form-group col-md-12 px-0">
                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                    <small class="help-block"></small>
                                </div>
                                 <div class="form-group col-md-12 px-0">
                                    {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                                    <small class="help-block"></small>
                                </div> 
                            </div>
                            <div class="pr-btns pt-sm-2 d-flex justify-content-center">
                                <button class="btn-one mx-0 font1" type="submit">
                                    SAVE
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>

    <!-- <script src="{{asset('public/front/js/jquery.min.js')}}"></script> -->
    <script src="{{asset('public/front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/front/js/owl.carousel.min.js')}}"></script>
    {!! Html::script(asset('public/front/js/jquery.noty.js')) !!}
    {!! Html::script(asset('public/front/js/global.js')) !!}

    <script type="text/javascript">
    $(function(){

        $( ".dateofbirth" ).focus(function() {  
          $( this ).attr('type', 'date');  
        });

        var change_pass_var = '<?php if(isset($change_pass_var)){ echo $change_pass_var; } ?>';
        if(change_pass_var!=''){
            $('#'+change_pass_var).modal('show');
        }

        /*$('.dateofbirth').datepicker({
            format: 'mm/dd/yyyy'
        });*/

        $(document).on('submit', '#formLogin', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data.status);
                if(data.status==1){
                    //$('#formLogin .alert-success').removeClass('hidden');
                    noty({
                            layout : 'bottomRight', 
                            theme : 'noty_theme_default', 
                            type : 'success',     
                            text: data.sucess,     
                            timeout : 100000,
                            closeButton:true,
                            animation : {
                                easing: 'swing',
                                speed: 150 
                            }       
                        });

                    setTimeout(function () {
                        $('#login').modal('hide');
                        window.location.href = "account";
                    }, 1000);
                }else{
                    if(data.errors){
                        $.each(data.errors, function (key, value) {
                            var input = '#formLogin input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    }
                }
            })
            .fail(function(data) {
                if(data.errors!=''){
                    $.each(data.errors, function (key, value) {
                        //alert(key);
                        var input = '#formLogin input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                }
            });
        });

        $(document).on('submit', '#formRegister', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data.status);
                if(data.status==1){
                    //$('#formRegister .alert-success').removeClass('hidden');

                    noty({
                            layout : 'bottomRight', 
                            theme : 'noty_theme_default', 
                            type : 'success',     
                            text: data.sucess,     
                            timeout : 100000,
                            closeButton:true,
                            animation : {
                                easing: 'swing',
                                speed: 150 
                            }       
                        });

                    setTimeout(function () {
                        $('#login').modal('hide');
                        location.reload();
                    }, 1000);
                }else{
                    if(data.errors){
                        $.each(data.errors, function (key, value) {
                            var input = '#formRegister input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    }
                }
            })
            .fail(function(data) {
                if(data.errors!=''){
                    $.each(data.errors, function (key, value) {
                        //alert(key);
                        var input = '#formRegister input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                }
            });
        });


        $(document).on('submit', '#changePassForm', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data.status);
                if(data.status==1){
                    //$('#formRegister .alert-success').removeClass('hidden');

                    noty({
                            layout : 'bottomRight', 
                            theme : 'noty_theme_default', 
                            type : 'success',     
                            text: data.sucess,     
                            timeout : 100000,
                            closeButton:true,
                            animation : {
                                easing: 'swing',
                                speed: 150 
                            }       
                        });

                    setTimeout(function () {
                        $('#change-pass').modal('hide');
                        location.reload();
                    }, 1000);
                }else{
                    if(data.errors){
                        $.each(data.errors, function (key, value) {
                            var input = '#changePassForm input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    }
                }
            })
            .fail(function(data) {
                if(data.errors!=''){
                    $.each(data.errors, function (key, value) {
                        //alert(key);
                        var input = '#changePassForm input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                }
            });
        });

        $(document).on('submit', '#resetPassForm', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data.status);
                if(data.status==1){
                    //$('#formRegister .alert-success').removeClass('hidden');

                    noty({
                            layout : 'bottomRight', 
                            theme : 'noty_theme_default', 
                            type : 'success',     
                            text: data.sucess,     
                            timeout : 100000,
                            closeButton:true,
                            animation : {
                                easing: 'swing',
                                speed: 150 
                            }       
                        });

                    setTimeout(function () {
                        $('#change-again').modal('hide');
                        location.reload();
                    }, 1000);
                }else{
                    if(data.errors){
                        $.each(data.errors, function (key, value) {
                            var input = '#resetPassForm input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    }
                }
            })
            .fail(function(data) {
                if(data.errors!=''){
                    $.each(data.errors, function (key, value) {
                        //alert(key);
                        var input = '#resetPassForm input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                }
            });
        });

        $(document).on('submit', '#changeprofilePassForm', function(e) { 
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data.status);
                if(data.status==1){
                    //$('#formRegister .alert-success').removeClass('hidden');

                    noty({
                            layout : 'bottomRight', 
                            theme : 'noty_theme_default', 
                            type : 'success',     
                            text: data.sucess,     
                            timeout : 100000,
                            closeButton:true,
                            animation : {
                                easing: 'swing',
                                speed: 150 
                            }       
                        });

                    setTimeout(function () {
                        $('#change-profile-pass').modal('hide');
                        location.reload();
                    }, 1000);
                }else{
                    if(data.errors){
                        $.each(data.errors, function (key, value) {
                            var input = '#changeprofilePassForm input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    }
                }
            })
            .fail(function(data) {
                if(data.errors!=''){
                    $.each(data.errors, function (key, value) {
                        //alert(key);
                        var input = '#changeprofilePassForm input[name=' + key + ']';
                        $(input + '+small').text(value);
                        $(input).parent().addClass('has-error');
                    });
                }
            });
        });
 
    });
        // $(".menuToggle").click(function () {
        //     $(".navBar ul").addClass("showMenus");
        // });

        // $(".closeIcon").click(function () {
        //     $(".navBar ul").removeClass("showMenus");
        // });


        function OpenModal(element, param=null)
            {
                $(".close").click();
                setTimeout(function () {
                    $(element).modal('show');
                    if(param!=null){
                        $('#login .nav-tabs '+param).click();
                    }
                }, 500);
            }
            $(document).ready(function(){
            $('.bars').on('click', function(){
                $('.nav-content').addClass('trims');
            });
            $('#closest').on('click', function(){
                $('.nav-content').removeClass('trims');
            });
        });

        $(function(){
           $(window).on('scroll',function(){
             if($(document).scrollTop() > 50){
                $('#header').addClass('shrink');
             }else{
                $('#header').removeClass('shrink');
             }
           });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            (function ($) {
                $('#header__icon').click(function (e) {
                    e.preventDefault();
                    $('body').toggleClass('with--sidebar');
                });

                $('#site-cache').click(function (e) {
                    $('body').removeClass('with--sidebar');
                });
            })(jQuery);

        });

        $('.fe-owl').owlCarousel({
            autoplay:true,
            loop:true,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:1,
                    nav:false
                },
                1199:{
                    items:1,
                    nav:false,
                    dots:true,
                },
                1200:{
                    items:2,
                    nav:true,
                    navText: ["<img src='{{asset('public/front/images/left.png')}}'>","<img src='{{asset('public/front/images/right.png')}}'>"],
                    loop:false
                }
            }
        })
    </script>