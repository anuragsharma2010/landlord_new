<!-----------Header---------------->

<header class="header-wrapper" id="header">
        <div class="container">
            <nav class="nav-wraper navs-in">
                <a href="{{ url('/') }}" class="brand"> <img src="{{asset('public/front/images/home/logo.jpg')}}" alt="logo"></a>
                
                <div class="nav-content">
                    <button type="button" class="close m-0" id="closest">
                        <span aria-hidden="true" class="font-clrff">×</span>
                    </button>
                    <div class="nav-cinner">
                        <ul class="links-controls">
                            <li>
                                <a href="#" class="active font1">For Renter</a>
                            </li>
                            <li>
                                <a href="{{ url('about-us') }}" class="font1">About Us</a>
                            </li>
                             <li>
                                <a href="{{ url('for-management') }}" class="font1">For Management</a>
                            </li>
                            <li>
                                <a href="{{ url('pages/contact-us') }}" class="font1">Contact Us</a>
                            </li> 
                        </ul>
                    </div>
                </div>
                <div class="user-name align-self-center ">
                    <ul class="homeheader d-flex">
                    @guest
                        <li>
                            <a class="font18 font1" onclick="OpenModal('#login', '#home-tab')">Sign In</a>
                        </li>
                        <li class="font18 font1">
                            <a onclick="OpenModal('#login', '#profile-tab')">Sign Up</a>
                        </li>
                    @else
                    <li>
                            <a class="font18 font1" href="{{ url('account') }}">{{ ucwords(Auth::user()->name) }}</a>
                        </li>
                        <li class="font18 font1">
                            <a class="" href="{{ route('logout') }}"    
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                    </ul>
                </div>
                <button class="bars"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </nav>
        </div>
</header>

<!-----------Header-end---------------->