@if(isset($breadcrumb))
<ul class="d-flex justify-content-center">
    @foreach($breadcrumb['pages'] as $key=>$pages)
        @if(is_array($pages))
            <li>{!!  Html::decode(Html::linkAsset(route($pages[0],$pages[1]), $key, array('class'=>'font18 font1 color20'))) !!}</li>
        @else
            <li>{!!  Html::decode(Html::linkAsset(route($pages), $key, array('class'=>'font18 font1 color20'))) !!}</li>
        @endif
    @endforeach
    <li><a href="" class="font18 font1 color20 px-2">></a></li>
    <li><a href="javascript:void(0);" class="font18 font1 color20 active-pg">{{ $breadcrumb['active'] }}</a></li>
</ul>
@endif