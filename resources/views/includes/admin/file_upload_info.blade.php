<div class="callout callout-info ">
    <ul>
        <li>{{trans('admin.ALLOW_IMAGE_TYPE')  }} &nbsp; {{ config('global.image_mime_type') }}</li>
        <li>{{trans('admin.MAX_UPLOAD_FILE_SIZE')  }} &nbsp; {{ config('global.upload_file_size') }}</li>

        @if(isset($filecount))
            <li>{{trans('admin.MAXIMUM :FILECOUNT FILES_ARE_ALLOWED_TO_BE_UPLOADED', ['FILECOUNT' => config('global.max_file_count_upload')]) }}</li>
        @endif

        <li>{{trans('admin.LARGE_FILES_MAY_TAKE_SOME_TIME')  }}</li>
    </ul>
</div>