<head>
    <meta charset="utf-8" name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <title>{{isset($title) ? config('settings.CONFIG_SITE_TITLE')." :: ".$title : config('settings.CONFIG_SITE_TITLE') }}</title>

    <meta name="description" content="Bringing back the lost art of socializing over meals.">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{asset('public/admin/img/favivo.png')}}">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon57.png')}}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon72.png')}}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon76.png')}}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon114.png')}}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon120.png')}}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon144.png')}}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon152.png')}}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{asset('public/admin/img/icon180.png')}}" sizes="180x180">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    {!! Html::style( asset('public/admin/css/bootstrap.min.css')) !!}

    <!-- Related styles of various icon packs and plugins -->
    {!! Html::style( asset('public/admin/css/plugins.css')) !!}

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    {!! Html::style( asset('public/admin/css/main.css')) !!}
    {!! Html::style( asset('public/admin/css/custom.css')) !!}
    {!! Html::style( asset('public/admin/css/font-awesome.css')) !!}
    {!! Html::style( asset('public/admin/css/jquery.noty.css')) !!}
    {!! Html::style( asset('public/admin/css/noty_theme_default.css')) !!}
    {!! Html::style( asset('public/admin/css/select2.min.css')) !!}
    {!! Html::style( asset('public/admin/css/bootstrap-clockpicker.min.css')) !!}
    {!! Html::style( asset('public/admin/css/bootstrap-multiselect.css')) !!}
    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    {!! Html::style( asset('public/admin/css/themes.css')) !!}
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) -->
    {!! Html::script(asset('public/admin/js/vendor/modernizr-3.3.1.min.js')) !!}
    
    <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    {!! Html::script(asset('public/admin/js/vendor/jquery-2.2.4.min.js')) !!}
    {!! Html::script(asset('public/admin/js/vendor/bootstrap.min.js')) !!}
    {!! Html::script(asset('public/admin/js/plugins.js')) !!}
    {!! Html::script(asset('public/admin/js/app.js')) !!}

    {!! Html::script(asset('public/admin/js/jquery.noty.js')) !!}
    {!! Html::script(asset('public/admin/js/global.js')) !!}
    {!! Html::script(asset('public/admin/js/jquery.blockUI.js')) !!}
    {!! Html::script(asset('public/admin/js/select2.full.min.js')) !!}
    {!! Html::script(asset('public/admin/js/bootstrap-clockpicker.min.js')) !!}
    {!! Html::script(asset('public/admin/js/bootstrap-multiselect.js')) !!}
    
    <script type="text/javascript">
        var IMAGE_URL = "{!! WEBSITE_ADMIN_IMG_URL !!}";
    </script>

    <script type="text/javascript">
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    </script>

    {!! Html::script(asset('public/admin/assets/widgets/ckeditor/ckeditor.js')) !!}

<script>
 CKEDITOR.on( 'instanceCreated', function( event ) {
        var editor = event.editor,
                element = editor.element;
        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
            editor.on( 'configLoaded', function() {
                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                'forms,iframe,image,newpage,removeformat,' +
                'smiley,specialchar,stylescombo,templates';
                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',      groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',    groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }
    });
 /*CKEDITOR.replace( 'ckeditor', {
  extraPlugins: 'imageuploader'
});*/

</script>
</head>