<?php
    $slug = BasicFunction::getCurrentUrl();
    $loggedInUser = adminUser();
    //pr($user, true);
?>

<!-- Main Sidebar -->
<div id="sidebar">
    <!-- Sidebar Brand -->
    <div id="sidebar-brand" class="themed-background">
        <a href="{{route('admin.dashboard.index')}}" class="sidebar-title">
            <i class="fa fa-cube"></i> 
            <span class="sidebar-nav-mini-hide"><strong>Landlord</strong></span>
        </a>
    </div>
    <!-- END Sidebar Brand -->

    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <?php 
                    $admin_menus = BasicFunction::getAllList();
                    
                    $module_ids = array();
                    if(!empty($loggedInUser)) {
                        $rolePermissions = BasicFunction::getPermissionByRoleId($loggedInUser->role_id);
                        if(!empty($rolePermissions)) {
                            
                            foreach($rolePermissions as $rolePermission) {
                                if($rolePermission['can_index'] == 1) {
                                    $module_ids[] = $rolePermission['module_id'];
                                }

                            }
                        }
                    }
                    
                ?>
                
                @foreach ($admin_menus as $admin_menu)
                    @if(in_array($admin_menu['id'], $module_ids) || $loggedInUser->role_id == 1)
                        @if(empty($admin_menu['child_list']))
                            <?php
                                $active_1 = '';
                                $route_1 = BasicFunction::getCurrentUrl($admin_menu['route']);

                                if ($slug == $route_1) {
                                    $active_1 = 'active';
                                }
                            ?>

                            <li>
                                <a href="{{ ($admin_menu['route']!='')?route($admin_menu['route']):'#' }}" class="{{$active_1}}">
                                    <i class="fa {{ $admin_menu['icon'] }}"></i> &nbsp;
                                    <span class="sidebar-nav-mini-hide"> {{ $admin_menu['name'] }}</span>
                                </a>
                            </li>
                        @else
                            @foreach ($admin_menu['child_list'] as $child_list)
                                <?php
                                    $active_2 = '';
                                    $route_2 = BasicFunction::getCurrentUrl($child_list['route']);
                                    if ($slug == $route_2) {
                                        $active_2 = 'active';
                                    }
                                ?>
                            @endforeach

                            <li class="{{$active_2}}">
                                <a href="#" class="sidebar-nav-menu">
                                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                    <i class="fa {{ $admin_menu['icon'] }}"></i> &nbsp;
                                    <span class="sidebar-nav-mini-hide"> {{ $admin_menu['name'] }}</span>
                                </a>
                                <ul>
                                    @foreach ($admin_menu['child_list'] as $child_list)
                                        @if(in_array($child_list['id'], $module_ids) || $loggedInUser->role_id == 1)
                                            <?php
                                                $active_3 = '';
                                                $route_3 = BasicFunction::getCurrentUrl($child_list['route']);

                                                if ($slug == $route_3) {
                                                    $active_3 = 'active';
                                                }
                                            ?>
                                            <li>
                                                <a href="{{ ($child_list['route']!='')?route($child_list['route']):'#' }}" class="{{$active_3}}">{{ $child_list['name'] }}</a>
                                            </li>
                                        @endif
                                    @endforeach    
                                </ul>
                            </li>
                        @endif
                    @endif
                @endforeach    
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->

    <!-- Wrapper for scrolling functionality -->
<!--    <div id="sidebar-scrsoll">
         Sidebar Content 
        <div class="sidebar-content">
             Sidebar Navigation 
            <ul class="sidebar-nav">
                <li>
                    <a href="index.html" class=" active">
                        <i class="gi gi-compass sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-airplane sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Components</span>
                    </a>
                    
                    <ul>
                        <li><a href="page_comp_todo.html">To-do List</a></li>
                        <li><a href="page_comp_gallery.html">Gallery</a></li>
                        <li><a href="page_comp_maps.html">Google Maps</a></li>
                        <li><a href="page_comp_calendar.html">Calendar</a></li>
                        <li><a href="page_comp_charts.html">Charts</a></li>
                        <li><a href="page_comp_animations.html">CSS3 Animations</a></li>
                        <li><a href="page_comp_tree.html">Tree View Lists</a></li>
                        <li><a href="page_comp_nestable.html">Nestable &amp; Sortable Lists</a></li>
                    </ul>
                </li>

                <li>
                    <a href="page_app_social.html">
                        <i class="fa fa-share-alt sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Social Net</span>
                    </a>
                </li>
                
                <li>
                    <a href="page_app_media.html">
                        <i class="gi gi-picture sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Media Box</span>
                    </a>
                </li>
                
                <li>
                    <a href="page_app_estore.html">
                        <i class="gi gi-shopping_cart sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">eStore</span>
                    </a>
                </li>
            </ul>
             END Sidebar Navigation 

             Color Themes 
             Preview a theme on a page functionality can be found in js/app.js - colorThemePreview() 
            <div class="sidebar-section sidebar-nav-mini-hide">
                <div class="sidebar-separator push">
                    <i class="fa fa-ellipsis-h"></i>
                </div>
                
                <ul class="sidebar-themes clearfix">
                    <li>
                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default" data-theme="default" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-default"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy" data-theme="{{asset('admin/css/themes/classy.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-classy"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social" data-theme="{{asset('admin/css/themes/social.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-social"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat" data-theme="{{asset('admin/css/themes/flat.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-flat"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst" data-theme="{{asset('admin/css/themes/amethyst.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-amethyst"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme" data-theme="{{asset('admin/css/themes/creme.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-creme"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion" data-theme="{{asset('admin/css/themes/passion.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                            <span class="section-side themed-background-dark-passion"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default + Light Sidebar" data-theme="default" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy + Light Sidebar" data-theme="{{asset('admin/css/themes/classy.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social + Light Sidebar" data-theme="{{asset('admin/css/themes/social.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat + Light Sidebar" data-theme="{{asset('admin/css/themes/flat.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst + Light Sidebar" data-theme="{{asset('admin/css/themes/amethyst.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme + Light Sidebar" data-theme="{{asset('admin/css/themes/creme.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion + Light Sidebar" data-theme="{{asset('admin/css/themes/passion.css')}}" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                            <span class="section-side"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default + Light Header" data-theme="default" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-default"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy + Light Header" data-theme="{{asset('admin/css/themes/classy.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-classy"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social + Light Header" data-theme="{{asset('admin/css/themes/social.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-social"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat + Light Header" data-theme="{{asset('admin/css/themes/flat.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-flat"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst + Light Header" data-theme="{{asset('admin/css/themes/amethyst.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">

                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-amethyst"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme + Light Header" data-theme="{{asset('admin/css/themes/creme.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-creme"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion + Light Header" data-theme="{{asset('admin/css/themes/passion.css')}}" data-theme-navbar="navbar-default" data-theme-sidebar="">
                            <span class="section-header"></span>
                            <span class="section-side themed-background-dark-passion"></span>
                            <span class="section-content"></span>
                        </a>
                    </li>
                </ul>
            </div>
             END Color Themes 
        </div>
         END Sidebar Content 
    </div>-->
    <!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->