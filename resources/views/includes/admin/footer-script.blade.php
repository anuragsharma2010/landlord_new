<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgIoKihA3jHH_ZftnjjZq3nF-ZTfDF9Us&libraries=places"></script>
 


<script type="text/javascript">
 /* script */
function initialize() {
    var lati = $("#latitude").val();
    var longi = $("#longitude").val();
    

    if(lati != ''){
        var latitude = parseFloat(lati);
    }else{
        var latitude = 10.536421;
    }

    if(longi != ''){
        var longitude = parseFloat(longi);
    }else{
        var longitude = -61.311951;
    }
    
    
    var latlng = new google.maps.LatLng(latitude,longitude);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latitude, lng: longitude},
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });

   
 
    var input = document.getElementById('searchInput');
    //console.log(input);

    
   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    //console.log(input);
    if(lati != '' && longi != ''){
        var formatted_address = $("#searchInput").val();
        if(formatted_address != ''){
            infowindow.open(map, marker);
            infowindow.setContent(formatted_address);   
        }
    }

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
        
        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);
       
    });

    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {   
         // console.log(results[0]);
            
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });

}

function bindDataToForm(address,lat,lng){
   document.getElementById('searchInput').value = address;
   document.getElementById('latitude').value = lat;
   document.getElementById('longitude').value = lng;
}

google.maps.event.addDomListener(window, 'load', initialize); 
</script>