<?php
   /*
    |--------------------------------------------------------------------------
    | Validation Language Lines for front 
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

return [

    /******** Event User Invite Management Start   */
    'EVENT_USER_NOT_REGISTERED'=>'You are not registered on Matrix. You can join event after registering on app.',
    'EVENT_INVITE_RESPONSE_URL_INVALID'=>'Event invitation url is not valid.',
    'EVENT_NOT_EXISTS'=>'Event does not exists.',
    'EVENT_USER_NOT_INVITED'=> 'You are not invited to this event.',
    'EVENT_INVITATION_EXPIRED'=>'Event invitation expired.',
    'EVENT_INVITE_ATTEND_SUCCESS'=>'Thanks for accepting the invitation.',
    'EVENT_INVITE_NOT_ATTEND_SUCCESS'=>'Your event invitation has been declined.',
    /******** Event User Invite Management End   */

    /******** Contact enquiry Management Start   */
    'CONTACT_ENQUIRY_SUCCESS'=>'Contact enquiry submitted successfully.',
    'CONTACT_ENQUIRY_ERROR'=>'Error while submitting contact enquiry.',
    'UNAUTHORIZED' => 'Unauthorized',
    'UNAUTHORIZED_ACCESS' => 'Unauthorized Access',
    'UNAUTHORIZED_ACCESS_DONT_PERMISSION' => 'You don\'t have permission.',

    '404NOTFOUND' => '404 Not Found',
    'PAGENOTFOUND' => 'Page Not Found',

    'CONTACT_US' => 'Contact Us',
    'CONTACT_ADD_SUCCESSFULLY' => 'We will contact with you asap.',

    'HOME' => 'Home',
    'MY_ACCOUNT' => 'My Account'
    /******** Contact enquiry Management End   */

];
