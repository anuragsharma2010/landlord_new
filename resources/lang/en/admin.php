<?php
   /*
    |--------------------------------------------------------------------------
    | Validation Language Lines for admin 
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

return [
    'SITE_TITLE'         => 'Site Title',
    'ADMIN_PAGE_LIMIT'   =>'Admin Page Limit',
    'GLOBAL_SETTINGS'    =>'Global Settings',
    'SUBMIT'             =>'Submit',
    'CANCEL'             =>'Cancel',
    'FRONT_PAGE_LIMIT'=>'Front Page Limit',
    'FROM_NAME'=>'From Name',
    'NAME'=>'Name',
    'EMAIL_SIGNATURE'=>'Email Signature',
    'REPLY_TO_EMAIL'=>'Reply To Email',
    'META_TITLE'=>'Meta Title',
    'META_KEYWORDS'=>'Meta Keywords',
    'STAFF_MAIL'=>'Staff Mail',
    'META_DESCRIPTION'=>'Meta Description',
    'FROMEMAIL'=>'From Mail',
    'COPYRIGHT'=>'Copyright',
    'CONFIG_MANAGEMENT'=>'Config Management',
    'DASHBOARD'=>' Dashboard',
    'RESET_PASSWORD'=>'Reset Password',
    'EMAIL_LOG'=>'EmailLog',
    'EMAIL_TYPE'=>'Email Type',
    'EMAIL_FROM'=>'Email From',
    'EMAIL_To'=>'Email To',
    'SENT_ON'=>'Email On',
    'ACTION'=>'Action',
    'DELETE'=>'Delete',
    'VIEW'=>'View',
    'DELETE_ALERT'=>'Are you sure you want to delete this records?',
    'EMAILLOG_DELETED_SUCCESSFULLY'=>'EmailLog deleted successfully.',
    'VIEW_EMAIL_LOG'=>'View EmailLog',
    'BACK'=>'Back',
    'TO'=>'To',
    'CMS_PAGES'=>'Cms Pages',
    'TITLE'=>'Title',
    'RANGE'=>'Range',
    'RANGE_START'=>'Range Start',
    'RANGE_END'=>'Range End',
    'DESCRIPTION'=>'Description',
    'STATUS'=>'Status',
    'META_TITLE'=>'Meta Title',
    'META_KEYWORDS'=>'Meta Keywords',
    'META_DESCRIPTION'=>'Meta Description',
    'SAVE'=>'Save',
    'RESET'=>'Reset',
    'CREATED_AT'=>'Created At',
    'UPDATED_AT'=>'Updated At',
    'ADD_NEW'=>'Add New',
    'ADD_CMS_PAGES'=>'Add Cms Page',
    'CMSPAGES_ADD_SUCCESSFULLY'=>'Cms page added successfully',
    'ACTIVE'=>'Active',
    'ACTIVE_ALERT'=>'Are you sure you want to active this records?',
    'INACTIVE'=>'Inactive',
    'INACTIVE_ALERT'=>'Are you sure you want to inactive this records?',
    'EDIT'=>'Edit',
    'SHOW'=>'View',
    'CMSPAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Cms page status changed successfully',
    'CMSPAGES_UPDATE_SUCCESSFULLY'=>'Cms page updated successfully',
    'CMSPAGES_DELETE_SUCCESSFULLY'=>'Cms page delete successfully.',
    'CANCEL'=>'Cancel',
    'EDIT_CMS_PAGES'=>'Edit Cms Page',
    'INVALID_URL'=>'Requested URL is invlaid',
    'HELLO'=>'Hello',
    'MY_PROFILE'=>'My Profile',
    'FIRST_NAME'=>'First Name',
    'LAST_NAME'=>'Last Name',
    'PRICE'=>'Price',
    'price'=>'Price',
    'EMAIL'=>'Email',
    'BACK_TO_DASHBOARD'=>'Back To Dashboard',
    'CHANGE_PASSWORD'=>'Change Password',
    'OLD_PASSWORD'=>'Old Password',
    'NEW_PASSWORD'=>'New Password',
    'CONFIRM_PASSWORD'=>'Confirm Password',
    'PASSWORD_CHANGE_SUCCESS'=>'Your Account password changed successfully',
    'MY_PROFILE_UPDATE_SUCCESS'=>'Your Profile updated successfully',
    'LOGIN_EMAIL_PASSWORD_NOT_MATCH'=>'Incorrect combination of email address or password',
    'LOGIN_SUCCESSFULLY'=>'Welcome to :site_tite,&nbsp;  :Name',
    'LOGOUT_SUCCESSFULLY'=>'You have successfully logged out',
    'NOT_LOGIN'=>'You are not logged in',
    'NOT_AUTHORITY'=>'You have not authority',
    'EMAIL_TEMPLATES'=>'Email Templates',
    'EDIT_EMAIL_TEMPLATES'=>'Edit Email Templates',
    'NAME'=>'Name',
    'SUBJECT'=>'Subject',
    'INSERT_CONSTRAINTS'=>'Insert Constraints',
    'FOOTER_CONTACT_INFO'=>'Footer Contact Info',
    'SOCIAL_ICON'=>'Social Icon',
    'FACEBOOK_ICON'=>'Facebook_Icon',
    'TWITTER_ICON'=>'Twitter_Icon',
    'GOOGLE_PLUS_ICON'=>'Google_Plus_Icon',
    'LINKEDIN_ICON'=>'Linkedin Icon',
    'PHONE_NUMBER'=>'Phone Number',
    'BLOG_CATEGORY'=>'Blog Categories',
    'LOCATION'=>'Location',
    'RATE'=>'Rate',
    'QUANTITY'=>'Quantity',
    'ADDRESS'=>'Address',
    'OTHER_ADDRESS'=>'Other Address',
    'COUNTRY'=>'Country',
    'STATE'=>'State',
    'CITY'=>'City',
    'SENDER'=>'Sender',
    'RECEIVER'=>'Receiver',
    'MESSAGE'=>'Message',
    'NOTIFICATIONS'=>'Manage Notifications',
    'IMAGE' => 'Image',
    'AMOUNT' => 'Amount',
    'DATETIME' => 'Date & Time',
    'TAX' => 'Tax (%)',
    'CUTOFF_TIME' => 'Cutoff Time (Hours)',
    'PERCENTAGE' => 'Percentage',
    'EXPIRY_DATE' => 'Expiry Date',
    'USE_PER_PERSON' => 'Use per Person',
    'MINIMUM_AMOUNT' => 'Minimun Amount',
    'SUBTOTAL' => 'Sub Total',
    'UNAUTHORIZED' => 'Unauthorized',
    'UNAUTHORIZED_ACCESS' => 'Unauthorized Access',
    'UNAUTHORIZED_ACCESS_DONT_PERMISSION' => 'You don\'t have permission.',
    'ADMIN_EMAIL' => 'Admin Email',

    /******** User Management Start   */
    'USER_NAME'=>'User',
    'USERS'=>'Users',
    'PHONE'=>'Phone Number',
    'SEND_CREDENTIALS'=>'Send Credential',
    'ADD_USER'=>'Add User',
    'EDIT_USER'=>'Edit User',
    'BUSINESS_NAME'=>'Business Name',
    'BUSINESS_EMAIL'=>'Business Email',
    'OTHER_EMAIL'=>'Other Email',
    'OTHER_CONTACT'=>'Other Contact',
    'PASSWORD'=>'Password',
    'USER_ADD_SUCCESSFULLY'=>'User added successfully.',
    'USER_CHANGE_STATUS_SUCCESSFULLY'=>'User status changed successfully.',
    'PROFILE_IMAGE'=>'Profile Image',
    'USER_UPDATE_SUCCESSFULLY'=>'User updated successfully.',
    'CREDENTIALS_SEND_SUCCESSFULLY'=>'Credentials send successfully.',
    'USER_DATA_NOT_EXPORTED'=>'User data cannot be exported.',
    'USER_DIETARIES' => 'User Dietaries',
    'USER_CUISINES' => 'User Cuisines',
    'DOB' => 'Date of birth',
    'SOCIAL_SECURITY_NUMBER' => 'Social Security Number',
    'MARITAL_STATUS' => 'Marital Status',
    'GOVTID' => 'Govt ID',

    /******** User Management End   */

    /******** Landlord Management Start   */
    'LANDLORD_NAME'=>'Landlord',
    'LANDLORDS'=>'Landlords',
    'PHONE'=>'Phone Number',
    'SEND_CREDENTIALS'=>'Send Credential',
    'ADD_LANDLORD'=>'Add Landlord',
    'EDIT_LANDLORD'=>'Edit Landlord',
    'VIEW_LANDLORD'=>'View Landlord',
    'BUSINESS_NAME'=>'Business Name',
    'BUSINESS_EMAIL'=>'Business Email',
    'OTHER_EMAIL'=>'Other Email',
    'OTHER_CONTACT'=>'Other Contact',
    'PASSWORD'=>'Password',
    'LANDLORD_ADD_SUCCESSFULLY'=>'Landlord added successfully.',
    'LANDLORD_CHANGE_STATUS_SUCCESSFULLY'=>'Landlord status changed successfully.',
    'PROFILE_IMAGE'=>'Profile Image',
    'LANDLORD_UPDATE_SUCCESSFULLY'=>'Landlord updated successfully.',
    'LANDLORD_DELETE_SUCCESSFULLY'=>'Landlord delete successfully.',
    'CREDENTIALS_SEND_SUCCESSFULLY'=>'Credentials send successfully.',
    'LANDLORD_DATA_NOT_EXPORTED'=>'Landlord data cannot be exported.',

    /******** Landlord Management End   */

    /******** Cater Management Start   */
    'CATERERS'=>'Manage Caterers',
    'ADD_CATERER'=>'Add Caterer',
    'EDIT_CATERER'=>'Edit Caterer',
    'VIEW_CATERERS'=>'View Caterer',
    'BANK_ACCOUNT'=>'Bank Account',
    'ONLINE_ACCOUNT1'=>'Online Account1',
    'ONLINE_ACCOUNT2'=>'Online Account2',
    'CATERER_ADD_SUCCESSFULLY'=>'Caterer added successfully.',
    'CATERER_UPDATE_SUCCESSFULLY'=>'Caterer updated successfully.',
    'CATERER_CHANGE_STATUS_SUCCESSFULLY'=>'Caterer status changed successfully.',
    
    /******** Preparer Management Start   */

    'PREPARERS'=>'Manage Preparers',
    'ADD_PREPARER'=>'Add Preparer',
    'EDIT_PREPARER'=>'Edit Preparer',
    'VIEW_PREPARERS'=>'View Preparer',
    'PREPARER_ADD_SUCCESSFULLY'=>'Preparer added successfully.',
    'PREPARER_UPDATE_SUCCESSFULLY'=>'Preparer updated successfully.',
    'PREPARER_CHANGE_STATUS_SUCCESSFULLY'=>'Preparer status changed successfully.',
    
    /******** Cater Management Start   */
    
    /******** Role Management Start   */
    'ROLE'=>'Role',
    'ROLES'=>'Manage Roles',
    'SELECT_ROLES'=>'Select Any Role',
    'ADD_ROLE'=>'Add Role',
    'EDIT_ROLE'=>'Edit Role',
    'ROLE_ADD_SUCCESSFULLY'=>'Role added successfully.',
    'ROLE_CHANGE_STATUS_SUCCESSFULLY'=>'Role status changed successfully.',
    'ROLE_UPDATE_SUCCESSFULLY'=>'Role updated successfully.',

    /******** Role Management End   */

    /******** Allergy Management Start   */
    'ALLERGY'=>'Allergies',
    'ALLERGIES'=>'Manage Allergies',
    'SELECT_ALLERGIES'=>'Select Any Allergy',
    'ADD_ALLERGY'=>'Add Allergy',
    'EDIT_ALLERGY'=>'Edit Allergy',
    'ALLERGY_ADD_SUCCESSFULLY'=>'Allergy added successfully.',
    'ALLERGY_CHANGE_STATUS_SUCCESSFULLY'=>'Allergy status changed successfully.',
    'ALLERGY_UPDATE_SUCCESSFULLY'=>'Allergy updated successfully.',
    /******** Allergy Management End   */

    /******** Business Management Start   */
    'BUSINESS'=>'Business',
    'ALLERGIES'=>'Manage Business',
    'SELECT_ALLERGIES'=>'Select Any Business',
    'ADD_BUSINESS'=>'Add Business',
    'EDIT_BUSINESS'=>'Edit Business',
    'BUSINESS_ADD_SUCCESSFULLY'=>'Business added successfully.',
    'BUSINESS_CHANGE_STATUS_SUCCESSFULLY'=>'Business status changed successfully.',
    'BUSINESS_UPDATE_SUCCESSFULLY'=>'Business updated successfully.',
    'BUSINESS_DELETE_SUCCESSFULLY'=>'Business delete successfully.',
    /******** Business Management End   */

    /******** Position Management Start   */
    'POSITION'=>'Position',
    'ALLERGIES'=>'Manage Position',
    'SELECT_ALLERGIES'=>'Select Any Position',
    'ADD_POSITION'=>'Add Position',
    'EDIT_POSITION'=>'Edit Position',
    'POSITION_ADD_SUCCESSFULLY'=>'Position added successfully.',
    'POSITION_CHANGE_STATUS_SUCCESSFULLY'=>'Position status changed successfully.',
    'POSITION_UPDATE_SUCCESSFULLY'=>'Position updated successfully.',
    'POSITION_DELETE_SUCCESSFULLY'=>'Position delete successfully.',
    /******** Position Management End   */

    /******** Building Management Start   */
    'BUILDING'=>'Building',
    'ALLERGIES'=>'Manage Building',
    'SELECT_ALLERGIES'=>'Select Any Building',
    'ADD_BUILDING'=>'Add Building',
    'EDIT_BUILDING'=>'Edit Building',
    'VIEW_BUILDING'=>'View Building',
    'BUILDING_ADD_SUCCESSFULLY'=>'Building added successfully.',
    'BUILDING_CHANGE_STATUS_SUCCESSFULLY'=>'Building status changed successfully.',
    'BUILDING_UPDATE_SUCCESSFULLY'=>'Building updated successfully.',
    'BUILDING_DELETE_SUCCESSFULLY'=>'Building delete successfully.',
    'ZIP_CODE'=>'Zip Code',
    /******** Building Management End   */

    /******** Cuision Management Start   */
    'CUISION'=>'Cuisine',
    'CUISIONS'=>'Manage Cuisine',
    'SELECT_CUISIONS'=>'-- Select Cuisine --',
    'ADD_CUISION'=>'Add Cuisine',
    'EDIT_CUISION'=>'Edit Cuisine',
    'CUISION_ADD_SUCCESSFULLY'=>'Cuisine added successfully.',
    'CUISION_CHANGE_STATUS_SUCCESSFULLY'=>'Cuisine status changed successfully.',
    'CUISION_UPDATE_SUCCESSFULLY'=>'Cuisine updated successfully.',
    'CUISINE_IMAGE'=>'Cuisine Image',
    'CUISINE_BUDGETS'=>'Cuisine Budgets',
    'ADD_CUISION_BUDGET'=>'Add Cuisine Budget',
    'CUISION_BUDGET_ASSIGNED'=>'Cuisine budget already assigned',
    'CUISION_BUDGET_ASSIGNED_SUCCESSFULLY'=> 'Cuisine budget assigned successfully.',
    'CUISION_BUDGET_CHANGE_STATUS_SUCCESSFULLY'=> 'Cuisine budget status changed successfully.',
    'ADD_CUISINE_BUDGET'=>'Add Cuisine Budget',
    /******** Cuision Management End   */

    /******** Dietary Management Start   */
    'DIETARY'=>'Dietary',
    'DIETARIES'=>'Manage Dietaries',
    'SELECT_DIETARIES'=>'Select Any Dietary',
    'ADD_DIETARY'=>'Add Dietary',
    'EDIT_DIETARY'=>'Edit Dietary',
    'DIETARY_ADD_SUCCESSFULLY'=>'Dietary added successfully.',
    'DIETARY_CHANGE_STATUS_SUCCESSFULLY'=>'Dietary status changed successfully.',
    'DIETARY_UPDATE_SUCCESSFULLY'=>'Dietary updated successfully.',
    'OTHER_DIETARIES'=>'Other Dietaries',
    /******** Dietary Management End   */

    /******** Budgets Management Start   */
    'BUDGET'=>'Budget',
    'BUDGETS'=>'Budgets',
    'BUDGETS'=>'Manage Budgets',
    'SELECT_BUDGETS'=>'-- Select Budget --',
    'ADD_BUDGETS'=>'Add Budget',
    'EDIT_BUDGETS'=>'Edit Budget',
    'BUDGETS_ADD_SUCCESSFULLY'=>'Budget added successfully.',
    'BUDGETS_CHANGE_STATUS_SUCCESSFULLY'=>'Budget status changed successfully.',
    'BUDGETS_UPDATE_SUCCESSFULLY'=>'Budget updated successfully.',
    /******** Budgets Management End   */

    /******** protiens Management Start   */
    'PROTEIN'=>'Proteins',
    'PROTEINS'=>'Manage Proteins',
    'SELECT_PROTEINS'=>'Select Any Protein',
    'ADD_PROTEIN'=>'Add Protein',
    'EDIT_PROTEIN'=>'Edit Protein',
    'PROTEIN_ADD_SUCCESSFULLY'=>'Protein added successfully.',
    'PROTEIN_CHANGE_STATUS_SUCCESSFULLY'=>'Protein status changed successfully.',
    'PROTEIN_UPDATE_SUCCESSFULLY'=>'Protein updated successfully.',
    /******** protiens Management End   */

    /******** Bag Management Start   */
    'BAG'=>'Bags',
    'BAGS'=>'Manage Bags',
    'SELECT_BAGS'=>'Select Any Bag',
    'ADD_BAG'=>'Add Bag',
    'EDIT_BAG'=>'Edit Bag',
    'BAG_ADD_SUCCESSFULLY'=>'Bag added successfully.',
    'BAG_CHANGE_STATUS_SUCCESSFULLY'=>'Bag status changed successfully.',
    'BAG_UPDATE_SUCCESSFULLY'=>'Bag updated successfully.',
    /******** Bag Management End   */

    /******** variations Management Start   */
    'VARIATION'=>'Variations',
    'VARIATIONS'=>'Manage Variations',
    'SELECT_VARIATIONS'=>'Select Any Variation',
    'ADD_VARIATION'=>'Add Variation',
    'EDIT_VARIATION'=>'Edit Variation',
    'VARIATION_ADD_SUCCESSFULLY'=>'Variation added successfully.',
    'VARIATION_CHANGE_STATUS_SUCCESSFULLY'=>'Variation status changed successfully.',
    'VARIATION_UPDATE_SUCCESSFULLY'=>'Variation updated successfully.',
    'VARIATION_TYPE'=>'Select Variation Type',
    /******** variations Management End   */

    /******** variations Management Start   */
    'ADDSUBVARIATION'=>'Add Sub Variations',
    'SUB_VARIATIONS'=>'Manage Sub Variations',
    'SELECT_VARIATIONS'=>'Select Any Sub Variation',
    'ADD_SUB_VARIATION'=>'Add Sub Variation',
    'EDIT_SUB_VARIATION'=>'Edit Sub Variation',
    'SUB_VARIATION_ADD_SUCCESSFULLY'=>'Sub Variation added successfully.',
    'SUB_VARIATION_CHANGE_STATUS_SUCCESSFULLY'=>'Sub Variation status changed successfully.',
    'SUB_VARIATION_UPDATE_SUCCESSFULLY'=>'Sub Variation updated successfully.',
    /******** variations Management End   */

    /******** food category Management Start   */
    'ADDFOODCATEGORY'=>'Add Food Categories',
    'FOOD_CATEGORIES'=>'Manage Food Categories',
    'SELECT_CATEGORIES'=>'Select Any Food Category',
    'ADD_FOOD_CATEGORY'=>'Add Food Category',
    'EDIT_FOOD_CATEGORY'=>'Edit Food Category',
    'FOOD_CATEGORY_ADD_SUCCESSFULLY'=>'Food Category added successfully.',
    'FOOD_CATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Food Category status changed successfully.',
    'FOOD_CATEGORY_UPDATE_SUCCESSFULLY'=>'Food Category updated successfully.',
    /******** food category Management End   */

    /************CATEGORY management start **************/
    'CATEGORY'=>'Categories',
    'CATEGORY_TYPE'=>'Category Type',
    'ADD_CATEGORY'=>'Add Category',
    'EDIT_CATEGORY'=>'Edit Category',
    'MAIN_CATEGORY'=>'Mian Category',
    'MAIN_CATEGORY'=>'Mian Category',
    'SUB_CATEGORY'=>'Sub Category',
    'CATEGORY_ADD_SUCCESSFULLY'=>'Category add successfully',
    'CATEGORY_CHANGE_STATUS_SUCCESSFULLY'=>'Category status changed  successfully',
    'CATEGORY_UPDATE_SUCCESSFULLY'=>'Category updated successfully',
    'PLEASE_SELECT'=>'Please Select',
    'SUBCATEGORY'=>'Subcategory',
    'TAG_CATEGORY'=>'Tags',
    /************CATEGORY management end **************/



    /* Faq Start*/
    'ADD_FAQ_CATEGORY'=>'Add Faq Category',
    'FAQCATEGORY'=>'Faq Categories',    
    'ALL_FAQ_CATEGORIES'=>'All Faq Category',
    'FAQ_CATEGORY_ADD_SUCCESSFULLY'=>'Faq category add successfully',
    'EDIT_FAQ_CATEGORY'=>'Edit Faq Category',
    'FAQ_CATEGORY_UPDATE_SUCCESSFULLY'=>'Faq Category Update Successfully',
    'FAQ_CATEGORY_DELETE_SUCCESSFULLY'=>'Faq Category Delete Successfully',
    'ALL_FAQ'=>'Faq',
    'ADD_FAQ'=>'Add Faq',
    'FAQ_ADD_SUCCESSFULLY'=>'Faq Add Successfully',
    'EDIT_FAQ'=>'Edit Faq',
    'FAQ_DELETE_SUCCESSFULLY'=>'Faq Delete Successfully',
    'FAQ_UPDATE_SUCCESSFULLY'=>'Faq Update Successfully',
    'FAQPAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Faqpages Change Status Successsfully',
    'FAQ'=>'Faq',
    
    /*  Admin Menu Management   start */
    'ADMIN_MENUS'=>'Admin Menus',
    'ADD_MENU'=>'Add Menu',
    'EDIT_MENU'=>'Edit Menu',
    'SHOW_ON_DESHBOARD'=>'Show on dashboard',
    'ROUTE'=>'route',
    'ICON'=>'icon',
    'MENU_ORDER'=>'Menu Order',
    'ORDER'=>'Order',
    'ADMIN_MENUS_ADD_SUCCESSFULLY'=>'Admin Menu added successfully.',
    'YES'=>'Yes',
    'NO'=>'No',
    'CHILD_MENU'=>'Child Menu',
    'ADMIN_MENUS_UPDATE_SUCCESSFULLY'=>'Admin Menu updated successfully.',
    'ADMIN_MENUS_CHANGE_STATUS_SUCCESSFULLY'=>'Admin Menu status changed successfully.',
    'MENUS'=>'Menu',

    /* Admin Menu Management   end */

  /*  Menu Management   start */


    'DYNAMIC'=>'Dynamic',
    'TYPE'=>'Type',
    'PARAMETER'=>'Parameter',
    'POSITION'=>'Position',
    'PARAMETER_INFO'=>'Please Enter Parameter in JSON form Like:- {a:1,b:2,c:3,d:4} ',
    'MENUS_ADD_SUCCESSFULLY'=>'Menu added successfully.',
    'MENUS_UPDATE_SUCCESSFULLY'=>'Menu updated successfully.',
    'MENUS_CHANGE_STATUS_SUCCESSFULLY'=>'Menu status changed successfully.',
  /* Menu Management   end */

/* Contacts Start*/
    'CONTACT_NUMBER'=>'Contact Number',
    'ENQUIRY_TYPE'=>'Enquiry Type',
    'CONTACT_DELETE_SUCCESSFULLY'=>'Contact delete successfully',
    'All_CONTACTS'=>'CONTACTS',

    'PARKING_PAGES'=>'Parking Spot',
    'ADD_PARKING_PAGES'=>'Add Parking Spot',
    'EDIT_PARKING_PAGES'=>'Edit Parking Spot',
    'PARKINGPAGES_CHANGE_STATUS_SUCCESSFULLY'=>'Parking spot status changed successfully',

    'PERMISSION_PAGES'=>'Permissons Manager',


    /******** Foods Management Start   */
    'FOODS'=>'Manage Foods',
    'SELECT_FOOD'=>'Select Any Food',
    'ADD_FOOD'=>'Add Food',
    'EDIT_FOOD'=>'Edit Food',
    'FOOD_ADD_SUCCESSFULLY'=>'Food added successfully.',
    'FOOD_CHANGE_STATUS_SUCCESSFULLY'=>'Food status changed successfully.',
    'FOOD_UPDATE_SUCCESSFULLY'=>'Food updated successfully.',
    /******** Foods Management End   */

    'PREPAIRTIME'=>'Prepair Time',
    'DELHIVERYTIME'=>'Delivery Time',

    /******** address Management Start   */
    'ADDRESS'=>'Address',
    'SELECT_ADDRESS'=>'Select Any Address',
    'ADD_ADDRESS'=>'Add Address',
    'EDIT_ADDRESS'=>'Edit Address',
    'ADDRESS_ADD_SUCCESSFULLY'=>'Address added successfully.',
    'ADDRESS_CHANGE_STATUS_SUCCESSFULLY'=>'Address status changed successfully.',
    'ADDRESS_UPDATE_SUCCESSFULLY'=>'Address updated successfully.',
    'ADDRESS_TYPE'=>'Select Address Type',
    /******** address Management End   */

    'ADDRESS1'=>'Address1',
    'ADDRESS2'=>'Address2',
    'LANDMARK'=>'Landmark',

    /******** Location Management Start   */
    'LOCATION'=>'Location',
    'LOCATIONS'=>'Manage Location',
    'SELECT_LOCATION'=>'Select Location',
    'ADD_LOCATION'=>'Add Location',
    'EDIT_LOCATION'=>'Edit Location',
    'LOCATION_ADD_SUCCESSFULLY'=>'Location added successfully.',
    'LOCATION_CHANGE_STATUS_SUCCESSFULLY'=>'Location status changed successfully.',
    'LOCATION_UPDATE_SUCCESSFULLY'=>'Location updated successfully.',
    /******** Location Management End   */

    /******** Event Management Start   */
    'EVENT'=>'Events',
    'EVENTS'=>'Manage Event',
    'SELECT_EVENT'=>'Select Event',
    'ADD_EVENT'=>'Add Event',
    'EDIT_EVENT'=>'Edit Event',
    'EVENT_ADD_SUCCESSFULLY'=>'Event added successfully.',
    'EVENT_CHANGE_STATUS_SUCCESSFULLY'=>'Event status changed successfully.',
    'EVENT_UPDATE_SUCCESSFULLY'=>'Event updated successfully.',
    'EVENT_ORGANISER' => 'Event Organiser',
    'EVENT_BUDGET' => 'Event Budget',
    'EVENT_GUEST_COUNT' => 'Guest Count',
    'EVENT_TITLE' => 'Event Title',
    'EVENT_TYPE' => 'Event Type',
    'EVENT_DATE' => 'Event Date',
    'EVENT_TIME' => 'Event Time',
    'EVENT_CUISINE' => 'Event Cuisine',
    'EVENT_FOOD_MENU' => 'Event Food Menu',
    'PROVIDE_TABLEWARE' => 'Provide Tableware',
    'EVENT_DATA_NOT_EXPORTED'=>'Event data cannot be exported.',
    'EVENT_DATE_TIME' => 'Event Date & Time',
    /******** Event Management End   */

    /******** Order Food Management Start   */
    'FOOD_ORDER'=>'Food Orders',
    'FOOD_ORDERS'=>'Manage Food Orders',
    'EDIT_FOOD_ORDER'=>'Edit Food Order',
    'FOOD_ORDER_TYPE' => 'Food Order Type',
    'FOOD_ORDER_DELIVERY_DATE' => 'Food Order Delivery Date',
    'FOOD_ORDER_DELIVERY_TIME' => 'Food Order Delivery Time',
    'FOOD_ORDER_BUDGET' => 'Food Order Budget',
    'FOOD_ORDER_CUISINE' => 'Food Order Cuisine',
    'FOOD_ORDER_ADD_SUCCESSFULLY'=>'Food order added successfully.',
    'FOOD_ORDER_CHANGE_STATUS_SUCCESSFULLY'=>'Food order status changed successfully.',
    'FOOD_ORDER_UPDATE_SUCCESSFULLY'=>'Food order updated successfully.',
    'FOOD_ORDER_DATA_NOT_EXPORTED'=>'Food order data cannot be exported.',
    /******** Order Food Management End   */

     /******** Food Menu Management Start   */
    'FOODMENU'=>'Food Menu',
    'FOODMENUS'=>'Food Menus (Individual)',
    'SELECT_FOODMENU'=>'Select Food Menu',
    'ADD_FOODMENU'=>'Add Food Menu',
    'EDIT_FOODMENU'=>'Edit Food Menu',
    'FOODMENU_ADD_SUCCESSFULLY'=>'Food Menu added successfully.',
    'FOODMENU_CHANGE_STATUS_SUCCESSFULLY'=>'Food Menu status changed successfully.',
    'FOODMENU_UPDATE_SUCCESSFULLY'=>'Food Menu updated successfully.',
    'FOOD_MENU_TYPE'=>'Food Menu Type',
    'FOOD_MENU_FOR'=>'Food Menu For',
    'SELECT_FOOD_MENU_TYPE'=>'-- Select Food Menu Type --',
    'SELECT_FOOD_MENU_FOR'=>'-- Select Food Menu For --',
    'PARENT_FOOD_MENU'=>'Parent Food Menu',
    'SELECT_PARENT_FOOD_MENU'=>'-- Select Parent Food Menu --',
    'FOOD_MENU_PRICE'=>'Food Menu Price',
    'SUB_FOOD_MENU'=>'Sub Food Menu',
    'SELECT_SUB_FOOD_MENU'=>'-- Select Sub Food Menu --',
    'FOOD_MENU_DATA_NOT_EXPORTED'=>'Food menus cannot be exported.',
    /******** Food Menu Management End   */

    /******** Food Menu Item Management Start   */
    'FOODMENUITEM'=>'Food Menu Item',
    'FOODMENUITEMS'=>'Food Menu Items (Individual)',
    'SELECT_FOODMENUITEM'=>'Select Food Menu Item',
    'ADD_FOODMENUITEM'=>'Add Food Menu Item',
    'EDIT_FOODMENUITEM'=>'Edit Food Menu Item',
    'FOODMENUITEM_ADD_SUCCESSFULLY'=>'Food Menu Item added successfully.',
    'FOODMENUITEM_CHANGE_STATUS_SUCCESSFULLY'=>'Food Menu Item status changed successfully.',
    'FOODMENUITEM_UPDATE_SUCCESSFULLY'=>'Food Menu Item updated successfully.',
    'FOOD_MENU_ITEM_DESCRIPTION'=>'Food Menu Item Description',
    'GROUP_FOOD_MENU_ITEMS'=>'Food Menu Items (Group)',
    'INDIVIDUAL_FOOD_MENU_ITEM_DATA_NOT_EXPORTED'=>'Individual Food menu items cannot be exported.',
    'GROUP_FOOD_MENU_ITEM_DATA_NOT_EXPORTED'=>'Group Food menu items cannot be exported.',
    /******** Food Menu Item Management End   */
    
     'ATTENDIES'=>'Attendees',
     'ATTENDEING'=>'Attending',
     'EVENT_ATTENDEES'=>'Event Attendees',

      /******** Order Management Start   */
    'ORDER'=>'Order',
    'ORDERS'=>'Orders',
    'ORDER_CHANGE_STATUS_TO_DELIVERED_SUCCESSFULLY'=>'Order status changed to delivered successfully.',
    'INVOICE_NO'=>'Invoice No.',
    'ORDER_TYPE'=>'Order Type',
    'ORDER_AMOUNT'=>'Order Amount',
    'TAX_AMOUNT'=>'Tax Amount',
    'TOTAL_AMOUNT'=>'Total Amount',
    'PAYMENT_TYPE' => 'Payment Type',
    'ORDER_STATUS'=>'Order Status',
    'ORDER_DATE'=>'Order Date',
    'ORDER_DETAIL'=>'Order Detail',
    'ORDER_DATA_NOT_EXPORTED'=>'Order data cannot be exported.',
    /******** Order Management End   */

    /******** Event Enquiry Management Start   */
    'EVENT_ENQUIRY'=>'Event Enquiries',
    'EVENT_ENQUIRIES'=>'Manage Event Enquiries',
    'EVENT_ENQUIRY_DATA_NOT_EXPORTED'=>'Event enquiries cannot be exported.',
    /******** Event Enquiry Management End   */

    /******** Promo Code Management Start   */
    'PROMO_CODES'=>'Manage Promo Code',
    'ADD_PROMO_CODE'=>'Add Promo Code',
    'EDIT_PROMO_CODE'=>'Edit Promo Code',
    'PROMO_CODE_ADD_SUCCESSFULLY'=>'Promo code added successfully.',
    'PROMO_CODE_CHANGE_STATUS_SUCCESSFULLY'=>'Promo code status changed successfully.',
    'PROMO_CODE_UPDATE_SUCCESSFULLY'=>'Promo code updated successfully.',
    'PROMO_CODE_DATA_NOT_EXPORTED'=>'Promo code cannot be exported.',
    /******** Promo Code Management End   */

    /******** Food Bar Management Start   */
    'FOOD_BARS'=>'Manage Food Bar',
    'ADD_FOOD_BAR'=>'Add Food Bar',
    'EDIT_FOOD_BAR'=>'Edit Food Bar',
    'FOOD_BAR_DATE' => 'Food Bar Date',
    'FOOD_BAR_TIME' => 'Food Bar Time',
    'FOOD_BAR_ADD_SUCCESSFULLY'=>'Food bar added successfully.',
    'FOOD_BAR_CHANGE_STATUS_SUCCESSFULLY'=>'Food bar status changed successfully.',
    'FOOD_BAR_UPDATE_SUCCESSFULLY'=>'Food bar updated successfully.',
    'FOOD_BAR_DATA_NOT_EXPORTED'=>'Food bar cannot be exported.',
    /******** Food Bar Management End   */

    /******** Have Some Food Item Management Start   */
    'HAVE_SOME_FOOD_ITEMS'=>'Manage Have Some Food Items',
    'ADD_HAVE_SOME_FOOD_ITEM'=>'Add Have Some Food Item',
    'EDIT_HAVE_SOME_FOOD_ITEM'=>'Edit Have Some Food Item',
    'HAVE_SOME_FOOD_ITEM_ADD_SUCCESSFULLY'=>'Food item added successfully.',
    'HAVE_SOME_FOOD_ITEM_CHANGE_STATUS_SUCCESSFULLY'=>'Food item status changed successfully.',
    'HAVE_SOME_FOOD_ITEM_UPDATE_SUCCESSFULLY'=>'Food item updated successfully.',
    'HAVE_SOME_FOOD_ITEM_DATA_NOT_EXPORTED'=>'Food item cannot be exported.'
    /******** Have Some Food Item Management End   */
];
