<?php

if(!defined('DS')) {
    define("DS", '/');
}

if(!defined('MAIN_FOLDER')) {
    define("MAIN_FOLDER",  basename(__DIR__).'/');
}

if(!defined('PUBLIC_FOLDER')) {
    define("PUBLIC_FOLDER",  'public/');
}

if(!defined('ADMIN_FOLDER')) {
    define('ADMIN_FOLDER', 'admin/');
}

if(!defined('SITE_ROOT_PATH')) {
    define('SITE_ROOT_PATH', base_path());
}

if(!defined('SITE_WEBROOT_PATH')) {
    define('SITE_WEBROOT_PATH', public_path().'/');
}

if(!defined('SITE_URL')) {
    define("SITE_URL",url('/'));
}
if(!defined('ADMIN_URL')) {
    define("ADMIN_URL",SITE_URL.'/'.ADMIN_FOLDER);
}

if(!defined('WEBSITE_URL')) {
    define("WEBSITE_URL",url('/'));
}

if(!defined('WEBSITE_PUBLIC_URL')) {
    define("WEBSITE_PUBLIC_URL", WEBSITE_URL. '/');
}

if(!defined('WEBSITE_ADMIN_URL')) {
    define("WEBSITE_ADMIN_URL", WEBSITE_URL . ADMIN_FOLDER);
}

if (!defined('WEBSITE_ADMIN_IMG_URL')) {
    define("WEBSITE_ADMIN_IMG_URL", WEBSITE_URL . ADMIN_FOLDER . 'img' . '/');
}

if (!defined('PUBLIC_FOLDER_URL')) {
    define('PUBLIC_FOLDER_URL', WEBSITE_PUBLIC_URL . 'public/');
}

if (!defined('WEBSITE_FRONT_IMG_URL')) {
    define("WEBSITE_FRONT_IMG_URL", PUBLIC_FOLDER_URL . 'images/');
}

if (!defined('PUBLIC_ROOT_PATH')) {
    define('PUBLIC_ROOT_PATH', SITE_ROOT_PATH . DS. 'public');
}


if (!defined('UPLOADS_FOLDER_URL')) {
    define('UPLOADS_FOLDER_URL', PUBLIC_FOLDER_URL . 'uploads/');
}

if (!defined('UPLOADS_ROOT_PATH')) {
    define('UPLOADS_ROOT_PATH', PUBLIC_ROOT_PATH . DS. 'uploads');
}

if (!defined('DATE_SEPERATOR')) {
    define("DATE_SEPERATOR", '/');
}

if (!defined('DATE_FORMATE')) {
    define('DATE_FORMATE', 'd' . DATE_SEPERATOR . 'm' . DATE_SEPERATOR . 'Y');
}

if (!defined('MDY_FORMATE')) {
    define('MDY_FORMATE', 'm' . DATE_SEPERATOR . 'd' . DATE_SEPERATOR . 'Y');
}

if (!defined('DATETIME_FORMATE')) {
    define('DATETIME_FORMATE', 'd' . DATE_SEPERATOR . 'm' . DATE_SEPERATOR . 'Y' . ' h:i A');
}

if (!defined('MDY_DATETIME_FORMATE')) {
    define('MDY_DATETIME_FORMATE', 'm' . DATE_SEPERATOR . 'd' . DATE_SEPERATOR . 'Y' . ' h:i A');
}

if (!defined('PARKING_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('PARKING_IMAGES_UPLOAD_DIRECTROY_PATH', UPLOADS_ROOT_PATH . DS . 'parking_images' . DS);    
}

/* Cuisine */
if (!defined('CUISINE_IMAGE_URL')) {
    define('CUISINE_IMAGE_URL', UPLOADS_FOLDER_URL . 'cuisine_images' . DS);
}

if (!defined('CUISINE_THUMB_130_IMAGE_URL')) {
    define('CUISINE_THUMB_130_IMAGE_URL', CUISINE_IMAGE_URL . "thumb130" . DS);
}

if (!defined('CUISINE_THUMB_200_IMAGE_URL')) {
    define('CUISINE_THUMB_200_IMAGE_URL', CUISINE_IMAGE_URL . "thumb200" . DS);
}

if (!defined('CUISINE_IMAGE_UPLOAD_DIRECTROY_PATH')) {
    define('CUISINE_IMAGE_UPLOAD_DIRECTROY_PATH', UPLOADS_ROOT_PATH . DS . 'cuisine_images' . DS);
}

if (!defined('GOVID_IMAGE_UPLOAD_DIRECTROY_PATH')) {
    define('GOVID_IMAGE_UPLOAD_DIRECTROY_PATH', UPLOADS_ROOT_PATH . DS . 'gov_id_docs' . DS);
}

/* User */
if (!defined('USER_IMAGE_URL')) {
    define('USER_IMAGE_URL', UPLOADS_FOLDER_URL . 'user_images' . DS);
}

/* Documents */
if (!defined('GOVID_IMAGE_URL')) {
    define('GOVID_IMAGE_URL', UPLOADS_FOLDER_URL . 'gov_id_docs' . DS);
}

if (!defined('USER_IMAGE_UPLOAD_DIRECTROY_PATH')) {
    define('USER_IMAGE_UPLOAD_DIRECTROY_PATH', UPLOADS_ROOT_PATH . DS . 'user_images' . DS);
}

if (!defined('T130_WIDTH')) {
    define('T130_WIDTH','130');
}
if (!defined('T200_WIDTH')) {
    define('T200_WIDTH','200');
}

if (!defined('T130')) {
    define('T130', 'thumb130/');
}
if (!defined('T200')) {
    define('T200', 'thumb200/');
}




