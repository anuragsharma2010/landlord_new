<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(array('namespace' => 'Api'), function(){
	Route::post('/login', ['as' => 'login', 'uses' => 'UsersController@login']);
	Route::post('/register', ['as' => 'register', 'uses' => 'UsersController@register']);
	Route::post('/facebook_login', ['as' => 'facebook_login', 'uses' => 'UsersController@facebook_login']);
	Route::post('/google_login', ['as' => 'google_login', 'uses' => 'UsersController@google_login']);
	Route::post('/check_otp', ['as' => 'check_otp', 'uses' => 'UsersController@check_otp']);
	Route::post('/forgot_password', ['as' => 'forgot_password', 'uses' => 'UsersController@forgot_password']);
	Route::post('/reset_password', ['as' => 'reset_password', 'uses' => 'UsersController@reset_password']);
	Route::post('/change_password', ['as' => 'change_password', 'uses' => 'UsersController@change_password']);
	Route::post('resend_otp', ['as' => 'resend_otp', 'uses' => 'UsersController@resend_otp']);
	
	Route::group(['middleware' => 'auth:api'], function(){
		
		Route::post('/addMoneyToWallet', ['as' => 'addMoneyToWallet', 'uses' => 'UsersController@addMoneyToWallet']);
		Route::post('/getWalletAmount', ['as' => 'getWalletAmount', 'uses' => 'UsersController@getWalletAmount']);
		
		Route::post('/allergies', ['as' => 'allergies', 'uses' => 'AllergiesController@index']);
		Route::post('/dietaries', ['as' => 'dietaries', 'uses' => 'DietariesController@index']);
		Route::post('/proteins', ['as' => 'proteins', 'uses' => 'ProteinsController@index']);
		Route::post('/cuisine', ['as' => 'cuisine', 'uses' => 'CuisineController@index']);
		Route::post('/budgets', ['as' => 'cuisine', 'uses' => 'BudgetsController@index']);
		
		Route::post('/add_user_address', ['as' => 'add_user_address', 'uses' => 'UsersController@add_user_address']);
		Route::post('/edit_user_address', ['as' => 'edit_user_address', 'uses' => 'UsersController@edit_user_address']);
		Route::post('/delete_user_address', ['as' => 'delete_user_address', 'uses' => 'UsersController@delete_user_address']);
		Route::post('/get_user_address', ['as' => 'get_user_address', 'uses' => 'UsersController@get_user_address']);
		Route::post('/user_address_list', ['as' => 'user_address_list', 'uses' => 'UsersController@user_address_list']);
		Route::post('/select_user_address', ['as' => 'select_user_address', 'uses' => 'UsersController@select_user_address']);

		Route::post('/user_list', ['as' => 'user_list', 'uses' => 'UsersController@user_list']);
		Route::post('/user_profile', ['as' => 'user_profile', 'uses' => 'UsersController@user_profile']);
		Route::post('/edit_profile', ['as' => 'edit_profile', 'uses' => 'UsersController@edit_profile']);
		Route::post('/add_user_allergies', ['as' => 'add_user_allergies', 'uses' => 'UsersController@add_user_allergies']);
		Route::post('/add_user_proteins', ['as' => 'add_user_proteins', 'uses' => 'UsersController@add_user_proteins']);
		Route::post('/add_user_dietaries', ['as' => 'add_user_dietaries', 'uses' => 'UsersController@add_user_dietaries']);
		Route::post('/add_user_cuisine', ['as' => 'add_user_cuisine', 'uses' => 'UsersController@add_user_cuisine']);

		Route::post('/invite_users', ['as' => 'invite_users', 'uses' => 'UsersController@invite_users']);
		
		Route::post('/foods', ['as' => 'foods', 'uses' => 'FoodsController@index']);

		Route::post('/locations', ['as' => 'locations', 'uses' => 'LocationsController@index']);
		//Route::post('/cuisine_menu_list', ['as' => 'cuisine_menu_list', 'uses' => 'CuisineController@get_cuisine_menu_list']);
		Route::post('/group_menu_list', ['as' => 'group_menu_list', 'uses' => 'FoodsController@get_group_menu_list']);
		Route::post('/individual_menu_list', ['as' => 'individual_menu_list', 'uses' => 'FoodsController@get_individual_menu_list']);
		Route::post('/add_event', ['as' => 'add_event', 'uses' => 'EventsController@add_event']);
		Route::post('/get_user_events', ['as' => 'get_user_events', 'uses' => 'EventsController@get_user_events']);
		Route::post('/delete_event_menu', ['as' => 'delete_event_menu', 'uses' => 'EventsController@delete_event_menu']);
		Route::post('/edit_event_menu', ['as' => 'edit_event_menu', 'uses' => 'EventsController@edit_event_menu']);
		Route::post('/event_checkout', ['as' => 'event_checkout', 'uses' => 'EventsController@event_checkout']);
		Route::post('/event_list', ['as' => 'event_list', 'uses' => 'EventsController@event_list']);
		Route::post('/get_event', ['as' => 'get_event', 'uses' => 'EventsController@get_event']);
		Route::post('/attend_event', ['as' => 'attend_event', 'uses' => 'EventsController@attend_event']);
		Route::post('/bookmark_event', ['as' => 'bookmark_event', 'uses' => 'EventsController@bookmark_event']);
		Route::post('/get_user_bookmark_events', ['as' => 'get_user_bookmark_events', 'uses' => 'EventsController@get_user_bookmark_events']);
		Route::post('/get_event_attendees', ['as' => 'get_event_attendees', 'uses' => 'EventsController@get_event_attendees']);

		Route::post('/order_food', ['as' => 'order_food', 'uses' => 'FoodsController@order_food']);
		Route::post('/order_food_checkout', ['as' => 'order_food_checkout', 'uses' => 'FoodsController@order_food_checkout']);
		Route::post('/get_user_food_order', ['as' => 'get_user_food_order', 'uses' => 'FoodsController@get_user_food_order']);
		Route::post('/delete_food_order_menu', ['as' => 'delete_food_order_menu', 'uses' => 'FoodsController@delete_food_order_menu']);
		Route::post('/edit_food_order_menu', ['as' => 'edit_food_order_menu', 'uses' => 'FoodsController@edit_food_order_menu']);

		Route::post('/get_braintree_token', ['as' => 'get_braintree_token', 'uses' => 'UsersController@get_braintree_token']);
		Route::post('/get_user_orders', ['as' => 'get_user_orders', 'uses' => 'UsersController@get_user_orders']);

		Route::post('/get_budget_cuisine_list', ['as' => 'get_budget_cuisine_list', 'uses' => 'CuisineController@get_budget_cuisine_list']);
		Route::post('/delete_event', ['as' => 'delete_event', 'uses' => 'EventsController@delete_event']);
		Route::post('/delete_order_food', ['as' => 'delete_order_food', 'uses' => 'FoodsController@delete_order_food']);
		Route::post('/get_user_cart', ['as' => 'get_user_cart', 'uses' => 'UsersController@get_user_cart']);
		Route::post('/add_event_enquiry', ['as' => 'add_event_enquiry', 'uses' => 'EventsController@add_event_enquiry']);
		Route::post('reorder_food_order', ['as' => 'reorder_food_order', 'uses' => 'FoodsController@reorder_food_order']);
		Route::post('get_promo_code_data', ['as' => 'get_promo_code_data', 'uses' => 'PromoCodesController@get_promo_code_data']);
		Route::post('/my_event_list', ['as' => 'my_event_list', 'uses' => 'EventsController@my_event_list']);
		Route::post('/food_bar_list', ['as' => 'food_bar_list', 'uses' => 'FoodsController@food_bar_list']);
		Route::post('/add_food_bar_to_cart', ['as' => 'add_food_bar_to_cart', 'uses' => 'FoodsController@add_food_bar_to_cart']);
		Route::post('/remove_food_bar', ['as' => 'remove_food_bar', 'uses' => 'FoodsController@remove_food_bar']);
		Route::post('/get_food_bar', ['as' => 'get_food_bar', 'uses' => 'FoodsController@get_food_bar']);
		Route::post('/food_bar_checkout', ['as' => 'food_bar_checkout', 'uses' => 'FoodsController@food_bar_checkout']);
		Route::post('/get_food_bar_attendees', ['as' => 'get_food_bar_attendees', 'uses' => 'FoodsController@get_food_bar_attendees']);
		Route::post('/get_cms_page', ['as' => 'get_cms_page', 'uses' => 'UsersController@get_cms_page']);
		Route::post('/add_user_card', ['as' => 'add_user_card', 'uses' => 'UsersController@add_user_card']);
		Route::post('/get_user_cards', ['as' => 'get_user_cards', 'uses' => 'UsersController@get_user_cards']);
		Route::post('/have_some_food_item_list', ['as' => 'have_some_food_item_list', 'uses' => 'FoodsController@have_some_food_item_list']);
		Route::post('/have_some_food_item_checkout', ['as' => 'have_some_food_item_checkout', 'uses' => 'FoodsController@have_some_food_item_checkout']);
		//Route::post('/logout', ['as' => 'logout', 'uses' => 'UsersController@logout']);
	});
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
 require base_path().'/global_constants.php';