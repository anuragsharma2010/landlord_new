<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
/*Route::group(['namespace' => 'Front', 'middleware' => 'UserMiddleware'], function() {
  //  Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/login', ['as' => 'login', 'uses' => 'UserController@getLogIn']);
    Route::get('/signup', ['as' => 'signup', 'uses' => 'UserController@getSignUp']);
    Route::get('/category', ['as' => 'category', 'uses' => 'CategoryController@getCategory']);
    Route::get('/product', ['as' => 'product', 'uses' => 'ProductController@getProduct']);
    Route::get('/activate_account/{token?}', ['as' => 'activateUser', 'uses' => 'UserController@index']);
});*/

/*Route::get('/', function () {
    return view('test');
});*/

Auth::routes();
//Route::post('/login', ['uses' => 'Auth\LoginController@login'])->name('login');

Route::get('/', ['as' => '/', 'uses' => 'Front\HomeController@index']);

Route::post('password/sendemail', ['uses' => 'Auth\ForgotPasswordController@sendemail']);
Route::get('password/reset_password/{email_token}/{change_pass_var}', ['uses' => 'Auth\ResetPasswordController@resetPassword']);
Route::post('password/reset_password', ['uses' => 'Auth\ResetPasswordController@resetPasswordUpdate']);
Route::post('password/change_password', ['uses' => 'Front\UserController@profilePasswordUpdate']);

Route::get('account', ['as' => 'front.account.index', 'uses' => 'Front\UserController@account']);
Route::patch('profile-update/{slug}', ['as' => 'front.profile_update', 'uses' => 'Front\UserController@profile_update']);

Route::get('/event-invite-user-update/{event_slug}/{user_email}/{attending}', ['as' => 'event_invite_user_update', 'uses' => 'Front\HomeController@updateEventInviteUserData']);
Route::get('/{slug}', ['as' => 'front.pages.index', 'uses' => 'Front\PagesController@index']);
/*******For Unauthorised*******/
Route::get('error/unauthorized', ['as' => 'front.unauthorized.index', 'uses' => 'Front\UnauthorizedController@index']);
Route::get('error/not-found', ['as' => 'front.unauthorized.notfound', 'uses' => 'Front\UnauthorizedController@notfound']);
/*******For Unauthorised end*******/
Route::get('pages/contact-us', ['as' => 'front.contact.index', 'uses' => 'Front\ContactController@index']);
Route::post('pages/store', ['as' => 'front.contact.store', 'uses' => 'Front\ContactController@store']);

Route::get('country/{country_id}', 'Front\UserController@getState');
Route::get('state/{state_id}', 'Front\UserController@getCity');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::group(['middleware' => 'IsNotAuthenticated'], function() {

        Route::get('login', function() {
            return View::make('home');
        });

        Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@getLogin']);
        Route::post('login', ['as' => 'admin.login', 'uses' => 'AuthController@postLogin']);
        Route::get('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@forgotPassword']);
        Route::post('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
        Route::get('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPassword']);
        Route::post('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPasswordUpdate']);
    });

    Route::group(['middleware' => 'auth.admin'], function() {

        /*******For Unauthorised*******/
        Route::get('unauthorized', ['as' => 'admin.unauthorized.index', 'uses' => 'UnauthorizedController@index']);
        /*******For Unauthorised end*******/

        Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);
        Route::get('change-password', ['as' => 'admin.ChangePassword', 'uses' => 'UsersController@ChangePassword']);
        Route::post('change-password', ['as' => 'admin.UpdateChangePassword', 'uses' => 'UsersController@UpdateChangePassword']);

        
        Route::get('dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'PagesController@index']);
        Route::get('/', ['as' => 'dashboard', 'uses' => 'PagesController@index']);

        Route::resource('settings', 'SettingsController', ['names' => [
                'index' => 'admin.settings.index',
                'update' => 'admin.settings.update',
        ]]);



        /* Admin menu route */
        //Route::resource('adminmenus', 'AdminMenuController');

        Route::get('adminmenus', ['as' => 'admin.adminmenus.index', 'uses' => 'AdminMenuController@index']);

        Route::get('add-adminmenu/{id?}', ['as' => 'admin.adminmenus.create', 'uses' => 'AdminMenuController@create']);

        Route::post('add-adminmenu/{id?}', ['as' => 'admin.adminmenus.store', 'uses' => 'AdminMenuController@store']);

        Route::get('adminmenu-status-change/{id}/{status}/{parent_id?}', ['as' => 'admin.adminmenus.status_change', 'uses' => 'AdminMenuController@status_change']);

        Route::get('child-adminmenu/{id?}', ['as' => 'admin.adminmenus.child_menu', 'uses' => 'AdminMenuController@childMenu']);

        Route::get('edit-adminmenu/{id}/{parent_id?}', ['as' => 'admin.adminmenus.edit', 'uses' => 'AdminMenuController@edit']);

        Route::post('edit-adminmenu/{id}/{parent_id?}', ['as' => 'admin.adminmenus.update', 'uses' => 'AdminMenuController@update']);
        /* fornt menu route */

        //admin routes starts
        Route::get('profile', ['as' => 'admin.profile', 'uses' => 'UsersController@profile']);

        Route::post('profile', ['as' => 'admin.updateProfile', 'uses' => 'UsersController@updateProfile']);

        Route::get('user-email_verify/{id}/{email_verify}', ['as' => 'admin.users.email_verify', 'uses' => 'UsersController@email_verify']);
        Route::get('send-credentials/{id}', ['as' => 'admin.users.send_credentials', 'uses' => 'UsersController@sendCredentials']);
        Route::get('view-users/{id}', ['as' => 'admin.users.view', 'uses' => 'UsersController@view']);
        //admin routes ends

      
        //caterer routes starts
        Route::resource('users', 'UsersController', ['names' => [
                'index' => 'admin.users.index',
                'create' => 'admin.users.create',
                'store' => 'admin.users.store',
                'edit' => 'admin.users.edit',
                'update' => 'admin.users.update',
        ]]);

        Route::get('user-status-change/{id}/{status}', ['as' => 'admin.users.status_change', 'uses' => 'UsersController@status_change']);
        
        Route::get('export-user-data/{type}', ['as' => 'admin.users.exportData', 'uses' => 'UsersController@exportData']);

        //Address routes starts

        Route::get('user_address/index/{slug}', ['as' => 'admin.address.index', 'uses' => 'AddressController@index']);
        Route::get('user_address/add/{slug}', ['as' => 'admin.address.add', 'uses' => 'AddressController@create']);
        Route::post('address/store/{slug}', ['as' => 'admin.address.store', 'uses' => 'AddressController@store']);
        Route::get('user_address/edit/{slug1}/{slug2}', ['as' => 'admin.address.edit', 'uses' => 'AddressController@edit']);
        Route::patch('user_address/update/{slug1}/{slug2}', ['as' => 'admin.address.update', 'uses' => 'AddressController@update']);

        Route::get('address-status-change/{slug1}/{slug2}/{status}', ['as' => 'admin.address.status_change', 'uses' => 'AddressController@status_change']);
        //Address routes ends

        Route::resource('preparers', 'PreparerController', ['names' => [
                'index' => 'admin.preparers.index',
                'create' => 'admin.preparers.create',
                'store' => 'admin.preparers.store',
                'edit' => 'admin.preparers.edit',
                'update' => 'admin.preparers.update',
        ]]);

        Route::get('prepares-status-change/{id}/{status}', ['as' => 'admin.preparers.status_change', 'uses' => 'PreparerController@status_change']);
        Route::get('send-prepare-credentials/{id}', ['as' => 'admin.preparers.send_credentials', 'uses' => 'PreparerController@sendCredentials']);

        Route::get('country/{country_id}', 'UsersController@getState');
        Route::get('state/{state_id}', 'UsersController@getCity');
        //caterer routes ends

        //prepare routes starts
        Route::get('manage-prepare/{slug}', ['as' => 'admin.manage_prepare', 'uses' => 'UsersController@manage_prepare']);
        Route::get('create-prepare/{slug}', ['as' => 'admin.create_prepare', 'uses' => 'UsersController@create_prepare']);
        //prepare routes ends

        //user routes starts
        Route::get('manage-user', ['as' => 'admin.manage_user', 'uses' => 'UsersController@manage_user']);
        Route::get('create-user', ['as' => 'admin.create_user', 'uses' => 'UsersController@create_user']);
        Route::post('store-user', ['as' => 'admin.store_user', 'uses' => 'UsersController@store_user']);
        
        Route::get('edit-user/{slug}', ['as' => 'admin.edit_user', 'uses' => 'UsersController@edit_user']);
        Route::patch('update-user/{slug}', ['as' => 'admin.update_user', 'uses' => 'UsersController@update_user']);

        Route::get('status-change-user/{id}/{status}', ['as' => 'admin.status_change_user', 'uses' => 'UsersController@status_change_user']);
        //user routes ends

        //landlord user routes starts
        Route::get('manage-landlord-user', ['as' => 'admin.manage_landlord_user', 'uses' => 'UsersController@manage_landlord_user']);
        Route::get('create-landlord-user', ['as' => 'admin.create_landlord_user', 'uses' => 'UsersController@create_landlord_user']);
        Route::post('store-landlord-user', ['as' => 'admin.store_landlord_user', 'uses' => 'UsersController@store_landlord_user']);
        
        Route::get('edit-landlord-user/{slug}', ['as' => 'admin.edit_landlord_user', 'uses' => 'UsersController@edit_landlord_user']);
        Route::get('delete-landlord-user/{slug}', ['as' => 'admin.delete_landlord_user', 'uses' => 'UsersController@delete_landlord_user']);
        Route::patch('update-landlord-user/{slug}', ['as' => 'admin.update_landlord_user', 'uses' => 'UsersController@update_landlord_user']);

        Route::get('status-change-landlord-user/{id}/{status}', ['as' => 'admin.status_change_landlord_user', 'uses' => 'UsersController@status_change_landlord_user']);
        Route::get('status-change-landlord-user/{id}/{status}', ['as' => 'admin.status_change_landlord_user', 'uses' => 'UsersController@status_change_landlord_user']);
        //landlord user routes ends


        //cms routes starts
        Route::resource('cms', 'CmsController', ['names' => [
                'index' => 'admin.cms.index',
                'create' => 'admin.cms.create',
                'store' => 'admin.cms.store',
                'edit' => 'admin.cms.edit',
                'update' => 'admin.cms.update',
        ]]);

        Route::get('cms-status-change/{id}/{status}', ['as' => 'admin.cms.status_change', 'uses' => 'CmsController@status_change']);
        Route::get('cms/delete/{slug}', ['as' => 'admin.cms.delete', 'uses' => 'CmsController@delete']);

        //cms routes ends

        //Role routes starts
        Route::resource('roles', 'RolesController', ['names' => [
                'index' => 'admin.roles.index',
                'create' => 'admin.roles.create',
                'store' => 'admin.roles.store',
                'edit' => 'admin.roles.edit',
                'update' => 'admin.roles.update',
        ]]);

        Route::get('role-status-change/{id}/{status}', ['as' => 'admin.roles.status_change', 'uses' => 'RolesController@status_change']);

        //Role routes ends

        //Permission Route start

         Route::resource('permissions', 'PermissionsController', ['names' => [
                'index' => 'admin.permissions.index',
                'create' => 'admin.permissions.create',
                'store' => 'admin.permissions.store',
                'edit' => 'admin.permissions.edit',
                'update' => 'admin.permissions.update',
        ]]);
        //Route::resource('permissions', 'PermissionsController');
        //Permission Route end

        //business routes starts
        Route::resource('business', 'BusinessController', ['names' => [
                'index' => 'admin.business.index',
                'create' => 'admin.business.create',
                'store' => 'admin.business.store',
                'edit' => 'admin.business.edit',
                'update' => 'admin.business.update',
        ]]);

        Route::get('business-status-change/{slug}/{status}', ['as' => 'admin.business.status_change', 'uses' => 'BusinessController@status_change']);

        Route::get('business/delete/{slug}', ['as' => 'admin.business.delete', 'uses' => 'BusinessController@delete']);

        //business routes ends

        //position routes starts
        Route::resource('position', 'PositionController', ['names' => [
                'index' => 'admin.position.index',
                'create' => 'admin.position.create',
                'store' => 'admin.position.store',
                'edit' => 'admin.position.edit',
                'update' => 'admin.position.update',
        ]]);

        Route::get('position-status-change/{slug}/{status}', ['as' => 'admin.position.status_change', 'uses' => 'PositionController@status_change']);

        Route::get('position/delete/{slug}', ['as' => 'admin.position.delete', 'uses' => 'PositionController@delete']);

        //position routes ends

        //buildings routes starts
        Route::resource('buildings', 'BuildingsController', ['names' => [
                'index' => 'admin.buildings.index',
                'create' => 'admin.buildings.create',
                'store' => 'admin.buildings.store',
                'edit' => 'admin.buildings.edit',
                'update' => 'admin.buildings.update',
        ]]);

        Route::get('building-status-change/{slug}/{status}', ['as' => 'admin.buildings.status_change', 'uses' => 'BuildingsController@status_change']);

        Route::get('buildings/delete/{slug}', ['as' => 'admin.buildings.delete', 'uses' => 'BuildingsController@delete']);
        Route::get('buildings/view/{slug}', ['as' => 'admin.buildings.view', 'uses' => 'BuildingsController@view']);

        //buildings routes ends

        //Notification routes starts
        Route::resource('notifications', 'NotificationsController', ['names' => [
                'index' => 'admin.notifications.index',
        ]]);
        //Notification routes ends

        
        //locations routes starts
        Route::resource('locations', 'LocationsController', ['names' => [
                'index' => 'admin.locations.index',
                'create' => 'admin.locations.create',
                'store' => 'admin.locations.store',
                'edit' => 'admin.locations.edit',
                'update' => 'admin.locations.update',
        ]]);
        Route::get('location-status-change/{slug}/{status}', ['as' => 'admin.locations.status_change', 'uses' => 'LocationsController@status_change']);
        
        //locations routes ends
        
    });
});

require base_path() . '/global_constants.php';          
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
